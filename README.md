# Schulplaner

JavaFX Schulprojekt am CSBME

Run
```
Desktop: application -> run
Mobile: gluon mobile for * -> *install
```

Gradle Repositories:
```
repositories {
    jcenter()
    maven {
        url 'http://nexus.gluonhq.com/nexus/content/repositories/releases'
    }
    mavenLocal()
}
```

Gradle Dependencies:
```
dependencies {
    compile 'com.gluonhq:charm:5.0.2'
    compile 'org.projectlombok:lombok:1.18.10'
    compile 'com.jfoenix:jfoenix:8.0.8'
    compile 'de.csbme.schulplaner:Lib:1.0.0-SNAPSHOT'
    compile 'com.gluonhq:glisten-afterburner:1.4.1'
    compile 'io.netty:netty-all:4.0.36.Final'
    compile group: 'com.calendarfx', name: 'view', version: '8.6.1'
    compile group: 'io.github.typhon0', name: 'AnimateFX', version: '1.2.1'
}
```

Gradle Android config:
```
android {
    manifest = 'src/android/AndroidManifest.xml'
    androidSdk = 'C:/Users/YOUR NAME/AppData/Local/Android/Sdk'
    compileSdkVersion = 'YOUR VERSION'
    packagingOptions {
        pickFirst 'META-INF/INDEX.LIST'
        pickFirst 'META-INF/LICENSE'
        pickFirst 'META-INF/io.netty.versions.properties'
    }
}
```

Gradle down config:
```
downConfig {
    version = '3.8.6'
    plugins 'display', 'lifecycle', 'statusbar', 'storage', 'device'
}
```