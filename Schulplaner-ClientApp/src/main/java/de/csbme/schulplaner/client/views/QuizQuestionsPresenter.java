package de.csbme.schulplaner.client.views;

import com.gluonhq.charm.glisten.afterburner.GluonPresenter;
import com.gluonhq.charm.glisten.animation.FlipInXTransition;
import com.gluonhq.charm.glisten.animation.NoTransition;
import com.gluonhq.charm.glisten.mvc.View;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXProgressBar;
import de.csbme.schulplaner.client.Main;
import de.csbme.schulplaner.client.builder.HeaderBuilder;
import de.csbme.schulplaner.client.components.NavigationComponent;
import de.csbme.schulplaner.client.components.SidebarNavigationComponent;
import de.csbme.schulplaner.client.manager.AppViewManager;
import de.csbme.schulplaner.client.types.PlatformType;
import de.csbme.schulplaner.client.utils.ComponentUtil;
import de.csbme.schulplaner.client.utils.WindowUtil;
import de.csbme.schulplaner.lib.object.Quiz;
import de.csbme.schulplaner.lib.time.TimeUtil;
import de.csbme.schulplaner.lib.user.Settings;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.time.Duration;
import java.time.LocalTime;
import java.util.*;

public class QuizQuestionsPresenter extends GluonPresenter<Main> implements PresenterFunctions {

    @FXML
    private View root;

    @FXML
    private HBox hbxTop, hbxExit;

    @FXML
    private VBox vbxCenter, vbxSidebar, vbxLeftbar;

    @FXML
    private StackPane stckCenter;

    @FXML
    private Label lblQuestions, lblQuestion;

    @FXML
    private JFXProgressBar pbQuestion;

    @FXML
    private AnchorPane anchBottom, anchOne, anchTwo, anchThree, anchFour;

    private Queue<Quiz.Question> questionQueue = null;

    private int currentQuestion, questionSize, rightAnswers;

    private LocalTime startTime;

    public void initialize() {
        WindowUtil.disableAppBar(root);
        WindowUtil.percentageConstraints(root, hbxTop, stckCenter);

        root.setShowTransitionFactory(view -> {
            if (Main.getInstance().getAttributes().getUser().getSettings().get(Settings.Key.ANIMATION, Boolean.class)) {
                return new FlipInXTransition(stckCenter);
            }
            return new NoTransition();
        });

        onHover(anchOne);
        onHover(anchTwo);
        onHover(anchThree);
        onHover(anchFour);

        if (Main.getInstance().getAttributes().getPlatform() == PlatformType.MOBILE) {
            anchBottom.getChildren().add(new NavigationComponent());
        } else {
            new SidebarNavigationComponent(vbxSidebar, vbxLeftbar, hbxExit);
        }
    }

    @Override
    public void refresh() {
        this.hbxTop.getChildren().clear();
        if (Main.getInstance().getAttributes().getUser().getCurrentQuiz() == null
                || Main.getInstance().getAttributes().getUser().getCurrentQuiz().getQuestions().isEmpty()) {
            lblQuestion.setText("Es wurden keine Fragen gefunden!");
            updateAnswer(anchOne, "", false);
            updateAnswer(anchTwo, "", false);
            updateAnswer(anchThree, "", false);
            updateAnswer(anchFour, "", false);
            this.getDialog().setTitle("Keine Fragen!").addContentText("Es existieren keine Fragen für dieses Quiz!")
                    .addButton("Schließen", true, e -> AppViewManager.changeView(AppViewManager.QUIZ_VIEW)).show();
            return;
        }
        Quiz currentQuiz = Main.getInstance().getAttributes().getUser().getCurrentQuiz();
        List<Quiz.Question> questions = currentQuiz.getQuestions();
        this.questionQueue = new LinkedList<>();
        List<Quiz.Question> questionsShuffle = new ArrayList<>(questions);
        Collections.shuffle(questionsShuffle);
        for (int i = 0; i < 10 && i < questionsShuffle.size(); i++) {
            this.questionQueue.add(questionsShuffle.get(i));
        }
        this.questionSize = this.questionQueue.size();
        this.startTime = LocalTime.now();

        new HeaderBuilder().setTitle(currentQuiz.getName()).addHeaderDescription(currentQuiz.getDescription())
                .addClickableItem("Quiz verlassen", e -> AppViewManager.changeView(AppViewManager.QUIZ_VIEW))
                .addChangeViewButton(AppViewManager.QUIZ_VIEW).show(this.hbxTop);

        this.currentQuestion = 0;
        this.rightAnswers = 0;
        this.nextQuestion();
    }

    @Override
    public StackPane getCenterPane() {
        return this.stckCenter;
    }

    private void nextQuestion() {
        Quiz.Question question = this.questionQueue.poll();
        if (question == null) {
            // End
            this.getDialog().setTitle("Quiz abgeschlossen").addContentText(
                    "Richtige Antworten: " + this.rightAnswers + "/" + this.questionSize + " (" + ((this.rightAnswers * 100) / this.questionSize) + "%)",
                    "Benötigte Zeit: " + TimeUtil.getDurationFromMilliseconds(Duration.between(this.startTime, LocalTime.now()).toMillis())
            ).addButton("Schließen", true, e -> {}).addCloseEvent(e -> AppViewManager.changeView(AppViewManager.QUIZ_VIEW)).show();
            return;
        }
        this.currentQuestion++;
        this.lblQuestions.setText("Frage " + this.currentQuestion + "/" + this.questionSize);
        this.pbQuestion.setProgress((float) this.currentQuestion / this.questionSize);
        this.lblQuestion.setText(question.getQuestion());
        this.updateAnswer(anchOne, question.getAnswer1(), question.getRightAnswer() == 1);
        this.updateAnswer(anchTwo, question.getAnswer2(), question.getRightAnswer() == 2);
        this.updateAnswer(anchThree, question.getAnswer3(), question.getRightAnswer() == 3);
        this.updateAnswer(anchFour, question.getAnswer4(), question.getRightAnswer() == 4);
    }

    private void onHover(AnchorPane anch) {
        anch.setOnMouseEntered(e -> {
            anch.getChildren().add(ComponentUtil.hoverCheck());
        });
        anch.setOnMouseExited(e -> {
            anch.getChildren().remove(2);
        });
    }

    private void updateAnswer(AnchorPane anchorPane, String answer, boolean rightAnswer) {
        anchorPane.getChildren().stream().filter(n -> n instanceof JFXButton).findFirst().ifPresent(node -> {
            JFXButton button = (JFXButton) node;
            button.setText(answer);
            button.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    if (rightAnswer) {
                        rightAnswers++;
                    }
                    nextQuestion();
                    button.removeEventHandler(ActionEvent.ACTION, this);
                }
            });
        });
    }
}