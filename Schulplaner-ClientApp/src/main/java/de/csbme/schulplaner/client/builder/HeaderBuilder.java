package de.csbme.schulplaner.client.builder;

import com.gluonhq.charm.glisten.afterburner.AppView;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXPopup;
import com.jfoenix.controls.JFXRippler;
import de.csbme.schulplaner.client.components.ArrowComponent;
import de.csbme.schulplaner.client.components.LogoComponent;
import de.csbme.schulplaner.client.views.PresenterFunctions;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;

public class HeaderBuilder {

    private List<HBox> items;

    private Label lblTitle;

    private ArrowComponent arrowComponent;

    private LogoComponent logoComponent;

    private String headerDescription;

    private Callable<Boolean> allowClickOnPopup;

    public HeaderBuilder() {
        this.headerDescription = null;
        this.items = new ArrayList<>();
        this.allowClickOnPopup = null;
    }

    public HeaderBuilder setTitle(String title) {
        this.lblTitle = new Label(title);
        this.lblTitle.getStyleClass().add("fs-24");
        return this;
    }

    public HeaderBuilder addNotificationAmount(int notifications) {
        this.headerDescription = "" + notifications;
        return this;
    }

    public HeaderBuilder addHeaderDescription(String headerDescription) {
        this.headerDescription = headerDescription;
        return this;
    }

    public HeaderBuilder addClickableLogo(String letter, PresenterFunctions presenterFunctions) {
        this.logoComponent = new LogoComponent(letter, 40, Pos.CENTER_RIGHT, presenterFunctions);
        return this;
    }

    public HeaderBuilder addClickableItem(String displayName, EventHandler<MouseEvent> event) {
        Label label = new Label(displayName);
        HBox hbxLabelHolder = new HBox(label);
        hbxLabelHolder.setOnMouseClicked(event);
        hbxLabelHolder.getStyleClass().add("hand");
        this.items.add(hbxLabelHolder);
        return this;
    }

    public HeaderBuilder allowToOpenClickableItems(Callable<Boolean> callable) {
        this.allowClickOnPopup = callable;
        return this;
    }

    public HeaderBuilder addChangeViewButton(AppView newView) {
        this.arrowComponent = new ArrowComponent(newView);
        return this;
    }

    public void show(HBox hBox) {
        VBox vbxContent = new VBox();

        AtomicReference<JFXRippler> atomicRippler = new AtomicReference<>(null);
        if (this.items.size() > 0) {
            JFXHamburger burger = new JFXHamburger();
            atomicRippler.set(new JFXRippler(burger));
            JFXListView<HBox> list = new JFXListView<>();
            list.getItems().addAll(this.items);
            list.selectionModelProperty().addListener((observable, oldValue, newValue) -> {

            });
            JFXPopup popup = new JFXPopup(list);
            burger.setOnMouseClicked(e -> {
                try {
                    if (this.allowClickOnPopup != null && !this.allowClickOnPopup.call()) {
                        return;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                popup.show(atomicRippler.get(), JFXPopup.PopupVPosition.TOP, JFXPopup.PopupHPosition.RIGHT);
            });
            list.setOnMouseClicked(e -> popup.hide());
        }

        HBox hbxNotification = new HBox();
        hbxNotification.setAlignment(Pos.CENTER_LEFT);
        if (this.lblTitle != null) {
            hbxNotification.getChildren().add(this.lblTitle);
        }
        if (this.headerDescription != null) {
            hbxNotification.getChildren().add(this.getNotificationLabel(this.headerDescription));
        }

        StackPane stckContent = new StackPane();
        stckContent.getChildren().add(hbxNotification);
        if (this.logoComponent != null) {
            stckContent.getChildren().add(this.logoComponent);
            vbxContent.getChildren().add(stckContent);
        } else {
            if (atomicRippler.get() != null) {
                stckContent.getChildren().add(atomicRippler.get());
            }
            if (this.arrowComponent != null) {
                vbxContent.getChildren().add(this.arrowComponent);
                VBox.setMargin(this.arrowComponent, new Insets(0, 0, 18, 0));
            }
            vbxContent.getChildren().add(stckContent);
        }

        HBox.setHgrow(vbxContent, Priority.ALWAYS);
        hBox.getChildren().addAll(vbxContent);
    }

    private Label getNotificationLabel(String description) {
        Label lblNotification = new Label(description);
        lblNotification.getStyleClass().add("notifications");

        try {
            if (Integer.parseInt(description) == 0) {
                lblNotification.setVisible(false);
            }
        } catch (NumberFormatException e) {
        }
        HBox.setMargin(lblNotification, new Insets(0, 0, 0, 8));

        return lblNotification;
    }
}
