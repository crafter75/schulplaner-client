package de.csbme.schulplaner.client.utils;

import com.jfoenix.controls.JFXComboBox;
import de.csbme.schulplaner.client.types.CrudIconType;
import javafx.animation.ScaleTransition;
import javafx.geometry.Point3D;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.shape.SVGPath;
import javafx.util.Duration;

public class ComponentUtil {

    public static <T> JFXComboBox<T> pickerComboBox() {
        JFXComboBox<T> comboBox = new JFXComboBox<>();
        comboBox.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(comboBox, Priority.ALWAYS);
        return comboBox;
    }

    public static Label crudIcon(CrudIconType type) {
        SVGPath icon = new SVGPath();
        icon.setContent(type.getPath());
        icon.getStyleClass().add("addIcon");

        Label lblIcon = new Label("", icon);

        lblIcon.setAlignment(Pos.CENTER);
        lblIcon.getStyleClass().add("boxCircle");

        return lblIcon;
    }

    public static Label hoverCheck() {
        SVGPath icoCheck = new SVGPath();
        icoCheck.getStyleClass().add("checkIcon");
        icoCheck.setContent("M10.187,10.681l-5.2-8.007-3.244,3.6A1,1,0,0,1,.257,4.938L4.405.331A1,1,0,0,1,5.749.2a2.814,2.814,0,0,1,.582.779l5.533,8.613a1,1,0,1,1-1.678,1.089Z");
        icoCheck.setRotationAxis(new Point3D(100, 0, 1));

        Label lblContent = new Label("", icoCheck);
        lblContent.getStyleClass().add("quizCheck");

        AnchorPane.setTopAnchor(lblContent, 8D);
        AnchorPane.setRightAnchor(lblContent, 18D);
        AnchorPane.setBottomAnchor(lblContent, 8D);

        return lblContent;
    }

}
