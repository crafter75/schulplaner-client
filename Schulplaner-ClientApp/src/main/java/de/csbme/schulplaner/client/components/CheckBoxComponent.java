package de.csbme.schulplaner.client.components;

import com.jfoenix.controls.JFXCheckBox;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

public class CheckBoxComponent extends HBox {

    private JFXCheckBox cbxComponent;

    public CheckBoxComponent( String title ) {
        Label lblTitle = new Label( title );
        lblTitle.getStyleClass().add( "checkboxComponent" );
        HBox.setMargin( lblTitle, new Insets( 6, 0, 0, 0 ) );

        this.cbxComponent = new JFXCheckBox();
        HBox.setHgrow( this.cbxComponent, Priority.ALWAYS );
        HBox.setMargin( this.cbxComponent, new Insets( 0, 3, 0, 0 ) );
        this.cbxComponent.setNodeOrientation( NodeOrientation.RIGHT_TO_LEFT );
        this.cbxComponent.setMaxWidth( Double.MAX_VALUE );

        this.setAlignment( Pos.CENTER_LEFT );
        this.getChildren().addAll( lblTitle, this.cbxComponent );
    }

    public void setSelected( boolean value ) {
        this.cbxComponent.setSelected( value );
    }

    public boolean isSelected() {
        return this.cbxComponent.isSelected();
    }

    public void addListener( ChangeListener<Boolean> changeListener ) {
        this.cbxComponent.selectedProperty().addListener( changeListener );
    }

}