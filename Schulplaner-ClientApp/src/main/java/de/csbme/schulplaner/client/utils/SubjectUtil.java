package de.csbme.schulplaner.client.utils;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import de.csbme.schulplaner.client.Main;
import de.csbme.schulplaner.client.views.PresenterFunctions;
import de.csbme.schulplaner.lib.network.packets.subject.SubjectCreatePacket;
import de.csbme.schulplaner.lib.network.packets.subject.SubjectDeletePacket;
import de.csbme.schulplaner.lib.network.packets.subject.SubjectUpdatePacket;
import de.csbme.schulplaner.lib.object.Subject;

import java.util.Optional;
import java.util.stream.Collectors;

public class SubjectUtil {

    public static void showSubjectAddOrUpdateDialog( PresenterFunctions presenterFunctions, Subject subject ) {
        if ( Main.getInstance().getAttributes().getUser().getClassInfo() == null ) {
            presenterFunctions.showMessageDialog( "Fehler", "Du musst zuerst eine Klasse beitreten oder erstellen!" );
            return;
        }
        JFXTextField txtSubject = new JFXTextField();
        txtSubject.setPromptText( "Name" );
        if ( subject != null ) {
            txtSubject.setText( subject.getName() );
        }

        JFXTextField txtSubjectShort = new JFXTextField();
        txtSubjectShort.setPromptText( "Kürzel" );
        txtSubjectShort.textProperty().addListener( ( observable, oldValue, newValue ) -> {
            if ( newValue.length() > 3 ) {
                newValue = newValue.substring( 0, 3 );
            }
            txtSubjectShort.setText( newValue );
        } );
        if ( subject != null ) {
            txtSubjectShort.setText( subject.getShortName() );
        }

        presenterFunctions.getDialog().setTitle( subject == null ? "Fach erstellen" : "Fach bearbeiten" )
                .addContentNode( txtSubject, txtSubjectShort )
                .addButton( "Speichern", true, e -> {
                    if ( txtSubject.getText() == null || txtSubject.getText().isEmpty()
                            || txtSubjectShort.getText() == null || txtSubjectShort.getText().isEmpty() ) {
                        e.consume();
                        presenterFunctions.showMessageDialog( "Fehler", "Bitte fülle alle Felder aus!" );
                        return;
                    }

                    if ( Main.getInstance().getNettyClient().isConnected() ) {
                        if ( subject == null ) {
                            // Create
                            Main.getInstance().getNettyClient().sendPacket( new SubjectCreatePacket( Main.getInstance().getAttributes().getUser().getId(),
                                    Main.getInstance().getAttributes().getUser().getClassInfo().getClassId(), txtSubject.getText(), txtSubjectShort.getText().toUpperCase() ) );
                            return;
                        }
                        // Update
                        Main.getInstance().getNettyClient().sendPacket( new SubjectUpdatePacket( Main.getInstance().getAttributes().getUser().getId(),
                                subject.getSubjectId(), txtSubject.getText(), txtSubjectShort.getText() ) );
                    } else {
                        presenterFunctions.showMessageDialog( "Fehler", "Zurzeit besteht keine Verbindung zum Server!" );
                    }
                } )
                .addButton( "Abbrechen", true, e -> {
                } )
                .show();
    }

    public static void showSubjectEditOrDeleteDialog( PresenterFunctions presenterFunctions, boolean edit ) {
        if ( Main.getInstance().getAttributes().getUser().getClassInfo() == null ) {
            presenterFunctions.showMessageDialog( "Fehler", "Du musst zuerst eine Klasse beitreten oder erstellen!" );
            return;
        }

        JFXComboBox<String> cbxSubject = new JFXComboBox<>();
        cbxSubject.setPromptText( "Fach" );
        cbxSubject.getItems().addAll( Main.getInstance().getAttributes().getUser().getSubjects().stream().map( Subject::getName ).collect( Collectors.toList() ) );
        if ( cbxSubject.getItems().isEmpty() ) {
            cbxSubject.getItems().add( "Keine Fächer gefunden!" );
        }
        cbxSubject.setValue( cbxSubject.getItems().get( 0 ) );

        presenterFunctions.getDialog().setTitle( edit ? "Fach bearbeiten" : "Fach löschen" )
                .addContentNode( cbxSubject )
                .addButton( edit ? "Bearbeiten" : "Löschen", true, e -> {
                    if ( cbxSubject.getValue() == null || cbxSubject.getValue().isEmpty()
                            || "Keine Fächer gefunden!".equals( cbxSubject.getValue() ) ) {
                        e.consume();
                        presenterFunctions.showMessageDialog( "Fehler", "Es wurde kein Fach ausgewählt!" );
                        return;
                    }
                    Optional<Subject> optional = Main.getInstance().getAttributes().getUser().getSubjectByName( cbxSubject.getValue() );
                    if ( !optional.isPresent() ) {
                        presenterFunctions.showMessageDialog( "Fehler", "Das Fach konnte nicht gefunden werden!" );
                        return;
                    }
                    if ( edit ) {
                        showSubjectAddOrUpdateDialog( presenterFunctions, optional.get() );
                    } else {
                        if ( Main.getInstance().getNettyClient().isConnected() ) {
                            Main.getInstance().getNettyClient().sendPacket( new SubjectDeletePacket( Main.getInstance().getAttributes().getUser().getId(),
                                    optional.get().getSubjectId() ) );
                        } else {
                            presenterFunctions.showMessageDialog( "Fehler", "Zurzeit besteht keine Verbindung zum Server!" );
                        }
                    }
                } ).addButton( "Abbrechen", true, e -> {
        } ).show();
    }
}
