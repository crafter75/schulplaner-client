package de.csbme.schulplaner.client.builder;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.events.JFXDialogEvent;
import de.csbme.schulplaner.client.utils.WindowUtil;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class DialogBuilder implements Cloneable {

    private JFXDialog dialog;

    private JFXDialogLayout dialogLayout;

    private Label lblHeader;

    private VBox vbxContent;

    private List<JFXButton> buttons;

    private EventHandler<JFXDialogEvent> closeEventHandler;

    private boolean showed;

    public DialogBuilder( StackPane root ) {
        this.dialogLayout = new JFXDialogLayout();
        this.dialog = new JFXDialog( root, this.dialogLayout, JFXDialog.DialogTransition.CENTER );
        this.lblHeader = new Label();
        this.vbxContent = new VBox( 22 );
        this.buttons = new ArrayList<>();
        this.closeEventHandler = null;
        this.showed = false;
    }

    public DialogBuilder setTitle( String title ) {
        this.lblHeader.setText( title );
        return this;
    }

    public DialogBuilder addContentText( String... text ) {
        this.vbxContent.getChildren().addAll( Arrays.stream( text ).map( Label::new ).collect( Collectors.toList() ) );
        return this;
    }

    public DialogBuilder addContentNode( Node... node ) {
        this.vbxContent.getChildren().addAll( node );
        return this;
    }

    public DialogBuilder addCloseEvent( EventHandler<JFXDialogEvent> event ) {
        this.closeEventHandler = event;
        return this;
    }

    public DialogBuilder addButton( String text, boolean closeDialogOnClick, EventHandler<ActionEvent> event ) {
        JFXButton button = new JFXButton( text );
        button.setOnAction( e -> {
            event.handle( e );
            if ( closeDialogOnClick && !e.isConsumed() ) {
                this.dialog.close();
            }
        } );
        this.buttons.add( button );
        return this;
    }

    public void show() {
        StackPane.setAlignment( this.vbxContent, Pos.TOP_CENTER );

        this.dialogLayout.setHeading( this.lblHeader );
        this.dialogLayout.setBody( this.vbxContent );

        if ( this.buttons.size() > 0 ) {
            if ( this.buttons.size() == 1 ) {
                this.dialogLayout.setActions( this.createDialogButton( this.vbxContent, this.buttons.get( 0 ), true ) );
            } else {
                this.dialogLayout.setActions( this.createMultipleDialogButton( this.vbxContent,
                        this.buttons.stream().map( b -> this.createDialogButton( this.vbxContent, b, false ) ).toArray( Node[]::new ) ) );
            }
        }
        if ( this.closeEventHandler != null ) {
            this.dialog.setOnDialogClosed( this.closeEventHandler );
        }
        this.showed = true;
        this.dialog.show();
    }

    public Optional<VBox> getContentNodes() {
        if ( this.showed ) {
            return this.dialogLayout.getBody().stream().filter( n -> n instanceof VBox ).map( n -> (VBox) n ).findFirst();
        }
        return Optional.of( this.vbxContent );
    }

    private AnchorPane createDialogButton( VBox vBox, JFXButton btnDialog, boolean single ) {
        AnchorPane anchButton = new AnchorPane( btnDialog );

        if ( single ) {
            WindowUtil.widthProperty( btnDialog, vBox );
        } else {
            WindowUtil.removeAnchorPadding( btnDialog );
            HBox.setHgrow( anchButton, Priority.ALWAYS );
        }

        btnDialog.getStyleClass().add( "secondaryButton" );

        return anchButton;
    }

    private HBox createMultipleDialogButton( VBox vBox, Node... nodes ) {
        HBox hbxContent = new HBox( nodes );
        hbxContent.setSpacing( 10 );

        WindowUtil.widthProperty( hbxContent, vBox );

        return hbxContent;
    }

    @Override
    protected DialogBuilder clone() throws CloneNotSupportedException {
        return (DialogBuilder) super.clone();
    }

}