package de.csbme.schulplaner.client.utils;

import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.mvc.View;
import de.csbme.schulplaner.client.Main;
import de.csbme.schulplaner.client.types.PlatformType;
import de.csbme.schulplaner.client.types.ViewType;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.layout.*;

public class WindowUtil {

    public static void percentageConstraints(Pane pane, Pane top, Pane center, Pane bottom) {
        pane.widthProperty().addListener((observable, oldValue, newValue) -> {
            double constraints = (newValue.doubleValue() / 100) * 8.5;

            if (Main.getInstance().getAttributes().getPlatform() == PlatformType.DESKTOP) {
                AnchorPane.setLeftAnchor(top, constraints + 100);
                AnchorPane.setLeftAnchor(center, constraints + 100);
                AnchorPane.setLeftAnchor(bottom, constraints + 100);
            } else {
                AnchorPane.setLeftAnchor(top, constraints);
                AnchorPane.setLeftAnchor(center, constraints);
                AnchorPane.setLeftAnchor(bottom, constraints);
            }

            AnchorPane.setRightAnchor(top, constraints);
            AnchorPane.setRightAnchor(center, constraints);
            AnchorPane.setRightAnchor(bottom, constraints);

        });
    }

    public static void percentageConstraints(Pane pane, Pane single) {
        pane.widthProperty().addListener((observable, oldValue, newValue) -> {
            double constraints = (newValue.doubleValue() / 100) * 8.5;

            if (Main.getInstance().getAttributes().getPlatform() == PlatformType.DESKTOP) {
                AnchorPane.setLeftAnchor(single, constraints + 100);
            } else {
                AnchorPane.setLeftAnchor(single, constraints);
            }

        });
    }

    public static void percentageConstraints(Pane pane, Pane top, Pane center) {
        pane.widthProperty().addListener((observable, oldValue, newValue) -> {
            double constraints = (newValue.doubleValue() / 100) * 8.5;

            if (Main.getInstance().getAttributes().getPlatform() == PlatformType.DESKTOP) {
                AnchorPane.setLeftAnchor(top, constraints + 100);
                AnchorPane.setLeftAnchor(center, constraints + 100);
            } else {
                AnchorPane.setLeftAnchor(top, constraints);
                AnchorPane.setLeftAnchor(center, constraints);
            }

            AnchorPane.setRightAnchor(top, constraints);
            AnchorPane.setRightAnchor(center, constraints);

        });
    }

    public static void percentageConstraintsMobile(Pane pane, Pane top, Pane center, Pane bottom) {
        pane.widthProperty().addListener((observable, oldValue, newValue) -> {
            double constraints = (newValue.doubleValue() / 100) * 8.5;

            AnchorPane.setLeftAnchor(top, constraints);
            AnchorPane.setLeftAnchor(center, constraints);
            AnchorPane.setLeftAnchor(bottom, constraints);

            AnchorPane.setRightAnchor(top, constraints);
            AnchorPane.setRightAnchor(center, constraints);
            AnchorPane.setRightAnchor(bottom, constraints);

        });
    }

    public static void percentageConstraintsMobile(Pane pane, Pane top, Pane center) {
        pane.widthProperty().addListener((observable, oldValue, newValue) -> {
            double constraints = (newValue.doubleValue() / 100) * 8.5;

            AnchorPane.setLeftAnchor(top, constraints);
            AnchorPane.setLeftAnchor(center, constraints);

            AnchorPane.setRightAnchor(top, constraints);
            AnchorPane.setRightAnchor(center, constraints);

        });
    }

    public static void disableAppBar(View view) {
        view.showingProperty().addListener((obs, oldValue, newValue) -> {
            if (newValue) {
                AppBar appBar = MobileApplication.getInstance().getAppBar();
                appBar.setVisible(false);
            }
        });
    }

    public static void widthProperty(Control control, VBox vBox) {
        control.minWidthProperty().bind(vBox.widthProperty());
        control.maxWidthProperty().bind(vBox.widthProperty());
    }

    public static void widthProperty(Pane pane, VBox vBox) {
        pane.minWidthProperty().bind(vBox.widthProperty());
        pane.maxWidthProperty().bind(vBox.widthProperty());
    }

    public static void removeAnchorPadding(Node node) {
        AnchorPane.setLeftAnchor(node, 0D);
        AnchorPane.setRightAnchor(node, 0D);
    }

}