package de.csbme.schulplaner.client.components;

import de.csbme.schulplaner.client.manager.AppViewManager;
import de.csbme.schulplaner.client.types.ViewType;
import javafx.geometry.*;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.shape.SVGPath;

public class NavigationComponent extends StackPane {

    public NavigationComponent() {
        HBox hbxBar = new HBox();
        hbxBar.getStyleClass().add("navigationBar");

        StackPane.setAlignment(hbxBar, Pos.BOTTOM_CENTER);

        HBox hbxLeft = new HBox(navigationButton("Stundenplan", ViewType.TIMETABLE), navigationButton("Einträge", ViewType.ENTRIES));

        HBox hbxCenter = new HBox();
        hbxCenter.setMinWidth(108);

        HBox hbxRight = new HBox(navigationButton("Notizen", ViewType.NOTES), navigationButton("Noten", ViewType.GRADES));

        hbxBar.getChildren().addAll(hbxLeft, hbxCenter, hbxRight);

        SVGPath icoWave = new SVGPath();
        icoWave.setContent("M85.241,7.9C81.005,13.2,80.81,21.162,80.81,30.84c0,18.259,5.619,29.906-6.967,42.613s-25.119,8.215-43.379,8.215c-8.381,0-16.184-.3-21.314,4.9C3.255,89.3,89.208,2.869,85.241,7.9Z");
        icoWave.setRotate(46);
        icoWave.getStyleClass().add("navigationBarIcon");
        StackPane.setMargin(icoWave, new Insets(0, 0, 15, 0));

        SVGPath icoHome = new SVGPath();
        icoHome.setContent(ViewType.HOME.getPath());
        icoHome.getStyleClass().add("navigationBarIcon");

        StackPane stckHome = new StackPane(icoHome);
        stckHome.setOnMouseClicked(e -> AppViewManager.changeView(AppViewManager.HOME_VIEW));
        stckHome.getStyleClass().add("homeIcon");

        StackPane.setMargin(stckHome, new Insets(0, 0, 26, 0));

        AnchorPane.setLeftAnchor(this, 0D);
        AnchorPane.setRightAnchor(this, 0D);

        this.getChildren().addAll(hbxBar, icoWave, stckHome);
    }

    private VBox navigationButton(String title, ViewType type) {
        VBox vBox = new VBox(4);
        vBox.getStyleClass().add("navigationButton");

        SVGPath icon = new SVGPath();
        icon.setContent(type.getPath());
        icon.getStyleClass().add("navigationBarIcon");

        vBox.setOnMouseClicked(e -> AppViewManager.changeView(type.getView()));
        vBox.getStyleClass().add("hand");

        Label lblTitle = new Label(title);
        lblTitle.getStyleClass().add("fs-8");

        if (title.equalsIgnoreCase("Stundenplan")) {
            HBox.setMargin(vBox, new Insets(0, 20, 0, 8));
        } else if (title.equalsIgnoreCase("Noten")) {
            HBox.setMargin(vBox, new Insets(0, 8, 0, 20));
        }

        vBox.getChildren().addAll(icon, lblTitle);

        return vBox;
    }

}