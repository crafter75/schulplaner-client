package de.csbme.schulplaner.client.types;

public enum  PlatformType {

    DESKTOP,
    MOBILE,
    UNKNOWN;
}
