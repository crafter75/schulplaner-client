package de.csbme.schulplaner.client.views;

import com.gluonhq.charm.glisten.afterburner.GluonPresenter;
import com.gluonhq.charm.glisten.animation.FlipInXTransition;
import com.gluonhq.charm.glisten.animation.NoTransition;
import com.gluonhq.charm.glisten.mvc.View;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import de.csbme.schulplaner.client.Main;
import de.csbme.schulplaner.client.User;
import de.csbme.schulplaner.client.builder.DialogBuilder;
import de.csbme.schulplaner.client.builder.HeaderBuilder;
import de.csbme.schulplaner.client.components.CheckBoxComponent;
import de.csbme.schulplaner.client.components.NavigationComponent;
import de.csbme.schulplaner.client.components.SidebarNavigationComponent;
import de.csbme.schulplaner.client.manager.AppViewManager;
import de.csbme.schulplaner.client.types.CrudIconType;
import de.csbme.schulplaner.client.types.PlatformType;
import de.csbme.schulplaner.client.utils.ComponentUtil;
import de.csbme.schulplaner.client.utils.WindowUtil;
import de.csbme.schulplaner.lib.network.packets.quiz.QuizCreateUpdatePacket;
import de.csbme.schulplaner.lib.network.packets.quiz.QuizDeletePacket;
import de.csbme.schulplaner.lib.network.packets.quiz.QuizRequestPacket;
import de.csbme.schulplaner.lib.network.packets.quiz.QuizSearchPacket;
import de.csbme.schulplaner.lib.object.Quiz;
import de.csbme.schulplaner.lib.user.Settings;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.util.StringConverter;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.util.List;
import java.util.stream.Collectors;

public class QuizPresenter extends GluonPresenter<Main> implements PresenterFunctions {

    @FXML
    private View root;

    @FXML
    private HBox hbxTop, hbxExit;

    @FXML
    private VBox vbxCenter, vbxSidebar, vbxLeftbar;

    @FXML
    private StackPane stckCenter;

    @FXML
    private FlowPane fpOwn, fpPresets;

    @FXML
    private AnchorPane anchBottom;

    private DialogBuilder openDialog;

    private double minWidth, maxWidth, minHeight, maxHeight;

    private Insets insets = null;

    public void initialize() {
        WindowUtil.disableAppBar(root);
        WindowUtil.percentageConstraints(root, hbxTop, stckCenter);

        root.setShowTransitionFactory(view -> {
            if (Main.getInstance().getAttributes().getUser().getSettings().get(Settings.Key.ANIMATION, Boolean.class)) {
                return new FlipInXTransition(stckCenter);
            }
            return new NoTransition();
        });

        if (Main.getInstance().getAttributes().getPlatform() == PlatformType.MOBILE) {
            anchBottom.getChildren().add(new NavigationComponent());
        } else {
            new SidebarNavigationComponent(vbxSidebar, vbxLeftbar, hbxExit);
        }
    }

    @Override
    public void refresh() {
        // Clear old nodes
        hbxTop.getChildren().clear();
        fpOwn.getChildren().clear();
        fpPresets.getChildren().clear();

        if (Main.getInstance().getNettyClient().isConnected()) {
            User user = Main.getInstance().getAttributes().getUser();
            if (!user.isLocal()) {
                Main.getInstance().getNettyClient().sendPacket(new QuizRequestPacket(user.getId()));
                return;
            }
        }
        this.updateQuizzes();
    }

    @Override
    public StackPane getCenterPane() {
        return this.stckCenter;
    }

    public void updateQuizzes() {
        // Clear old nodes
        hbxTop.getChildren().clear();
        fpOwn.getChildren().clear();
        fpPresets.getChildren().clear();

        if(this.insets == null) {
            this.minWidth = fpOwn.getMinWidth();
            this.minHeight = fpOwn.getMinHeight();
            this.maxWidth = fpOwn.getMaxWidth();
            this.maxHeight = fpOwn.getMaxHeight();
            this.insets = fpPresets.getPadding();
        }

        // Get quizzes
        List<Quiz> quizzes = Main.getInstance().getAttributes().getUser().getQuizzes();

        // Create box for all quizzes
        quizzes.forEach(this::addQuizBox);

        // Check if user has own quizzes
        if (fpOwn.getChildren().isEmpty()) {
            fpOwn.setMinWidth(0);
            fpOwn.setMinHeight(0);
            fpOwn.setMaxWidth(0);
            fpOwn.setMaxHeight(0);
            fpOwn.setVisible(false);

            fpPresets.setPadding(new Insets(0, 4, 0, 4));
        } else {
            fpOwn.setMinWidth(this.minWidth);
            fpOwn.setMinHeight(this.minHeight);
            fpOwn.setMaxWidth(this.maxWidth);
            fpOwn.setMaxHeight(this.maxHeight);
            fpOwn.setVisible(true);
            fpPresets.setPadding(this.insets);
        }

        // Set header
        new HeaderBuilder().setTitle("Quizze")
                .addClickableItem("Quiz hinzufügen", e -> this.showQuizDialog(null))
                .addClickableItem("Quiz-ID suchen", e -> {
                    JFXTextField txtQuizId = new JFXTextField();
                    txtQuizId.setPromptText("Quiz-ID");
                    txtQuizId.textProperty().addListener((observable, oldValue, newValue) -> {
                        if (!newValue.matches("\\d*")) {
                            newValue = newValue.replaceAll("[^\\d]", "");
                        }
                        if (newValue.length() > 5) {
                            newValue = newValue.substring(0, 5);
                        }
                        txtQuizId.setText(newValue);
                    });

                    this.getDialog().setTitle("Quiz suchen").addContentNode(txtQuizId).addButton("Suchen", true, e2 -> {
                        if (txtQuizId.getText() == null || txtQuizId.getText().isEmpty()) {
                            e.consume();
                            this.showMessageDialog("Fehler", "Bitte fülle alle Felder aus!");
                            return;
                        }
                        if (Main.getInstance().getNettyClient().isConnected()) {
                            Main.getInstance().getNettyClient().sendPacket(new QuizSearchPacket(Main.getInstance().getAttributes().getUser().getId(), Integer.parseInt(txtQuizId.getText())));
                        } else {
                            this.showMessageDialog("Fehler", "Zurzeit besteht keine Verbindung zum Server!");
                        }
                    }).addButton("Schließen", true, e2 -> {
                    }).show();
                }).addChangeViewButton(AppViewManager.HOME_VIEW)
                .addNotificationAmount((int) quizzes.stream().filter(q -> q.getCreatedBy() == -1 || q.getCreatedBy() ==
                        Main.getInstance().getAttributes().getUser().getId() || q.isPublicQuiz()).count())
                .show(hbxTop);
    }

    private void addQuizBox(Quiz quiz) {
        Label lblTitle = new Label(quiz.getName());
        Label lblDifficulty = new Label(quiz.getDescription());

        StackPane stckLine = new StackPane();
        stckLine.getStyleClass().add("quizLine");

        lblTitle.getStyleClass().add("fs-22");
        lblDifficulty.getStyleClass().add("quizSub");

        VBox vbxContent = new VBox(lblTitle, stckLine, lblDifficulty);

        if (Main.getInstance().getAttributes().getUser().getId() == quiz.getCreatedBy()) {
            Label crudPrivate = ComponentUtil.crudIcon(CrudIconType.PRIVATE);
            crudPrivate.setOnMouseClicked(e -> {
                CheckBoxComponent checkBoxComponent = new CheckBoxComponent("Öffentlich?");
                checkBoxComponent.setSelected(quiz.isPublicQuiz());
                this.getDialog().setTitle("Quiz Sichtbarkeit").addContentNode(checkBoxComponent)
                        .addButton("Speichern", true, e2 -> {
                            quiz.setPublicQuiz(checkBoxComponent.isSelected());
                            if (Main.getInstance().getNettyClient().isConnected()) {
                                Main.getInstance().getNettyClient().sendPacket(new QuizCreateUpdatePacket(Main.getInstance().getAttributes().getUser().getId(), quiz));
                            } else {
                                this.showMessageDialog("Fehler", "Zurzeit besteht keine Verbindung zum Server!");
                            }
                        }).addButton("Schließen", true, e2 -> {

                }).show();
            });
            StackPane.setAlignment(crudPrivate, Pos.TOP_LEFT);

            Label crudEdit = ComponentUtil.crudIcon(CrudIconType.EDIT);
            crudEdit.setOnMouseClicked(e -> this.showQuizDialog(quiz));
            StackPane.setAlignment(crudEdit, Pos.TOP_RIGHT);

            Label crudDelete = ComponentUtil.crudIcon(CrudIconType.DELETE);
            crudDelete.setOnMouseClicked(e -> {
                if (Main.getInstance().getNettyClient().isConnected()) {
                    Main.getInstance().getNettyClient().sendPacket(new QuizDeletePacket(Main.getInstance().getAttributes().getUser().getId(), quiz.getQuizId()));
                } else {
                    this.showMessageDialog("Fehler", "Zurzeit besteht keine Verbindung zum Server!");
                }
            });
            StackPane.setAlignment(crudDelete, Pos.BOTTOM_RIGHT);

            Label crudCopy = ComponentUtil.crudIcon(CrudIconType.COPY);
            crudCopy.setOnMouseClicked(e -> {
                Label lblName = new Label("Quiz-ID: " + quiz.getPublicQuizId());
                this.getDialog().setTitle("Quiz-ID kopieren").addContentNode(lblName).addButton("Kopieren", true, e2 -> {
                    StringSelection stringSelection = new StringSelection(quiz.getPublicQuizId() + "");
                    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                    clipboard.setContents(stringSelection, null);
                }).addButton("Schließen", true, e2 -> {
                }).show();
            });
            StackPane.setAlignment(crudCopy, Pos.BOTTOM_LEFT);

            vbxContent.setAlignment(Pos.CENTER);
            StackPane stckCrudOverlay = new StackPane(vbxContent, crudPrivate, crudEdit, crudDelete, crudCopy);
            stckCrudOverlay.getStyleClass().add("quizBox");
            stckCrudOverlay.setPadding(new Insets(9));

            vbxContent.setOnMouseClicked(e -> {
                Main.getInstance().getAttributes().getUser().setCurrentQuiz(quiz);
                AppViewManager.changeView(AppViewManager.QUIZ_QUESTIONS_VIEW);
            });
            fpOwn.getChildren().add(stckCrudOverlay);
        } else if (quiz.getCreatedBy() == -1) {
            vbxContent.getStyleClass().add("quizBox");
            vbxContent.setOnMouseClicked(e -> {
                Main.getInstance().getAttributes().getUser().setCurrentQuiz(quiz);
                AppViewManager.changeView(AppViewManager.QUIZ_QUESTIONS_VIEW);
            });
            fpPresets.getChildren().add(vbxContent);
        }

    }

    private void showQuizDialog(final Quiz quiz) {
        if (this.openDialog != null) {
            return;
        }
        JFXTextField txtName = new JFXTextField();
        txtName.setPromptText("Name");
        if (quiz != null) {
            txtName.setText(quiz.getName());
        }

        JFXTextField txtDescription = new JFXTextField();
        txtDescription.setPromptText("Beschreibung");
        if (quiz != null) {
            txtDescription.setText(quiz.getDescription());
        }

        CheckBoxComponent checkBoxComponent = new CheckBoxComponent("Öffentlich?");
        if (quiz != null) {
            checkBoxComponent.setSelected(quiz.isPublicQuiz());
        }

        DialogBuilder dialog = this.getDialog().setTitle(quiz == null ? "Quiz erstellen" : "Quiz bearbeiten")
                .addContentNode(txtName, txtDescription, checkBoxComponent);

        dialog.addCloseEvent(e -> {
            if (this.openDialog != null && this.openDialog.equals(dialog)) {
                this.openDialog = null;
            }
        });

        if (quiz != null && !quiz.getQuestions().isEmpty()) {
            JFXComboBox<Quiz.Question> cbxQuestions = new JFXComboBox<>();
            cbxQuestions.setPromptText("Fragen");
            cbxQuestions.getItems().addAll(quiz.getQuestions().stream().filter(p -> p.getQuestion() != null).collect(Collectors.toList()));
            cbxQuestions.setValue(cbxQuestions.getItems().get(0));
            cbxQuestions.setConverter(new StringConverter<Quiz.Question>() {
                @Override
                public String toString(Quiz.Question question) {
                    return question.getQuestion();
                }

                @Override
                public Quiz.Question fromString(String string) {
                    return quiz.getQuestions().stream().filter(q -> q.getQuestion().equals(string)).findFirst().orElse(null);
                }
            });

            JFXButton edit = new JFXButton("Frage bearbeiten");
            edit.setOnAction(e -> this.showQuestionDialog(quiz, cbxQuestions.getValue()));
            edit.getStyleClass().add("secondaryButton");

            JFXButton delete = new JFXButton("Frage löschen");
            delete.setOnAction(e -> {
                cbxQuestions.getValue().setQuestion(null);
                cbxQuestions.getItems().clear();
                cbxQuestions.getItems().addAll(quiz.getQuestions().stream().filter(p -> p.getQuestion() != null).collect(Collectors.toList()));
                if (cbxQuestions.getItems().isEmpty()) {
                    dialog.getContentNodes().ifPresent(vBox -> vBox.getChildren().removeIf(node -> {
                        if (node instanceof HBox) {
                            return ((HBox) node).getChildren().size() == 2 &&
                                    ((HBox) node).getChildren().stream().filter(n -> n instanceof JFXButton).count() == 2;
                        } else if (node instanceof JFXComboBox) {
                            return ((JFXComboBox) node).getPromptText().equals("Fragen");
                        }
                        return false;
                    }));
                    return;
                }
                cbxQuestions.setValue(cbxQuestions.getItems().get(0));
            });
            delete.getStyleClass().add("secondaryButton");

            HBox buttonBox = new HBox();
            buttonBox.setSpacing(10);
            buttonBox.getChildren().addAll(edit, delete);

            dialog.addContentNode(cbxQuestions, buttonBox);
        }

        if (quiz != null) {
            dialog.addButton("Frage hinzufügen", false, e -> this.showQuestionDialog(quiz, null));
        }

        dialog.addButton("Speichern", true, e -> {
            Quiz newQuiz = quiz;
            if (txtName.getText() == null || txtName.getText().isEmpty()
                    || txtDescription.getText() == null || txtDescription.getText().isEmpty()) {
                e.consume();
                this.showMessageDialog("Fehler", "Bitte fülle alle Felder aus!");
                return;
            }

            if (newQuiz == null) {
                newQuiz = new Quiz(txtName.getText(), txtDescription.getText(), Main.getInstance().getAttributes().getUser().getId(),
                        checkBoxComponent.isSelected());
            } else {
                newQuiz.setName(txtName.getText());
                newQuiz.setDescription(txtDescription.getText());
                newQuiz.setPublicQuiz(checkBoxComponent.isSelected());
            }

            if (Main.getInstance().getNettyClient().isConnected()) {
                Main.getInstance().getNettyClient().sendPacket(new QuizCreateUpdatePacket(Main.getInstance().getAttributes().getUser().getId(), newQuiz));
            } else {
                this.showMessageDialog("Fehler", "Zurzeit besteht keine Verbindung zum Server!");
            }
        }).addButton("Schließen", true, e -> {
        }).show();
    }

    private void showQuestionDialog(Quiz quiz, final Quiz.Question question) {
        JFXTextField txtQuestion = new JFXTextField();
        txtQuestion.setPromptText("Frage");
        if (question != null) {
            txtQuestion.setText(question.getQuestion());
        }

        JFXTextField txtAnswer1 = new JFXTextField();
        txtAnswer1.setPromptText("Antwort #1");
        if (question != null) {
            txtAnswer1.setText(question.getAnswer1());
        }

        JFXTextField txtAnswer2 = new JFXTextField();
        txtAnswer2.setPromptText("Antwort #2");
        if (question != null) {
            txtAnswer2.setText(question.getAnswer2());
        }

        JFXTextField txtAnswer3 = new JFXTextField();
        txtAnswer3.setPromptText("Antwort #3");
        if (question != null) {
            txtAnswer3.setText(question.getAnswer3());
        }

        JFXTextField txtAnswer4 = new JFXTextField();
        txtAnswer4.setPromptText("Antwort #4");
        if (question != null) {
            txtAnswer4.setText(question.getAnswer4());
        }

        JFXComboBox<Integer> cboRightAnswer = new JFXComboBox<>();
        cboRightAnswer.setPromptText("Richtige Antwort");
        cboRightAnswer.getItems().addAll(1, 2, 3, 4);
        cboRightAnswer.setValue(cboRightAnswer.getItems().get(0));

        this.getDialog().setTitle(question == null ? "Frage hinzufügen" : "Frage bearbeiten")
                .addContentNode(txtQuestion, txtAnswer1, txtAnswer2, txtAnswer3, txtAnswer4, cboRightAnswer)
                .addButton("Speichern", true, e -> {
                    if (txtQuestion.getText() == null || txtQuestion.getText().isEmpty()
                            || txtAnswer1.getText() == null || txtAnswer1.getText().isEmpty()
                            || txtAnswer2.getText() == null || txtAnswer2.getText().isEmpty()
                            || txtAnswer3.getText() == null || txtAnswer3.getText().isEmpty()
                            || txtAnswer4.getText() == null || txtAnswer4.getText().isEmpty()
                            || cboRightAnswer.getValue() == null) {
                        e.consume();
                        this.showMessageDialog("Fehler", "Bitte fülle alle Felder aus!");
                        return;
                    }
                    Quiz.Question newQuestion = question;
                    if (newQuestion == null) {
                        newQuestion = new Quiz.Question(txtQuestion.getText(), txtAnswer1.getText(), txtAnswer2.getText(),
                                txtAnswer3.getText(), txtAnswer4.getText(), cboRightAnswer.getValue());
                    } else {
                        newQuestion.setQuestion(txtQuestion.getText());
                        newQuestion.setAnswer1(txtAnswer1.getText());
                        newQuestion.setAnswer2(txtAnswer2.getText());
                        newQuestion.setAnswer3(txtAnswer3.getText());
                        newQuestion.setAnswer4(txtAnswer4.getText());
                        newQuestion.setRightAnswer(cboRightAnswer.getValue());
                        final int id = newQuestion.getQuestionId();
                        quiz.getQuestions().removeIf(q -> q.getQuestionId() == id);
                    }
                    quiz.getQuestions().add(newQuestion);
                }).addButton("Schließen", true, e -> {
        }).show();
    }
}