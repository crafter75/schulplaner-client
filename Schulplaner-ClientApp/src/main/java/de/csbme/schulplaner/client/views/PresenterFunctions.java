package de.csbme.schulplaner.client.views;

import de.csbme.schulplaner.client.builder.DialogBuilder;
import javafx.scene.layout.StackPane;

public interface PresenterFunctions {

    void refresh();

    StackPane getCenterPane();

    default DialogBuilder getDialog() {
        return new DialogBuilder( this.getCenterPane() );
    }

    default void showMessageDialog( String title, String... content ) {
        this.getDialog().setTitle( title ).addContentText( content ).addButton( "Schließen", true, e -> {
        } ).show();
    }
}
