package de.csbme.schulplaner.client.components.colorpicker;

import com.jfoenix.controls.JFXTextField;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Side;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.SVGPath;

public class ColorPickerFieldComponent extends HBox {

    private final ObjectProperty<Color> sceneColorProperty = new SimpleObjectProperty<>( Color.WHITE );

    public ColorPickerFieldComponent() {
        JFXTextField txtInput = new JFXTextField();
        txtInput.setPromptText( "Farbe" );
        txtInput.setEditable( false );

        SVGPath icon = new SVGPath();
        icon.getStyleClass().add( "icon" );
        icon.setContent( "M21.734,41.293a.308.308,0,0,0-.537-.066,2.718,2.718,0,0,1-1.492.938,5.578,5.578,0,0,0-1.5.728,1.917,1.917,0,0,0-.927,1.979l-.018.024a10.12,10.12,0,0,0-1.019-1.257A9.525,9.525,0,0,0,9.75,40.729c-.107,0-.216-.006-.323-.006A9.408,9.408,0,0,0,2.3,44.051a9.365,9.365,0,0,0-2.2,7.515c.034.223.077.449.128.671a8.367,8.367,0,0,0,4.676,6.13,4.427,4.427,0,0,0,1.7.356,3.79,3.79,0,0,0,3.345-2.048,4.431,4.431,0,0,0,.532-1.523,3.675,3.675,0,0,0-.015-.9,3.393,3.393,0,0,1-.013-.878A1.176,1.176,0,0,1,11.7,52.317a1.9,1.9,0,0,1,.578.093c-.227.416-.433.814-.613,1.183a12.009,12.009,0,0,0-.783,1.923c-.085.3-.311,1.115.24,1.5a.823.823,0,0,0,.627.121c.208-.039.841-.156,2.638-2.2.482-.549,1-1.185,1.546-1.889A4.187,4.187,0,0,0,18.7,50.059a4.4,4.4,0,0,0,.089-1.069c.522-.807.98-1.56,1.441-2.37.009-.016.018-.032.026-.049a2.541,2.541,0,0,0,1.584-1.432A4.9,4.9,0,0,0,21.734,41.293Zm-7.542,5.033a1.314,1.314,0,1,1,.169-.991A1.308,1.308,0,0,1,14.192,46.326Zm1.705,2.38c.664-.979,1.367-1.951,2.034-2.817a3.6,3.6,0,0,0,.36.284,2.768,2.768,0,0,0,.676.347c-.563.963-1.169,1.917-1.9,3a49.307,49.307,0,0,1-3.527,4.643,13.6,13.6,0,0,1-1.524,1.532,13.747,13.747,0,0,1,.867-1.99A49.459,49.459,0,0,1,15.9,48.706ZM4.237,48.7a1.314,1.314,0,1,1,1.113-.614A1.309,1.309,0,0,1,4.237,48.7ZM2.86,51.59a1.314,1.314,0,1,1-.169.991A1.308,1.308,0,0,1,2.86,51.59Zm4.207-7.8a1.314,1.314,0,1,1-.169.991A1.308,1.308,0,0,1,7.067,43.788Z" );

        icon.setOnMouseClicked( e -> {
            ColorPickerComponent myCustomColorPicker = new ColorPickerComponent( color -> txtInput.setText( hexReady( String.valueOf( color ) ) ) );
            myCustomColorPicker.setCurrentColor( sceneColorProperty.get() );

            CustomMenuItem itemColor = new CustomMenuItem( myCustomColorPicker );
            itemColor.setHideOnClick( false );
            sceneColorProperty.bind( myCustomColorPicker.customColorProperty() );

            ContextMenu contextMenu = new ContextMenu( itemColor );
            contextMenu.getStyleClass().add( "transparent-context-menu" );
            contextMenu.setOnHiding( t -> sceneColorProperty.unbind() );
            contextMenu.show( txtInput, Side.BOTTOM, 0, 0 );

            /*myCustomColorPicker.setOnMouseMoved( ev -> {
                txtInput.setText( hexReady( String.valueOf( myCustomColorPicker.getCurrentColor() ) ) );
            } );*/
        } );


        this.getChildren().addAll( txtInput, icon );
    }

    public static String hexReady( String str ) {
        String result = null;
        if ( ( str != null ) && ( str.length() > 0 ) ) {
            result = str.substring( 2, str.length() - 2 );
        }
        return "#" + result;
    }

}