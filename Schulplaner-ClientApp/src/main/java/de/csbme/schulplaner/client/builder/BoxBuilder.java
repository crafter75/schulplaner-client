package de.csbme.schulplaner.client.builder;

import de.csbme.schulplaner.client.Main;
import de.csbme.schulplaner.client.manager.AppViewManager;
import de.csbme.schulplaner.client.types.CrudIconType;
import de.csbme.schulplaner.client.types.EntriesType;
import de.csbme.schulplaner.client.utils.ComponentUtil;
import de.csbme.schulplaner.client.utils.DateRangeUtil;
import de.csbme.schulplaner.client.utils.HolidayUtil;
import de.csbme.schulplaner.client.views.EntriesPresenter;
import de.csbme.schulplaner.lib.object.Grade;
import de.csbme.schulplaner.lib.object.Note;
import de.csbme.schulplaner.lib.object.Subject;
import de.csbme.schulplaner.lib.time.TimeUtil;
import de.csbme.schulplaner.lib.user.Category;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.shape.SVGPath;

import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.stream.Collectors;

public class BoxBuilder {

    private List<AnchorPane> items;

    public BoxBuilder() {
        this.items = new ArrayList<>();
    }

    public BoxBuilder addHomeBox(EntriesType type, List<Note> notes, EventHandler<MouseEvent> addHandler) {
        VBox vbxContent = new VBox();
        vbxContent.getStyleClass().add("boxContent");

        //new ZoomIn(vbxContent).setDelay(Duration.millis(5)).play();

        StackPane stckTop = new StackPane();
        StackPane stckBottom = new StackPane();

        HBox hbxTop = new HBox(9);
        hbxTop.getStyleClass().add("boxTop");
        hbxTop.setAlignment(Pos.CENTER_LEFT);

        Label lblTitle = new Label();

        Label lblMiddle = new Label();
        lblMiddle.getStyleClass().add("boxMiddle");

        HBox hbxBottom = new HBox();
        hbxBottom.setAlignment(Pos.BOTTOM_LEFT);
        VBox.setMargin(hbxBottom, new Insets(10, 0, 0, 0));

        VBox.setMargin(stckBottom, new Insets(6, 0, 0, 0));

        HBox hbxCrud = new HBox(4);
        hbxCrud.setMaxWidth(24);
        hbxCrud.setMaxHeight(24);
        StackPane.setAlignment(hbxCrud, Pos.BOTTOM_RIGHT);

        SVGPath icon = new SVGPath();
        icon.getStyleClass().add("icon");
        icon.setContent(type.getPath());

        if (type != EntriesType.HOLIDAYS) {
            if (notes.isEmpty()) {
                lblMiddle.setText("Keine Sorge! Es stehen momentan keine " + type.getName() + " an.");
            } else {
                OptionalLong minTimestamp = notes.stream().mapToLong(Note::getExpireDate).min();
                OptionalLong maxTimestamp = notes.stream().mapToLong(Note::getExpireDate).max();
                if (minTimestamp.isPresent() && maxTimestamp.isPresent()) {
                    lblMiddle.setText(DateRangeUtil.getDateRange(minTimestamp.getAsLong(), maxTimestamp.getAsLong()));
                }

                notes.forEach(n -> {
                    Main.getInstance().getAttributes().getUser().getSubjectById(n.getSubjectId()).ifPresent(s -> {
                        Label lblSubject = new Label(s.getShortName());
                        lblSubject.getStyleClass().add("boxCircle");
                        lblSubject.setAlignment(Pos.CENTER);
                        lblSubject.setOnMouseClicked(e -> {
                            AppViewManager.changeView(AppViewManager.ENTRIES_VIEW);
                            if (AppViewManager.getCurrentPresenter() instanceof EntriesPresenter) {
                                ((EntriesPresenter) AppViewManager.getCurrentPresenter()).showNoteDialog(null, n, new int[0]);
                            }
                        });
                        HBox.setMargin(lblSubject, new Insets(0, 4, 0, 0));
                        hbxBottom.getChildren().add(lblSubject);
                    });
                });
            }
        } else {
            HolidayUtil.getNextTwoHolidays("NW", (holiday, holiday2) -> {
                LocalDate currentDate = LocalDate.now();
                String text = "";
                if (holiday != null) {
                    text += this.getHolidayLine(holiday, currentDate);
                }
                if (holiday2 != null) {
                    if (!text.isEmpty()) {
                        text += "\n";
                    }
                    text += this.getHolidayLine(holiday2, currentDate);
                }
                if (text.isEmpty()) {
                    text += "Es konnten keine Ferien abgerufen werden!";
                }
                lblMiddle.setText(text);
            }); // TODO allow more states (Current: North Rhine-Westphalia)
        }
        lblTitle.setText("Anstehende " + type.getName());

        hbxTop.getChildren().addAll(icon, lblTitle);

        SVGPath icoArrow = new SVGPath();
        icoArrow.setContent("M2462.656-371.929l-4.267-4.268-4.268,4.268a1,1,0,0,1-1.414,0,1,1,0,0,1,0-1.415l4.95-4.949a1,1,0,0,1,.732-.293,1,1,0,0,1,.732.293l4.95,4.949a1,1,0,0,1,0,1.415,1,1,0,0,1-.707.293A1,1,0,0,1,2462.656-371.929Z");
        icoArrow.getStyleClass().add("icon");

        StackPane stckArrow = new StackPane(icoArrow);
        stckArrow.setMaxHeight(7);
        stckArrow.setMaxWidth(12);
        stckArrow.getStyleClass().add("hand");

        double minHeight = vbxContent.getMinHeight();
        double maxHeight = vbxContent.getMaxHeight();
        stckArrow.setOnMouseClicked(e -> {
            icoArrow.setRotate(icoArrow.getRotate() + 180);
            if (stckBottom.getChildren().isEmpty()) {
                vbxContent.setMaxHeight(maxHeight);
                vbxContent.setMinHeight(minHeight);
                stckBottom.getChildren().addAll(new VBox(lblMiddle, hbxBottom), hbxCrud);
            } else {
                vbxContent.setMaxHeight(50);
                vbxContent.setMinHeight(50);
                stckBottom.getChildren().clear();
            }
        });

        StackPane.setAlignment(stckArrow, Pos.CENTER_RIGHT);

        stckTop.getChildren().addAll(hbxTop, stckArrow);

        if(type != EntriesType.HOLIDAYS) {
            Label lblIcon = ComponentUtil.crudIcon(CrudIconType.ADD);
            lblIcon.setOnMouseClicked(addHandler);
            hbxCrud.getChildren().addAll(lblIcon);
        }

        stckBottom.getChildren().addAll(new VBox(lblMiddle, hbxBottom), hbxCrud);

        vbxContent.getChildren().addAll(stckTop, stckBottom);

        AnchorPane anchContent = new AnchorPane(vbxContent);
        AnchorPane.setLeftAnchor(vbxContent, 10D);
        AnchorPane.setRightAnchor(vbxContent, 10D);

        this.items.add(anchContent);
        return this;
    }

    public BoxBuilder addNoteBox(Note note, EventHandler<MouseEvent> editHandler, EventHandler<MouseEvent> deleteHandler) {
        VBox vbxContent = new VBox();
        vbxContent.getStyleClass().add("boxContent");

        //new ZoomIn(vbxContent).setDelay(Duration.millis(5)).play();

        StackPane stckTop = new StackPane();
        StackPane stckBottom = new StackPane();

        HBox hbxTop = new HBox(9);
        hbxTop.getStyleClass().add("boxTop");
        hbxTop.setAlignment(Pos.CENTER_LEFT);

        Label lblTitle = new Label();
        Label lblMiddle = new Label();
        Label lblSubMiddle = new Label();

        HBox hbxBottom = new HBox(4);
        hbxBottom.setAlignment(Pos.BOTTOM_LEFT);
        VBox.setMargin(hbxBottom, new Insets(10, 0, 0, 0));

        VBox.setMargin(stckBottom, new Insets(6, 0, 0, 0));

        HBox hbxCrud = new HBox(4);
        hbxCrud.setMaxWidth(24);
        hbxCrud.setMaxHeight(24);
        StackPane.setAlignment(hbxCrud, Pos.BOTTOM_RIGHT);

        if (note.getCategory() == Category.NOTES.ordinal()) {
            lblTitle.setText(note.getTitle());
            lblTitle.getStyleClass().add("notesTitle");

            lblMiddle.setText(note.getContent());
            lblMiddle.getStyleClass().add("subMiddle");
        } else {
            lblTitle.setText(Category.values()[note.getCategory()].getDisplayName());
            lblTitle.getStyleClass().add("entriesBox");

            Optional<Subject> optionalSubject = Main.getInstance().getAttributes().getUser().getSubjectById(note.getSubjectId());
            lblMiddle.setText(optionalSubject.map(Subject::getName).orElse("Unbekanntes Fach"));
            lblMiddle.getStyleClass().add("boxMiddle");
            lblMiddle.getStyleClass().add("boxMiddleSemi");

            lblSubMiddle.setText(note.getContent());
            lblSubMiddle.getStyleClass().add("subMiddle");
        }

        hbxTop.getChildren().addAll(lblTitle);
        stckTop.getChildren().add(hbxTop);

        Label lblInfo = new Label();
        if (note.isDone()) {
            lblInfo.setText("Erledigt");
        } else {
            lblInfo.setText(note.getExpireDate() == -1L ? "Ohne Erinnerung" : TimeUtil.getDateFromMilliseconds(note.getExpireDate(), TimeUtil.DATE_WITHOUT_SECONDS));
        }
        lblInfo.getStyleClass().add("fs-12");
        StackPane.setAlignment(lblInfo, Pos.CENTER_RIGHT);
        stckTop.getChildren().add(lblInfo);

        Label crudEdit = ComponentUtil.crudIcon(CrudIconType.EDIT);
        crudEdit.setOnMouseClicked(editHandler);

        Label crudDelete = ComponentUtil.crudIcon(CrudIconType.DELETE);
        crudDelete.setOnMouseClicked(deleteHandler);

        hbxCrud.getChildren().addAll(crudEdit, crudDelete);

        if (note.getCategory() == Category.NOTES.ordinal()) {
            stckBottom.getChildren().addAll(new VBox(lblMiddle), hbxCrud);
        } else {
            stckBottom.getChildren().addAll(new VBox(lblMiddle, lblSubMiddle), hbxCrud);
        }

        vbxContent.getChildren().addAll(stckTop, stckBottom);

        AnchorPane anchContent = new AnchorPane(vbxContent);
        AnchorPane.setLeftAnchor(vbxContent, 10D);
        AnchorPane.setRightAnchor(vbxContent, 10D);

        this.items.add(anchContent);
        return this;
    }

    public BoxBuilder addGradesBox(List<Grade> grades, EventHandler<MouseEvent> addHandler, EventHandler<MouseEvent> editHandler, EventHandler<MouseEvent> deleteHandler) {
        VBox vbxContent = new VBox();
        vbxContent.getStyleClass().add("boxContent");

        //new ZoomIn(vbxContent).setDelay(Duration.millis(5)).play();

        Optional<Subject> optionalSubject = Main.getInstance().getAttributes().getUser().getSubjectById(grades.get(0).getSubjectId());
        Label lblSubjectLetter = new Label(optionalSubject.map(subject -> subject.getShortName().toUpperCase()).orElse("UF"));
        lblSubjectLetter.setAlignment(Pos.CENTER);
        HBox.setMargin(lblSubjectLetter, new Insets(0, 12, 0, 0));
        lblSubjectLetter.getStyleClass().add("boxCircleBig");

        Label lblSubject = new Label(optionalSubject.map(Subject::getName).orElse("Unbekanntes Fach"));
        lblSubject.getStyleClass().add("gradeSubject");

        Label lblAverage = new Label("Ø" + this.getAverageFromGrades(grades) + " aus " + grades.size() + " " + (grades.size() == 1 ? "Note" : "Noten"));
        lblAverage.getStyleClass().add("gradeAverage");

        HBox hbxCrud = new HBox(4);
        hbxCrud.setAlignment(Pos.CENTER_RIGHT);

        Label crudAdd = ComponentUtil.crudIcon(CrudIconType.ADD);
        crudAdd.setOnMouseClicked(addHandler);

        Label crudEdit = ComponentUtil.crudIcon(CrudIconType.EDIT);
        crudEdit.setOnMouseClicked(editHandler);

        Label crudDelete = ComponentUtil.crudIcon(CrudIconType.DELETE);
        crudDelete.setOnMouseClicked(deleteHandler);

        hbxCrud.getChildren().addAll(crudAdd, crudEdit, crudDelete);

        HBox.setHgrow(hbxCrud, Priority.ALWAYS);

        HBox hbxContent = new HBox(lblSubjectLetter, new VBox(lblSubject, lblAverage), hbxCrud);

        vbxContent.getChildren().addAll(hbxContent);

        AnchorPane anchContent = new AnchorPane(vbxContent);
        AnchorPane.setLeftAnchor(vbxContent, 10D);
        AnchorPane.setRightAnchor(vbxContent, 10D);

        this.items.add(anchContent);
        return this;
    }

    public void show(VBox vBox) {
        vBox.setSpacing(22);
        vBox.getChildren().addAll(this.items);
    }

    private double getAverageFromGrades(List<Grade> grades) {
        List<Grade> exams = grades.stream().filter(g -> g.getCategory() == Category.EXAM.ordinal()).collect(Collectors.toList());
        double averageExams = 0.0D;
        if (!exams.isEmpty()) {
            averageExams = exams.stream().mapToDouble(Grade::getGrade).sum() / exams.size();
        }

        List<Grade> other = grades.stream().filter(g -> g.getCategory() != Category.EXAM.ordinal()).collect(Collectors.toList());
        double averageOther = 0.0D;
        if (!other.isEmpty()) {
            averageOther = other.stream().mapToDouble(Grade::getGrade).sum() / other.size();
        }
        if (averageExams != 0.0D && averageOther != 0.0D) {
            double result = (averageExams + averageOther) / 2.0D; // Weighting maybe not right :D
            return Math.round(result * 100.0D) / 100.0D;
        }
        return Math.round((averageExams == 0.0D ? averageOther : averageExams) * 100.0D) / 100.0D;
    }

    private String getHolidayLine(HolidayUtil.Holiday holiday, LocalDate currentDate) {
        if (holiday.getStartDate().isBefore(currentDate)) {
            return "Zurzeit sind " + holiday.getDisplayName() + "!";
        } else {
            long days = ChronoUnit.DAYS.between(currentDate, holiday.getStartDate());
            return holiday.getDisplayName() + ": " + days + " " + (days == 1 ? "Tag" : "Tage");
        }
    }
}
