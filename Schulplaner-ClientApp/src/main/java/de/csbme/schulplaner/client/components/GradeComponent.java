package de.csbme.schulplaner.client.components;

import com.jfoenix.controls.JFXComboBox;
import de.csbme.schulplaner.client.types.CrudIconType;
import de.csbme.schulplaner.client.utils.ComponentUtil;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;

public class GradeComponent extends StackPane {

    public GradeComponent() {
        JFXComboBox cbxGrade = new JFXComboBox();
        JFXComboBox cbxType = new JFXComboBox();

        Label lblAdd = new Label("", ComponentUtil.crudIcon(CrudIconType.ADD));

        StackPane.setMargin(lblAdd, new Insets(10, 0, 0, 0));

        HBox hbxContent = new HBox(cbxGrade, cbxType);

        HBox.setHgrow(cbxGrade, Priority.ALWAYS);
        HBox.setHgrow(cbxType, Priority.ALWAYS);

        this.getChildren().addAll(hbxContent, lblAdd);
    }

}