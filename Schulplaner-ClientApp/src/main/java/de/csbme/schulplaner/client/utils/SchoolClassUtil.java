package de.csbme.schulplaner.client.utils;

import com.jfoenix.controls.JFXTextField;
import de.csbme.schulplaner.client.Main;
import de.csbme.schulplaner.client.User;
import de.csbme.schulplaner.client.views.PresenterFunctions;
import de.csbme.schulplaner.lib.network.packets.schoolclass.ClassCreatePacket;
import de.csbme.schulplaner.lib.network.packets.schoolclass.ClassJoinPacket;
import de.csbme.schulplaner.lib.network.packets.schoolclass.ClassLeavePacket;
import de.csbme.schulplaner.lib.network.packets.schoolclass.ClassMemberSizeRequestPacket;
import de.csbme.schulplaner.lib.object.Class;
import javafx.scene.control.Label;

public class SchoolClassUtil {

    public static void showCreateJoinOrViewClassDialog( PresenterFunctions presenterFunctions ) {
        User user = Main.getInstance().getAttributes().getUser();
        if(user == null || user.isLocal()) {
            presenterFunctions.showMessageDialog( "Fehler", "Du musst für diese Funktion eingeloggt sein!" );
            return;
        }
        if ( user.getClassInfo() == null ) {
            // Create or join class
            SchoolClassUtil.showClassCreateOrJoinDialog( presenterFunctions );
            return;
        }
        // Show class details
        if ( Main.getInstance().getNettyClient().isConnected() ) {
            Main.getInstance().getNettyClient().sendPacket( new ClassMemberSizeRequestPacket(
                    Main.getInstance().getAttributes().getUser().getClassInfo().getClassId() ) );
        } else {
            presenterFunctions.showMessageDialog( "Fehler", "Zurzeit besteht keine Verbindung zum Server!" );
        }
    }

    public static void showClassInfoDialog( PresenterFunctions presenterFunctions, long memberSize ) {
        // Show class info
        Class classInfo = Main.getInstance().getAttributes().getUser().getClassInfo();

        presenterFunctions.getDialog().setTitle( "Klassendetails" )
                .addButton( "Klasse verlassen", true, e2 -> {
                    if ( Main.getInstance().getNettyClient().isConnected() ) {
                        Main.getInstance().getNettyClient().sendPacket( new ClassLeavePacket( Main.getInstance().getAttributes().getUser().getId() ) );
                    } else {
                        presenterFunctions.showMessageDialog( "Fehler", "Zurzeit besteht keine Verbindung zum Server!" );
                    }
                } ).addButton( "Schließen", true, e2 -> {
        } ).addContentNode(
                new Label( "Name: " + classInfo.getName() ),
                new Label( "Schule: " + classInfo.getSchoolName() ),
                new Label( "Klassencode: " + classInfo.getClassCode() ),
                new Label( "Mitglieder: " + memberSize )
        ).show();
    }

    private static void showClassCreateOrJoinDialog( PresenterFunctions presenterFunctions ) {
        // Create or join class
        JFXTextField txtClassCode = new JFXTextField();
        txtClassCode.setPromptText( "Klassencode" );
        txtClassCode.textProperty().addListener( ( observable, oldValue, newValue ) -> {
            if ( !newValue.matches( "\\d*" ) ) {
                newValue = newValue.replaceAll( "[^\\d]", "" );
            }
            if ( newValue.length() > 5 ) {
                newValue = newValue.substring( 0, 5 );
            }
            txtClassCode.setText( newValue );
        } );

        presenterFunctions.getDialog().setTitle( "Klasse beitreten/erstellen" )
                .addContentNode( new Label( "Gebe hier einen Klassencode ein, um beizutreten." ), txtClassCode )
                .addButton( "Beitreten", true, e2 -> {
                    if ( txtClassCode.getText() == null || txtClassCode.getText().isEmpty() ) {
                        presenterFunctions.showMessageDialog( "Fehler", "Du musst zum beitreten einen Klassencode angeben!" );
                        e2.consume();
                        return;
                    }
                    int classCode = Integer.parseInt( txtClassCode.getText() );
                    if ( Main.getInstance().getNettyClient().isConnected() ) {
                        Main.getInstance().getNettyClient().sendPacket( new ClassJoinPacket( Main.getInstance().getAttributes().getUser().getId(),
                                classCode ) );
                    } else {
                        presenterFunctions.showMessageDialog( "Fehler", "Zurzeit besteht keine Verbindung zum Server!" );
                    }
                } )
                .addButton( "Klasse erstellen", true, e2 -> {
                    JFXTextField txtName = new JFXTextField();
                    txtName.setPromptText( "Name" );

                    JFXTextField txtSchoolName = new JFXTextField();
                    txtSchoolName.setPromptText( "Schulnamen" );

                    presenterFunctions.getDialog().setTitle( "Klasse erstellen" )
                            .addContentNode( txtName, txtSchoolName )
                            .addButton( "Erstellen", true, e3 -> {
                                if ( Main.getInstance().getNettyClient().isConnected() ) {
                                    Main.getInstance().getNettyClient().sendPacket( new ClassCreatePacket( Main.getInstance().getAttributes().getUser().getId(),
                                            txtName.getText(), txtSchoolName.getText() ) );
                                } else {
                                    presenterFunctions.showMessageDialog( "Fehler", "Zurzeit besteht keine Verbindung zum Server!" );
                                }
                            } )
                            .addButton( "Abbrechen", true, e3 -> {
                            } )
                            .show();
                } )
                .addButton( "Abbrechen", true, e2 -> {
                } )
                .show();
    }
}
