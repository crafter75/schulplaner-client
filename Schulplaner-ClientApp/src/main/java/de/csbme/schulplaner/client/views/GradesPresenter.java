package de.csbme.schulplaner.client.views;

import com.gluonhq.charm.glisten.afterburner.GluonPresenter;
import com.gluonhq.charm.glisten.animation.FlipInXTransition;
import com.gluonhq.charm.glisten.animation.NoTransition;
import com.gluonhq.charm.glisten.mvc.View;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import de.csbme.schulplaner.client.Main;
import de.csbme.schulplaner.client.User;
import de.csbme.schulplaner.client.builder.BoxBuilder;
import de.csbme.schulplaner.client.builder.DialogBuilder;
import de.csbme.schulplaner.client.builder.HeaderBuilder;
import de.csbme.schulplaner.client.components.DatePickerComponent;
import de.csbme.schulplaner.client.components.NavigationComponent;
import de.csbme.schulplaner.client.components.SidebarNavigationComponent;
import de.csbme.schulplaner.client.manager.AppViewManager;
import de.csbme.schulplaner.client.types.PlatformType;
import de.csbme.schulplaner.client.utils.DateRangeUtil;
import de.csbme.schulplaner.client.utils.SchoolClassUtil;
import de.csbme.schulplaner.client.utils.SubjectUtil;
import de.csbme.schulplaner.client.utils.WindowUtil;
import de.csbme.schulplaner.lib.network.packets.grade.GradeDeletePacket;
import de.csbme.schulplaner.lib.network.packets.grade.GradeRequestPacket;
import de.csbme.schulplaner.lib.network.packets.grade.GradeUpdatePacket;
import de.csbme.schulplaner.lib.object.Grade;
import de.csbme.schulplaner.lib.object.Subject;
import de.csbme.schulplaner.lib.time.TimeUtil;
import de.csbme.schulplaner.lib.user.Category;
import de.csbme.schulplaner.lib.user.Settings;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

import java.time.ZoneId;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class GradesPresenter extends GluonPresenter<Main> implements PresenterFunctions {

    @FXML
    private View root;

    @FXML
    private HBox hbxTop, hbxExit;

    @FXML
    private VBox vbxCenter, vbxSidebar, vbxLeftbar;

    @FXML
    private StackPane stckCenter;

    @FXML
    private AnchorPane anchBottom;

    @FXML
    private HBox hbxTopLabel;

    @FXML
    private Label lblPeriod, lblAverage;

    private DialogBuilder openDialog;

    public void initialize() {
        WindowUtil.disableAppBar( root );
        WindowUtil.percentageConstraints( root, hbxTop, stckCenter, hbxTopLabel );

        root.setShowTransitionFactory( view -> {
            if ( Main.getInstance().getAttributes().getUser().getSettings().get( Settings.Key.ANIMATION, Boolean.class ) ) {
                return new FlipInXTransition( stckCenter );
            }
            return new NoTransition();
        } );

        if (Main.getInstance().getAttributes().getPlatform() == PlatformType.MOBILE) {
            anchBottom.getChildren().add(new NavigationComponent());
        } else {
            new SidebarNavigationComponent(vbxSidebar, vbxLeftbar, hbxExit);
        }
    }

    @Override
    public void refresh() {
        // Clear old nodes
        hbxTop.getChildren().clear();
        vbxCenter.getChildren().clear();

        if ( Main.getInstance().getNettyClient().isConnected() ) {
            User user = Main.getInstance().getAttributes().getUser();
            if ( !user.isLocal() ) {
                Main.getInstance().getNettyClient().sendPacket( new GradeRequestPacket( user.getId() ) );
                return;
            }
        }
        this.updateGrades();
    }

    @Override
    public StackPane getCenterPane() {
        return this.stckCenter;
    }

    public void updateGrades() {
        // Clear old nodes
        hbxTop.getChildren().clear();
        vbxCenter.getChildren().clear();

        // Get grades
        List<Grade> grades = Main.getInstance().getAttributes().getUser().getGrades();

        // Update info labels
        if(!grades.isEmpty()) {
            double average = grades.stream().mapToDouble( Grade::getGrade ).sum() / grades.size();
            lblAverage.setText( "Ø" + ( Math.round( average * 100.0D ) / 100.0D ) );

            OptionalLong minTimestamp = grades.stream().mapToLong( Grade::getDate ).min();
            OptionalLong maxTimestamp = grades.stream().mapToLong( Grade::getDate ).max();
            if ( minTimestamp.isPresent() && maxTimestamp.isPresent() ) {
                lblPeriod.setText( DateRangeUtil.getDateRange( minTimestamp.getAsLong(), maxTimestamp.getAsLong() ) );
            }
        } else {
            lblAverage.setText( "" );
            lblPeriod.setText( "" );
        }

        // Get size of subjects with grades
        long subjectGradesAmount = grades.stream().filter( g ->
                Main.getInstance().getAttributes().getUser().getSubjects().stream().anyMatch( s ->
                        s.getSubjectId() == g.getSubjectId() ) ).mapToInt( Grade::getSubjectId ).distinct().count();

        // Set header
        new HeaderBuilder().setTitle( "Noten" )
                .addClickableItem( "Note hinzufügen", e -> this.showGradeDialog( null, null, null ) )
                .addClickableItem( "Quizze", e -> AppViewManager.changeView(AppViewManager.QUIZ_VIEW) )
                .addClickableItem( "Fach hinzufügen", e -> SubjectUtil.showSubjectAddOrUpdateDialog( this, null ) )
                .addClickableItem( "Fach bearbeiten", e -> SubjectUtil.showSubjectEditOrDeleteDialog( this, true ) )
                .addClickableItem( "Fach löschen", e -> SubjectUtil.showSubjectEditOrDeleteDialog( this, false ) )
                .addClickableItem( Main.getInstance().getAttributes().getUser().getClassInfo() == null ? "Klasse beitreten"
                        : "Klassendetails aufrufen", e -> SchoolClassUtil.showCreateJoinOrViewClassDialog( this ) )
                .allowToOpenClickableItems( () -> this.openDialog == null )
                .addChangeViewButton( AppViewManager.HOME_VIEW )
                .addNotificationAmount( (int) subjectGradesAmount )
                .show( hbxTop );


        // Set grades
        BoxBuilder boxBuilder = new BoxBuilder();
        Main.getInstance().getAttributes().getUser().getSubjects().forEach( s -> {
            List<Grade> subjectGrades = grades.stream().filter( g -> g.getSubjectId() == s.getSubjectId() ).collect( Collectors.toList() );
            if ( subjectGrades.size() > 0 ) {
                boxBuilder.addGradesBox( subjectGrades, addHandler -> this.showGradeDialog( s.getName() + "note hinzufügen", null, s),
                        editHandler -> this.showEditSubjectGrades( subjectGrades ), deleteHandler -> {
                            if ( Main.getInstance().getNettyClient().isConnected() ) {
                                Main.getInstance().getNettyClient().sendPacket( new GradeDeletePacket(
                                        Main.getInstance().getAttributes().getUser().getId(),
                                        subjectGrades.stream().mapToInt( Grade::getGradeId ).toArray()
                                ) );
                            } else {
                                this.showMessageDialog( "Fehler", "Zurzeit besteht keine Verbindung zum Server!" );
                            }
                        } );
            }
        } );
        boxBuilder.show( vbxCenter );
    }

    private void showGradeDialog( String title, final Grade grade, Subject defaultSubject ) {
        if ( this.openDialog != null ) {
            return;
        }
        if ( Main.getInstance().getAttributes().getUser().getSubjects().isEmpty() ) {
            this.showMessageDialog( "Fehler", "Es wurden keine Fächer gefunden!" );
            return;
        }
        AtomicReference<JFXComboBox<String>> subjectCbxReference = new AtomicReference<>( null );
        if ( defaultSubject == null ) {
            JFXComboBox<String> cbxSubject = new JFXComboBox<>();
            cbxSubject.setPromptText( "Fach" );
            cbxSubject.getItems().addAll( Main.getInstance().getAttributes().getUser().getSubjects().stream().map( Subject::getName ).collect( Collectors.toList() ) );
            if ( grade != null ) {
                cbxSubject.setValue( Main.getInstance().getAttributes().getUser().getSubjectById( grade.getSubjectId() )
                        .map( Subject::getName ).orElse( "Unbekannt" ) );
            } else {
                cbxSubject.setValue( cbxSubject.getItems().get( 0 ) );
            }
            subjectCbxReference.set( cbxSubject );
        }

        JFXComboBox<String> cbxType = new JFXComboBox<>();
        cbxType.setPromptText( "Gewichtung" );
        cbxType.getItems().addAll( Arrays.stream( Category.values() ).filter( c -> c != Category.NOTES ).map( Category::getDisplayName ).collect( Collectors.toList() ) );
        if ( grade != null ) {
            cbxType.setValue( Category.values()[grade.getCategory()].getDisplayName() );
        } else {
            cbxType.setValue( cbxType.getItems().get( 0 ) );
        }

        JFXTextField txtText = new JFXTextField();
        txtText.setPromptText( "Anmerkung" );
        if ( grade != null ) {
            txtText.setText( grade.getTitle() );
        }

        JFXTextField txtGrade = new JFXTextField();
        txtGrade.setPromptText( "Note" );
        txtGrade.textProperty().addListener( ( observable, oldValue, newValue ) -> {
            newValue = newValue.replaceAll( "[^\\d,.]", "" );
            if ( newValue.length() > 4 ) {
                newValue = newValue.substring( 0, 4 );
            }
            txtGrade.setText( newValue );
        } );
        if ( grade != null ) {
            txtGrade.setText( grade.getGrade() + "" );
        }

        DatePickerComponent dpAlarm = new DatePickerComponent( grade == null ? -1L : grade.getDate() );

        DialogBuilder builder = this.getDialog();
        this.openDialog = builder;

        if ( defaultSubject != null ) {
            builder.addContentNode( txtGrade, cbxType, txtText, dpAlarm );
        } else {
            builder.addContentNode( subjectCbxReference.get(), txtGrade, cbxType, txtText, dpAlarm );
        }

        if ( title == null ) {
            title = grade == null ? "Note hinzufügen" : "Note bearbeiten";
        }

        builder.setTitle( title )
                .addCloseEvent( e -> {
                    if ( this.openDialog != null && this.openDialog.equals( builder ) ) {
                        this.openDialog = null;
                    }
                } )
                .addButton( "Speichern", true, e -> {
                    Grade newGrade = grade;

                    if ( cbxType.getValue() == null || cbxType.getValue().isEmpty()
                            || txtGrade.getText() == null || txtGrade.getText().isEmpty()
                            || ( subjectCbxReference.get() != null && ( subjectCbxReference.get().getValue() == null
                            || subjectCbxReference.get().getValue().isEmpty() ) )
                            || dpAlarm.getSelectedDate() == null ) {
                        e.consume();
                        this.showMessageDialog( "Fehler", "Bitte fülle alle Felder aus!" );
                        return;
                    }

                    double gradeValue;
                    try {
                        String gradeText = txtGrade.getText();
                        if ( gradeText.length() > 4 ) {
                            gradeText = gradeText.substring( 0, 5 );
                        }
                        gradeValue = Double.parseDouble( gradeText.replace( ",", "." ) );
                    } catch ( NumberFormatException e2 ) {
                        e.consume();
                        this.showMessageDialog( "Fehler", "Gebe deine Note als Dezimalzahl ein!" );
                        return;
                    }

                    Optional<Category> optionalCategory = Arrays.stream( Category.values() ).filter( c -> c.getDisplayName().equalsIgnoreCase( cbxType.getSelectionModel().getSelectedItem() ) ).findFirst();
                    Optional<Subject> optionalSubject = defaultSubject != null ? Optional.of( defaultSubject ) : Main.getInstance().getAttributes().getUser().getSubjectByName( subjectCbxReference.get().getSelectionModel().getSelectedItem() );

                    long date = dpAlarm.getSelectedDate().atTime( 0, 0 ).atZone( ZoneId.systemDefault() ).toInstant().toEpochMilli();

                    try {
                        if ( newGrade == null ) {
                            newGrade = new Grade( gradeValue, optionalSubject.orElseThrow( (Supplier<Throwable>) () -> new NullPointerException( "Can't find subject for name!" ) ).getSubjectId(),
                                    optionalCategory.orElseThrow( (Supplier<Throwable>) () -> new NullPointerException( "Can't find category for name!" ) ),
                                    txtText.getText(), date );
                        } else {
                            newGrade.setGrade( gradeValue );
                            newGrade.setSubjectId( optionalSubject.orElseThrow( (Supplier<Throwable>) () -> new NullPointerException( "Can't find subject for name!" ) ).getSubjectId() );
                            newGrade.setCategory( optionalCategory.orElseThrow( (Supplier<Throwable>) () -> new NullPointerException( "Can't find category for name!" ) ).ordinal() );
                            newGrade.setTitle( txtText.getText() );
                            newGrade.setDate( date );
                        }
                    } catch ( Throwable e2 ) {
                        e2.printStackTrace();
                        this.showMessageDialog( "Fehler", "Es ist ein Fehler aufgetreten, versuche es erneut!" );
                        return;
                    }
                    if ( Main.getInstance().getNettyClient().isConnected() ) {
                        Main.getInstance().getNettyClient().sendPacket( new GradeUpdatePacket( Main.getInstance().getAttributes().getUser().getId(), newGrade ) );
                    } else {
                        this.showMessageDialog( "Fehler", "Zurzeit besteht keine Verbindung zum Server!" );
                    }
                } ).addButton( "Abbrechen", true, e -> {
        } ).show();
    }

    private void showEditSubjectGrades( List<Grade> grades ) {
        if ( this.openDialog != null ) {
            return;
        }
        JFXComboBox<Grade> cbxGrade = new JFXComboBox<>();
        cbxGrade.setPromptText( "Note" );
        cbxGrade.getItems().addAll( grades );
        cbxGrade.setConverter( new StringConverter<Grade>() {
            @Override
            public String toString( Grade grade ) {
                return grade.getGrade() + " | " + Category.values()[grade.getCategory()].getDisplayName() + " | "
                        + TimeUtil.getDateFromMilliseconds( grade.getDate(), TimeUtil.DATE_FORMATTER_ONLY_DATE );
            }

            @Override
            public Grade fromString( String string ) {
                String[] split = string.replace( " ", "" ).split( "\\|" );
                return grades.stream().filter( g -> Double.parseDouble( split[0] ) == g.getGrade()
                        && Category.values()[g.getCategory()].getDisplayName().equals( split[1] )
                        && TimeUtil.getDateFromMilliseconds( g.getDate(), TimeUtil.DATE_FORMATTER_ONLY_DATE ).equals( split[2] ) ).findFirst().orElse( null );
            }
        } );
        cbxGrade.setValue( cbxGrade.getItems().get( 0 ) );

        Label lblText = new Label( "Wähle eine Note zum bearbeiten aus!" );

        DialogBuilder builder = this.getDialog();
        this.openDialog = builder;
        builder.setTitle( "Noten bearbeiten" )
                .addContentNode( lblText, cbxGrade )
                .addCloseEvent( e -> {
                    if ( this.openDialog != null && this.openDialog.equals( builder ) ) {
                        this.openDialog = null;
                    }
                } )
                .addButton( "Bearbeiten", true, e -> {
                    if ( cbxGrade.getValue() == null ) {
                        e.consume();
                        this.showMessageDialog( "Fehler", "Es wurde keine Note ausgewählt!" );
                        return;
                    }
                    if ( this.openDialog != null && this.openDialog.equals( builder ) ) {
                        this.openDialog = null;
                    }
                    Grade grade = cbxGrade.getSelectionModel().getSelectedItem();
                    this.showGradeDialog( null, grade, Main.getInstance().getAttributes().getUser().getSubjectById( grade.getSubjectId() ).orElse( null ) );
                } )
                .addButton( "Löschen", true, e -> {
                    if ( cbxGrade.getValue() == null ) {
                        e.consume();
                        this.showMessageDialog( "Fehler", "Es wurde keine Note ausgewählt!" );
                        return;
                    }
                    if ( Main.getInstance().getNettyClient().isConnected() ) {
                        Main.getInstance().getNettyClient().sendPacket( new GradeDeletePacket( Main.getInstance().getAttributes().getUser().getId(), new int[]{ cbxGrade.getSelectionModel().getSelectedItem().getGradeId() } ) );
                    } else {
                        this.showMessageDialog( "Fehler", "Zurzeit besteht keine Verbindung zum Server!" );
                    }
                } )
                .addButton( "Abbrechen", true, e -> {
                } ).show();
    }
}