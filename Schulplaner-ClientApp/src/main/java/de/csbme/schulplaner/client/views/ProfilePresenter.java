package de.csbme.schulplaner.client.views;

import com.gluonhq.charm.glisten.afterburner.GluonPresenter;
import com.gluonhq.charm.glisten.animation.FlipInXTransition;
import com.gluonhq.charm.glisten.animation.NoTransition;
import com.gluonhq.charm.glisten.mvc.View;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import de.csbme.schulplaner.client.Main;
import de.csbme.schulplaner.client.User;
import de.csbme.schulplaner.client.builder.DialogBuilder;
import de.csbme.schulplaner.client.components.ArrowComponent;
import de.csbme.schulplaner.client.manager.AppViewManager;
import de.csbme.schulplaner.client.utils.LoginUtil;
import de.csbme.schulplaner.client.utils.WindowUtil;
import de.csbme.schulplaner.lib.network.packets.changelog.ChangelogRequestPacket;
import de.csbme.schulplaner.lib.network.packets.user.*;
import de.csbme.schulplaner.lib.object.Changelog;
import de.csbme.schulplaner.lib.time.TimeUtil;
import de.csbme.schulplaner.lib.user.Settings;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.SVGPath;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class ProfilePresenter extends GluonPresenter<Main> implements PresenterFunctions {

    @FXML
    private View root;

    @FXML
    private HBox hbxTop;

    @FXML
    private VBox vbxCenter;

    @FXML
    private Label lblEmail, lblName, lblEdit, lblLetter;

    @FXML
    private HBox hbxChangeEmail, hbxChangePassword, hbxAbout, hbxChangelog;

    @FXML
    private JFXCheckBox cbxAnimation, cbxAlert;

    @FXML
    private StackPane stckCenter, stckContent;

    public void initialize() {
        WindowUtil.percentageConstraints( root, hbxTop, stckCenter, stckContent );
        WindowUtil.disableAppBar( root );

        User user = Main.getInstance().getAttributes().getUser();

        root.setShowTransitionFactory( view -> {
            if ( Main.getInstance().getAttributes().getUser().getSettings().get( Settings.Key.ANIMATION, Boolean.class ) ) {
                return new FlipInXTransition( stckCenter );
            }
            return new NoTransition();
        } );

        hbxChangeEmail.setOnMouseClicked( e -> {
            JFXTextField txtEmail = new JFXTextField();
            txtEmail.setPromptText( "E-Mail" );

            this.getDialog().setTitle( "E-Mail ändern" )
                    .addContentNode( new Label( "Gebe deine neue E-Mail ein." ), txtEmail )
                    .addButton( "Aktualisieren", true, e2 -> {
                        if ( !this.isAllowedToChange() ) {
                            return;
                        }
                        if ( txtEmail.getText().equalsIgnoreCase( user.getMailAddress() ) ) {
                            this.showMessageDialog( "Fehler", "Dies ist bereits deine E-Mail!" );
                            e2.consume();
                            return;
                        }
                        if ( !LoginUtil.isMailAddress( txtEmail.getText() ) ) {
                            this.showMessageDialog( "Fehler", "Dies ist keine E-Mail!" );
                            e2.consume();
                            return;
                        }
                        if ( Main.getInstance().getNettyClient().isConnected() ) {
                            Main.getInstance().getNettyClient().sendPacket( new UserMailAddressUpdatePacket( Main.getInstance().getAttributes().getUser().getId(), txtEmail.getText() ) );
                        } else {
                            this.showMessageDialog( "Fehler", "Zurzeit besteht keine Verbindung zum Server!" );
                        }
                    } )
                    .addButton( "Abbrechen", true, e2 -> {
                    } ).show();
        } );

        hbxChangePassword.setOnMouseClicked( e -> {
            JFXPasswordField pwCurrent = new JFXPasswordField();
            pwCurrent.setPromptText( "Aktuelles Passwort" );

            JFXTextField txtPassword = new JFXTextField();
            txtPassword.setPromptText( "Neues Passwort" );

            JFXTextField txtPasswordRepeat = new JFXTextField();
            txtPasswordRepeat.setPromptText( "Neues Passwort erneut" );

            this.getDialog().setTitle( "Passwort ändern" )
                    .addContentNode( new Label( "Gebe deine neues Passwort ein." ), pwCurrent, txtPassword, txtPasswordRepeat )
                    .addButton( "Aktualisieren", true, e2 -> {
                        if ( !this.isAllowedToChange() ) {
                            return;
                        }
                        if ( !txtPassword.getText().equals( txtPasswordRepeat.getText() ) ) {
                            this.showMessageDialog( "Fehler", "Die Passwörter stimmen nicht überein!" );
                            e2.consume();
                            return;
                        }
                        if ( txtPassword.getText().equals( pwCurrent.getText() ) ) {
                            this.showMessageDialog( "Fehler", "Das alte Passwort stimmt mit dem neuen überein!" );
                            e2.consume();
                            return;
                        }
                        if ( Main.getInstance().getNettyClient().isConnected() ) {
                            Main.getInstance().getNettyClient().sendPacket( new UserPasswordUpdatePacket( Main.getInstance().getAttributes().getUser().getId(),
                                    LoginUtil.getHashedPassword( pwCurrent.getText() ), LoginUtil.getHashedPassword( txtPassword.getText() ) ) );
                        } else {
                            this.showMessageDialog( "Fehler", "Zurzeit besteht keine Verbindung zum Server!" );
                        }
                    } )
                    .addButton( "Abbrechen", true, e2 -> {
                    } ).show();
        } );

        hbxAbout.setOnMouseClicked( e -> {
            this.getDialog().setTitle( "Über uns" )
                    .addContentText( "Entwickelt von Jannik Buscha & Simon Perkons & Thorsten Engel", "HAI7A - PRG-Projekt am CSBME" )
                    .addButton( "Schließen", true, e2 -> {
                    } )
                    .show();
        } );

        hbxChangelog.setOnMouseClicked( e -> {
            if ( Main.getInstance().getNettyClient().isConnected() ) {
                Main.getInstance().getNettyClient().sendPacket( new ChangelogRequestPacket() );
            } else {
                this.showMessageDialog( "Fehler", "Zurzeit besteht keine Verbindung zum Server!" );
            }
        } );

        lblEdit.setOnMouseClicked( e -> {
            JFXTextField textField = new JFXTextField();
            textField.setPromptText( "Name" );

            this.getDialog().setTitle( "Name setzen" )
                    .addButton( "Aktualisieren", true, e2 -> {
                        if ( textField.getText() == null || textField.getText().isEmpty() ) {
                            e2.consume();
                            this.showMessageDialog( "Fehler", "Der Name kann nicht leer sein!" );
                            return;
                        }

                        if ( textField.getText().length() > 12 ) {
                            e2.consume();
                            this.showMessageDialog( "Fehler", "Der Name darf maximal 12 Zeichen lang sein!" );
                            return;
                        }
                        if ( Main.getInstance().getNettyClient().isConnected() ) {
                            Main.getInstance().getNettyClient().sendPacket( new UserNameUpdatePacket( Main.getInstance().getAttributes().getUser().getId(),
                                    textField.getText() ) );
                        } else {
                            this.showMessageDialog( "Fehler", "Zurzeit besteht keine Verbindung zum Server!" );
                        }
                    } )
                    .addButton( "Schließen", true, e2 -> {
                    } )
                    .addContentNode( new Label( "Gebe deinen neuen Username ein." ), textField )
                    .show();
        } );

        this.addSettingsChangeListener( this.cbxAnimation, Settings.Key.ANIMATION );
        this.addSettingsChangeListener( this.cbxAlert, Settings.Key.NOTIFICATIONS );

        hbxTop.getChildren().addAll( new ArrowComponent( AppViewManager.HOME_VIEW ), logout() );
    }

    @Override
    public void refresh() {
        User user = Main.getInstance().getAttributes().getUser();

        lblName.setText( user.getUsername() == null || user.getUsername().isEmpty() ? "Nicht gesetzt" : user.getUsername() );
        lblLetter.setText( user.getUsername() == null || user.getUsername().isEmpty() ? "N" : user.getUsername().substring( 0, 1 ).toUpperCase() );
        lblEmail.setText( user.getMailAddress() );

        cbxAnimation.setSelected( user.getSettings().get( Settings.Key.ANIMATION, Boolean.class ) );
        cbxAlert.setSelected( user.getSettings().get( Settings.Key.NOTIFICATIONS, Boolean.class ) );
    }

    @Override
    public DialogBuilder getDialog() {
        stckContent.setVisible( true );

        return new DialogBuilder( this.stckContent ).addCloseEvent( e -> stckContent.setVisible( false ) );
    }

    @Override
    public StackPane getCenterPane() {
        return this.stckContent;
    }

    public void showChangelogDialog( List<Changelog> changelogs ) {
        DialogBuilder builder = this.getDialog().setTitle( "Was ist neu?" ).addButton( "Schließen", true, e -> {
        } );
        AtomicBoolean first = new AtomicBoolean( true );
        changelogs.stream().sorted( Collections.reverseOrder() ).forEach( changelog -> {
            if ( !first.getAndSet( false ) ) {
                builder.addContentNode( new Label( "_____________________" ) );
            }

            VBox box = new VBox();

            box.getChildren().addAll(
                    new Label( "Version: " + changelog.getVersion() ),
                    new Label( "Datum: " + TimeUtil.getDateFromMilliseconds( changelog.getDate(), TimeUtil.DATE_FORMATTER_ONLY_DATE ) ),
                    new Label( changelog.getDescription() )
            );
            builder.addContentNode( box );
        } );
        builder.show();
    }

    private HBox logout() {
        SVGPath icon = new SVGPath();
        icon.setContent( "M1494.176-311.014l-7.567-1.5a.75.75,0,0,1-.608-.735v-15c0-.015.01-.029.011-.043a.515.515,0,0,1,0-.066.7.7,0,0,1,.06-.176.517.517,0,0,1,.021-.063.742.742,0,0,1,.2-.237c.006,0,.008-.014.014-.018a.557.557,0,0,1,.053-.023.777.777,0,0,1,.219-.094.781.781,0,0,1,.1-.018c.027,0,.051-.014.078-.014h12.108a.754.754,0,0,1,.757.751v4.5a.753.753,0,0,1-.757.75.753.753,0,0,1-.757-.75v-3.75h-6.195l2.628.782a.75.75,0,0,1,.54.718v12h3.027v-3.75a.753.753,0,0,1,.757-.75.753.753,0,0,1,.757.75v4.5a.754.754,0,0,1-.757.751h-3.784v.75a.748.748,0,0,1-.277.58.756.756,0,0,1-.48.171A.693.693,0,0,1,1494.176-311.014Zm6.39-7.04a.728.728,0,0,1,0-1.032l.943-.944h-4.077a.729.729,0,0,1-.73-.73.73.73,0,0,1,.73-.73h4.077l-.943-.943a.728.728,0,0,1,0-1.032.729.729,0,0,1,1.032,0l2.189,2.189a.759.759,0,0,1,.158.238.731.731,0,0,1,0,.558.729.729,0,0,1-.158.238l-2.189,2.19a.726.726,0,0,1-.517.213A.723.723,0,0,1,1500.566-318.054Z" );
        icon.getStyleClass().add( "navigationBarIcon" );

        icon.setOnMouseClicked( e -> {
            if ( Main.getInstance().getNettyClient().isConnected() && !Main.getInstance().getAttributes().getUser().isLocal() ) {
                Main.getInstance().getNettyClient().sendPacket( new UserLogoutPacket( Main.getInstance().getAttributes().getUser().getId() ) );
            } else {
                this.showMessageDialog( "Fehler", "Zurzeit besteht keine Verbindung zum Server!" );
            }
        } );

        HBox hbxContent = new HBox( icon );
        hbxContent.setAlignment( Pos.TOP_RIGHT );
        HBox.setHgrow( hbxContent, Priority.ALWAYS );

        return hbxContent;
    }

    private boolean isAllowedToChange() {
        User user = Main.getInstance().getAttributes().getUser();
        if ( user != null && !user.isLocal() ) {
            return true;
        }
        this.showMessageDialog( "Fehler", "Du bist nicht angemeldet!" );
        return false;
    }

    private void addSettingsChangeListener(JFXCheckBox checkBox, Settings.Key key) {
        checkBox.selectedProperty().addListener( ( observable, oldValue, newValue ) -> {
            if ( !isAllowedToChange() ) return;
            Main.getInstance().getAttributes().getUser().getSettings().manipulate( key, newValue );
            if ( Main.getInstance().getNettyClient().isConnected() ) {
                Main.getInstance().getNettyClient().sendPacket( new UserManipulateSettingPacket(
                        Main.getInstance().getAttributes().getUser().getId(),
                        key.ordinal(),
                        newValue
                ) );
            }
        } );
    }
}