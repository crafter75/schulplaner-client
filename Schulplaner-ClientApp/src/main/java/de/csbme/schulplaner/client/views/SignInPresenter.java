package de.csbme.schulplaner.client.views;

import com.gluonhq.charm.glisten.afterburner.GluonPresenter;
import com.gluonhq.charm.glisten.animation.FlipInXTransition;
import com.gluonhq.charm.glisten.mvc.View;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import de.csbme.schulplaner.client.Main;
import de.csbme.schulplaner.client.User;
import de.csbme.schulplaner.client.manager.AppViewManager;
import de.csbme.schulplaner.client.utils.LoginUtil;
import de.csbme.schulplaner.client.utils.WindowUtil;
import de.csbme.schulplaner.lib.network.packets.user.UserLoginPacket;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class SignInPresenter extends GluonPresenter<Main> implements PresenterFunctions {

    @FXML
    private View root;

    @FXML
    private HBox hbxTop, hbxBottom;

    @FXML
    private VBox vbxCenter;

    @FXML
    private StackPane stckCenter;

    @FXML
    private JFXTextField txtEmail;

    @FXML
    private JFXPasswordField pwField;

    @FXML
    private JFXButton btnSignIn;

    @FXML
    private Label lblNoAccount, lblSignUp;

    public void initialize() {
        WindowUtil.percentageConstraintsMobile( root, hbxTop, stckCenter, hbxBottom );
        WindowUtil.disableAppBar( root );

        root.setShowTransitionFactory( v -> new FlipInXTransition( stckCenter ) );

        root.addEventHandler( KeyEvent.KEY_PRESSED, ev -> {
            if ( ev.getCode() == KeyCode.ENTER ) {
                btnSignIn.fire();
                ev.consume();
            }
        } );

        lblSignUp.setOnMouseClicked( e -> AppViewManager.changeView( AppViewManager.SIGN_UP_VIEW ) );
        lblNoAccount.setOnMouseClicked( e -> {
            Main.getInstance().getAttributes().setUser( new User() );
            AppViewManager.changeView( AppViewManager.HOME_VIEW );
        } );
        btnSignIn.setOnAction( e -> {
            if ( !LoginUtil.isMailAddress( txtEmail.getText() ) ) {
                this.showMessageDialog( "Fehler", "Bitte gebe eine gültige E-Mail Adresse an!" );
                return;
            }
            if ( Main.getInstance().getNettyClient().isConnected() ) {
                Main.getInstance().getNettyClient().sendPacket( new UserLoginPacket( txtEmail.getText(), LoginUtil.getHashedPassword( pwField.getText() ) ) );
            } else {
                this.showMessageDialog( "Fehler", "Zurzeit besteht keine Verbindung zum Server!" );
            }
        } );

        switch ( Main.getInstance().getAttributes().getPlatform() ) {
            case DESKTOP:
                //   button.setStyle( "-fx-background-color: red" );
                break;
            case MOBILE:
                //     button.setStyle( "-fx-background-color: blue" );
                break;
            default:
                break;
        }

    }

    @Override
    public void refresh() {
        txtEmail.setText( "" );
        pwField.setText( "" );
    }

    @Override
    public StackPane getCenterPane() {
        return this.stckCenter;
    }
}
