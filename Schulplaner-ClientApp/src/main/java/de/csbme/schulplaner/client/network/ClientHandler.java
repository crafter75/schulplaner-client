package de.csbme.schulplaner.client.network;

import com.gluonhq.charm.glisten.afterburner.AppView;
import de.csbme.schulplaner.client.Main;
import de.csbme.schulplaner.client.User;
import de.csbme.schulplaner.client.manager.AppViewManager;
import de.csbme.schulplaner.client.utils.LoginUtil;
import de.csbme.schulplaner.client.utils.SchoolClassUtil;
import de.csbme.schulplaner.client.views.*;
import de.csbme.schulplaner.lib.network.Packet;
import de.csbme.schulplaner.lib.network.packets.ErrorPacket;
import de.csbme.schulplaner.lib.network.packets.changelog.ChangelogResponsePacket;
import de.csbme.schulplaner.lib.network.packets.grade.GradeRefreshPacket;
import de.csbme.schulplaner.lib.network.packets.note.NoteRefreshPacket;
import de.csbme.schulplaner.lib.network.packets.note.NoteReminderPacket;
import de.csbme.schulplaner.lib.network.packets.quiz.QuizResponsePacket;
import de.csbme.schulplaner.lib.network.packets.quiz.QuizSearchResponsePacket;
import de.csbme.schulplaner.lib.network.packets.schoolclass.ClassCreatedPacket;
import de.csbme.schulplaner.lib.network.packets.schoolclass.ClassJoinedPacket;
import de.csbme.schulplaner.lib.network.packets.schoolclass.ClassLeftPacket;
import de.csbme.schulplaner.lib.network.packets.schoolclass.ClassMemberSizeResponsePacket;
import de.csbme.schulplaner.lib.network.packets.subject.SubjectCreatedPacket;
import de.csbme.schulplaner.lib.network.packets.subject.SubjectDeletedPacket;
import de.csbme.schulplaner.lib.network.packets.subject.SubjectRefreshPacket;
import de.csbme.schulplaner.lib.network.packets.subject.SubjectUpdatedPacket;
import de.csbme.schulplaner.lib.network.packets.timetable.TimetableRefreshPacket;
import de.csbme.schulplaner.lib.network.packets.user.*;
import de.csbme.schulplaner.lib.object.Note;
import de.csbme.schulplaner.lib.object.Subject;
import de.csbme.schulplaner.lib.time.TimeUtil;
import de.csbme.schulplaner.lib.user.Category;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.ReferenceCountUtil;
import javafx.application.Platform;

import java.awt.*;
import java.util.Objects;
import java.util.Optional;

public class ClientHandler extends SimpleChannelInboundHandler<Object> {

    private NettyClient nettyClient;

    ClientHandler(NettyClient nettyClient) {
        this.nettyClient = nettyClient;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Object object) throws Exception {
        try {
            if (!(object instanceof Packet)) {
                return;
            }
            Channel channel = channelHandlerContext.channel();
            Packet packet = (Packet) object;

            // Packet for errors
            if (packet instanceof ErrorPacket) {
                ErrorPacket errorPacket = (ErrorPacket) packet;

                // Run in fx thread
                Platform.runLater(() -> {
                    if (AppViewManager.getCurrentView() != null) {
                        Objects.requireNonNull(AppViewManager.getCurrentPresenter()).showMessageDialog(
                                "Fehler " + errorPacket.getErrorCode().toString(),
                                errorPacket.getMessage());
                    }
                });
                return;
            }

            // Packet for registration complete
            if (packet instanceof UserRegisterCompletePacket) {
                UserRegisterCompletePacket userRegisterCompletePacket = (UserRegisterCompletePacket) packet;

                AppView view = AppViewManager.getCurrentView();
                if (view != null && view.equals(AppViewManager.SIGN_UP_VIEW)) {
                    // Run task in main javafx thread
                    Platform.runLater(() -> {
                        AppViewManager.changeView(AppViewManager.SIGN_IN_VIEW);
                        Objects.requireNonNull(AppViewManager.getCurrentPresenter())
                                .showMessageDialog("Registration", "Du hast dich erfolgreich registriert!",
                                        "ID: " + userRegisterCompletePacket.getId(),
                                        "E-Mail: " + userRegisterCompletePacket.getMailAddress());
                    });
                }
                return;
            }

            // Packet for login successful
            if (packet instanceof UserLoginCallbackPacket) {
                UserLoginCallbackPacket userLoginCallbackPacket = (UserLoginCallbackPacket) packet;

                // Set user
                User user = new User(userLoginCallbackPacket);
                Main.getInstance().getAttributes().setUser(user);

                // Run task in main javafx thread
                Platform.runLater(() -> AppViewManager.changeView(AppViewManager.HOME_VIEW));
                return;
            }

            // Packet for note refresh
            if (packet instanceof NoteRefreshPacket) {
                NoteRefreshPacket noteRefreshPacket = (NoteRefreshPacket) packet;

                Main.getInstance().getAttributes().getUser().setNotes(noteRefreshPacket.getNotes());
                Platform.runLater(() -> Objects.requireNonNull(AppViewManager.getCurrentPresenter()).refresh());
                return;
            }

            // Packet for note reminder
            if (packet instanceof NoteReminderPacket) {
                NoteReminderPacket noteReminderPacket = (NoteReminderPacket) packet;

                if (!Main.getInstance().getAttributes().getUser().isLocal()
                        && Main.getInstance().getAttributes().getUser().getId() == noteReminderPacket.getUserId()) {
                    Note note = noteReminderPacket.getNote();

                    // Show note screen
                    Platform.runLater(() -> Objects.requireNonNull(AppViewManager.getCurrentPresenter()).getDialog()
                            .setTitle("Erinnerung")
                            .addContentText(this.getNotificationContent(note))
                            .addButton("Eintrag bearbeiten", true, e -> {
                                if (note.getTitle() == null || note.getTitle().isEmpty()) {
                                    AppViewManager.changeView(AppViewManager.ENTRIES_VIEW);
                                    if (AppViewManager.getCurrentPresenter() instanceof EntriesPresenter) {
                                        ((EntriesPresenter) AppViewManager.getCurrentPresenter()).showNoteDialog(null, note, new int[0]);
                                    }
                                    return;
                                }
                                AppViewManager.changeView(AppViewManager.NOTES_VIEW);
                                if (AppViewManager.getCurrentPresenter() instanceof NotesPresenter) {
                                    ((NotesPresenter) AppViewManager.getCurrentPresenter()).showNoteDialog(note);
                                }
                            }).addButton("Schließen", true, e -> {
                            }).show());

                    // Check if supported
                    if (!SystemTray.isSupported()) {
                        return;
                    }

                    // Show windows notification
                    Platform.runLater(() -> {
                        try {
                            SystemTray tray = SystemTray.getSystemTray();

                            Image image = Toolkit.getDefaultToolkit().createImage("icon.png");

                            TrayIcon trayIcon = new TrayIcon(image, "Tray Demo");
                            trayIcon.setImageAutoSize(true);
                            trayIcon.setToolTip("System tray icon demo");
                            tray.add(trayIcon);

                            String content;
                            if (note.getTitle() == null || note.getTitle().isEmpty()) {
                                Optional<Subject> optional = Main.getInstance().getAttributes().getUser().getSubjectById(note.getSubjectId());
                                content = optional.map(Subject::getName).orElse("Unbekanntes Fach") + ": " + Category.values()[note.getCategory()].getDisplayName();
                            } else {
                                content = note.getTitle();
                            }
                            trayIcon.displayMessage("Schulplaner Erinnerung", content, TrayIcon.MessageType.INFO);
                        } catch (AWTException e) {
                            e.printStackTrace();
                        }
                    });
                }
                return;
            }

            // Packet for grade refresh
            if (packet instanceof GradeRefreshPacket) {
                GradeRefreshPacket gradeRefreshPacket = (GradeRefreshPacket) packet;

                Main.getInstance().getAttributes().getUser().setGrades(gradeRefreshPacket.getGrades());
                Platform.runLater(() -> {
                    if (AppViewManager.getCurrentPresenter() instanceof GradesPresenter) {
                        ((GradesPresenter) AppViewManager.getCurrentPresenter()).updateGrades();
                    }
                });
                return;
            }

            // Packet for log out
            if (packet instanceof UserLogoutPacket) {
                UserLogoutPacket userLogoutPacket = (UserLogoutPacket) packet;

                if (!Main.getInstance().getAttributes().getUser().isLocal()
                        && Main.getInstance().getAttributes().getUser().getId() == userLogoutPacket.getUserId()) {
                    Platform.runLater(() -> {
                        AppViewManager.changeView(AppViewManager.SIGN_IN_VIEW);
                        Main.getInstance().getAttributes().setUser(null);
                        Objects.requireNonNull(AppViewManager.getCurrentPresenter()).showMessageDialog("Ausgeloggt", "Du wurdest erfolgreich ausgeloggt!");
                    });
                }
                return;
            }

            // Packet for mail update
            if (packet instanceof UserMailAddressUpdatedPacket) {
                UserMailAddressUpdatedPacket mailUpdatedPacket = (UserMailAddressUpdatedPacket) packet;

                if (!Main.getInstance().getAttributes().getUser().isLocal()
                        && Main.getInstance().getAttributes().getUser().getId() == mailUpdatedPacket.getUserId()) {
                    Main.getInstance().getAttributes().getUser().setMailAddress(mailUpdatedPacket.getMailAddress());
                    Platform.runLater(() -> {
                        Objects.requireNonNull(AppViewManager.getCurrentPresenter()).refresh();
                        Objects.requireNonNull(AppViewManager.getCurrentPresenter()).showMessageDialog("E-Mail aktualisiert", "Deine E-Mail wurde aktualisiert!");
                    });
                }
                return;
            }

            // Packet for password update
            if (packet instanceof UserPasswordUpdatedPacket) {
                UserPasswordUpdatedPacket userPasswordUpdatedPacket = (UserPasswordUpdatedPacket) packet;

                if (!Main.getInstance().getAttributes().getUser().isLocal()
                        && Main.getInstance().getAttributes().getUser().getId() == userPasswordUpdatedPacket.getUserId()) {
                    Platform.runLater(() -> {
                        Objects.requireNonNull(AppViewManager.getCurrentPresenter()).refresh();
                        Objects.requireNonNull(AppViewManager.getCurrentPresenter()).showMessageDialog("Passwort aktualisiert", "Dein Passwort wurde aktualisiert!");
                    });
                }
                return;
            }

            // Packet for changelog respone
            if (packet instanceof ChangelogResponsePacket) {
                ChangelogResponsePacket changelogResponsePacket = (ChangelogResponsePacket) packet;

                Platform.runLater(() -> {
                    if (AppViewManager.getCurrentPresenter() != null && AppViewManager.getCurrentPresenter() instanceof ProfilePresenter) {
                        ((ProfilePresenter) AppViewManager.getCurrentPresenter()).showChangelogDialog(changelogResponsePacket.getChangeLogs());
                    }
                });
                return;
            }

            // Packet for update username
            if (packet instanceof UserNameUpdatePacket) {
                UserNameUpdatePacket userNameUpdatePacket = (UserNameUpdatePacket) packet;

                if (!Main.getInstance().getAttributes().getUser().isLocal()
                        && Main.getInstance().getAttributes().getUser().getId() == userNameUpdatePacket.getUserId()) {
                    Main.getInstance().getAttributes().getUser().setUsername(userNameUpdatePacket.getUsername());
                    Platform.runLater(() -> {
                        Objects.requireNonNull(AppViewManager.getCurrentPresenter()).refresh();
                        Objects.requireNonNull(AppViewManager.getCurrentPresenter()).showMessageDialog("Name aktualisiert", "Dein Username wurde aktualisiert!");
                    });
                }
                return;
            }

            // Packet for receiving created class
            if (packet instanceof ClassCreatedPacket) {
                ClassCreatedPacket classCreatedPacket = (ClassCreatedPacket) packet;

                if (!Main.getInstance().getAttributes().getUser().isLocal()
                        && Main.getInstance().getAttributes().getUser().getId() == classCreatedPacket.getUserId()) {
                    Main.getInstance().getAttributes().getUser().setClassInfo(classCreatedPacket.getClassInfo());

                    Platform.runLater(() -> {
                        Objects.requireNonNull(AppViewManager.getCurrentPresenter()).refresh();
                        Objects.requireNonNull(AppViewManager.getCurrentPresenter()).showMessageDialog("Klasse erstellt", "Deine Klasse wurde erfolgreich erstellt!\nDu kannst nun Leute mit dem Klassencode '" + Main.getInstance().getAttributes().getUser().getClassInfo().getClassCode() + "' einladen.");
                    });
                }
                return;
            }

            // Packet for receiving joined class
            if (packet instanceof ClassJoinedPacket) {
                ClassJoinedPacket classCreatedPacket = (ClassJoinedPacket) packet;

                if (!Main.getInstance().getAttributes().getUser().isLocal()
                        && Main.getInstance().getAttributes().getUser().getId() == classCreatedPacket.getUserId()) {
                    Main.getInstance().getAttributes().getUser().setClassInfo(classCreatedPacket.getClassInfo());

                    Platform.runLater(() -> {
                        Objects.requireNonNull(AppViewManager.getCurrentPresenter()).refresh();
                        Objects.requireNonNull(AppViewManager.getCurrentPresenter()).showMessageDialog("Klasse beigetreten", "Du bist der Klasse '" + classCreatedPacket.getClassInfo().getName() + "' beigetreten.");
                    });
                }
                return;
            }

            // Packet for class member size response
            if (packet instanceof ClassMemberSizeResponsePacket) {
                ClassMemberSizeResponsePacket classMemberSizeResponePacket = (ClassMemberSizeResponsePacket) packet;

                Platform.runLater(() -> SchoolClassUtil.showClassInfoDialog(Objects.requireNonNull(AppViewManager.getCurrentPresenter()), classMemberSizeResponePacket.getMemberSize()));
                return;
            }

            // Packet for leaving class
            if (packet instanceof ClassLeftPacket) {
                ClassLeftPacket classLeftPacket = (ClassLeftPacket) packet;

                if (!Main.getInstance().getAttributes().getUser().isLocal()
                        && Main.getInstance().getAttributes().getUser().getId() == classLeftPacket.getUserId()) {
                    Main.getInstance().getAttributes().getUser().setClassInfo(null);
                    Main.getInstance().getAttributes().getUser().getSubjects().clear();
                    Platform.runLater(() -> {
                        Objects.requireNonNull(AppViewManager.getCurrentPresenter()).refresh();
                        Objects.requireNonNull(AppViewManager.getCurrentPresenter()).showMessageDialog("Klasse verlassen", "Du hast die Klasse verlassen.");
                    });
                }
                return;
            }

            // Packet for subject update
            if (packet instanceof SubjectRefreshPacket) {
                SubjectRefreshPacket subjectRefreshPacket = (SubjectRefreshPacket) packet;

                if (!Main.getInstance().getAttributes().getUser().isLocal()) {
                    Main.getInstance().getAttributes().getUser().setSubjects(subjectRefreshPacket.getSubjects());
                    Platform.runLater(() -> Objects.requireNonNull(AppViewManager.getCurrentPresenter()).refresh());
                }
                return;
            }

            // Packet for creating subject confirmation
            if (packet instanceof SubjectCreatedPacket) {
                SubjectCreatedPacket subjectCreatedPacket = (SubjectCreatedPacket) packet;

                if (!Main.getInstance().getAttributes().getUser().isLocal()
                        && Main.getInstance().getAttributes().getUser().getId() == subjectCreatedPacket.getUserId()) {
                    Platform.runLater(() -> Objects.requireNonNull(AppViewManager.getCurrentPresenter()).showMessageDialog("Fach erstellt", "Das Fach wurde erfolgreich erstellt."));
                }
                return;
            }

            // Packet for deleted subject
            if (packet instanceof SubjectDeletedPacket) {
                SubjectDeletedPacket subjectDeletedPacket = (SubjectDeletedPacket) packet;

                if (!Main.getInstance().getAttributes().getUser().isLocal()
                        && Main.getInstance().getAttributes().getUser().getId() == subjectDeletedPacket.getUserId()) {
                    Platform.runLater(() -> Objects.requireNonNull(AppViewManager.getCurrentPresenter()).showMessageDialog("Fach gelöscht", "Das Fach wurde erfolgreich gelöscht."));
                }
                return;
            }

            // Packet for updated subject
            if (packet instanceof SubjectUpdatedPacket) {
                SubjectUpdatedPacket subjectUpdatedPacket = (SubjectUpdatedPacket) packet;

                if (!Main.getInstance().getAttributes().getUser().isLocal()
                        && Main.getInstance().getAttributes().getUser().getId() == subjectUpdatedPacket.getUserId()) {
                    Platform.runLater(() -> Objects.requireNonNull(AppViewManager.getCurrentPresenter()).showMessageDialog("Fach bearbeitet", "Das Fach wurde erfolgreich bearbeitet."));
                }
                return;
            }

            // Packet for timetable refresh
            if (packet instanceof TimetableRefreshPacket) {
                TimetableRefreshPacket timetableRefreshPacket = (TimetableRefreshPacket) packet;

                if (!Main.getInstance().getAttributes().getUser().isLocal()
                        && Main.getInstance().getAttributes().getUser().getId() == timetableRefreshPacket.getUserId()) {
                    Platform.runLater(() -> {
                        if (AppViewManager.getCurrentPresenter() instanceof TimetablePresenter) {
                            ((TimetablePresenter) AppViewManager.getCurrentPresenter()).updateTimetable(timetableRefreshPacket.getTimetable());
                        }
                    });
                }
                return;
            }

            // Packet for receiving quizzes
            if (packet instanceof QuizResponsePacket) {
                QuizResponsePacket quizResponsePacket = (QuizResponsePacket) packet;

                Main.getInstance().getAttributes().getUser().setQuizzes(quizResponsePacket.getQuizzes());
                Platform.runLater(() -> {
                    if (AppViewManager.getCurrentPresenter() instanceof QuizPresenter) {
                        ((QuizPresenter) AppViewManager.getCurrentPresenter()).updateQuizzes();
                    }
                });
                return;
            }

            // Packet for quiz response by searching
            if (packet instanceof QuizSearchResponsePacket) {
                QuizSearchResponsePacket quizSearchResponsePacket = (QuizSearchResponsePacket) packet;

                if (AppViewManager.getCurrentView() == null || !AppViewManager.getCurrentView().equals(AppViewManager.QUIZ_VIEW)) {
                    return;
                }

                Main.getInstance().getAttributes().getUser().setCurrentQuiz(quizSearchResponsePacket.getQuiz());
                Platform.runLater(() -> AppViewManager.changeView(AppViewManager.QUIZ_QUESTIONS_VIEW));
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ReferenceCountUtil.release(object);
        }
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        // Set channel
        this.nettyClient.setChannel(ctx.channel());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        // Remove channel
        this.nettyClient.setChannel(null);

        Platform.runLater(LoginUtil::onServerTimeout);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
    }

    private String[] getNotificationContent(Note note) {
        if (note.getTitle() == null || note.getTitle().isEmpty()) {
            Optional<Subject> optional = Main.getInstance().getAttributes().getUser().getSubjectById(note.getSubjectId());
            return new String[]{
                    "Wir sollten dich an einen Eintrag erinnern:",
                    "- Fach: " + optional.map(Subject::getName).orElse("Unbekanntes Fach"),
                    "- Typ: " + Category.values()[note.getCategory()].getDisplayName(),
                    "- Erstellt: " + TimeUtil.getDateFromMilliseconds(note.getCreateDate(), TimeUtil.DATE_WITHOUT_SECONDS),
                    "- Zuletzt bearbeitet: " + TimeUtil.getDateFromMilliseconds(note.getUpdateDate(), TimeUtil.DATE_WITHOUT_SECONDS),
                    "- Beschreibung: " + note.getContent()
            };
        }
        return new String[]{
                "Wir sollten dich an einen Eintrag erinnern:",
                "- Titel: " + note.getTitle(),
                "- Erstellt: " + TimeUtil.getDateFromMilliseconds(note.getCreateDate(), TimeUtil.DATE_WITHOUT_SECONDS),
                "- Zuletzt bearbeitet: " + TimeUtil.getDateFromMilliseconds(note.getUpdateDate(), TimeUtil.DATE_WITHOUT_SECONDS),
                "- Beschreibung: " + note.getContent()
        };
    }
}
