package de.csbme.schulplaner.client.utils;

import de.csbme.schulplaner.client.Main;
import de.csbme.schulplaner.client.User;
import de.csbme.schulplaner.client.manager.AppViewManager;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

public class LoginUtil {

    /**
     * Get the hashed password with SHA-512
     *
     * @param password to hash
     * @return the result
     */
    public static String getHashedPassword( String password ) {
        if ( password.length() == 128 ) {
            return password;
        }
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance( "SHA-512" );
            byte[] bytes = md.digest( password.getBytes( StandardCharsets.UTF_8 ) );
            StringBuilder sb = new StringBuilder();
            for ( int i = 0; i < bytes.length; i++ ) {
                sb.append( Integer.toString( ( bytes[i] & 0xff ) + 0x100, 16 ).substring( 1 ) );
            }
            generatedPassword = sb.toString();
        } catch ( NoSuchAlgorithmException e ) {
            e.printStackTrace();
        }
        return generatedPassword;
    }

    public static boolean isMailAddress( String mail ) {
        return mail.matches( "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$" );
    }

    public static void onServerTimeout() {
        if ( !AppViewManager.getCurrentView().equals( AppViewManager.SIGN_IN_VIEW ) ) {
            User user = Main.getInstance().getAttributes().getUser();
            if ( user != null && !user.isLocal() ) {
                AppViewManager.changeView( AppViewManager.SIGN_IN_VIEW );
                Objects.requireNonNull( AppViewManager.getCurrentPresenter() ).showMessageDialog( "Verbindung verloren", "Die Verbindung zum Server wurde verloren. Du wurdest ausgeloggt!" );
                Main.getInstance().getAttributes().setUser( null );
            }
        }
    }
}
