package de.csbme.schulplaner.client.components;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import de.csbme.schulplaner.client.utils.ComponentUtil;
import javafx.beans.value.ChangeListener;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.stream.IntStream;

public class DatePickerComponent extends HBox {

    private JFXComboBox<String> cboYear, cboMonth, cboDay;

    public DatePickerComponent( long input ) {
        this.setSpacing( 10 );

        LocalDate currentDate;
        if ( input != -1L ) {
            currentDate = Instant.ofEpochMilli( input ).atZone( ZoneId.systemDefault() ).toLocalDate();
        } else {
            currentDate = LocalDate.now();
        }

        this.cboYear = ComponentUtil.pickerComboBox();
        this.cboYear.getItems().addAll( IntStream.rangeClosed( currentDate.getYear(), currentDate.getYear() + 10 ).mapToObj( String::valueOf ).toArray( String[]::new ) );
        this.cboYear.setPromptText( "Jahr" );
        this.cboYear.setValue( currentDate.getYear() + "" );

        this.cboMonth = ComponentUtil.pickerComboBox();
        this.cboMonth.getItems().addAll( Arrays.stream( Month.values() ).map( m -> ( m.getValue() < 10 ? "0" : "" ) + m.getValue() ).toArray( String[]::new ) );
        this.cboMonth.setPromptText( "Monat" );
        this.cboMonth.setValue( ( currentDate.getMonth().getValue() < 10 ? "0" : "" ) + currentDate.getMonth().getValue() );

        this.cboDay = ComponentUtil.pickerComboBox();
        this.cboDay.getItems().addAll( this.getDayItems( currentDate.getMonth().length( currentDate.isLeapYear() ) ) );
        this.cboDay.setPromptText( "Tag" );
        this.cboDay.setValue( ( currentDate.getDayOfMonth() < 10 ? "0" : "" ) + currentDate.getDayOfMonth() );

        ChangeListener<String> changeListener = ( observable, oldValue, newValue ) -> {
            int year1 = Integer.parseInt( this.cboYear.getSelectionModel().getSelectedItem() );
            Month month = Month.of( Integer.parseInt( this.cboMonth.getSelectionModel().getSelectedItem() ) );
            int days = month.length( currentDate.withYear( year1 ).isLeapYear() );
            this.cboDay.getItems().clear();
            this.cboDay.getItems().addAll( this.getDayItems( days ) );
            this.cboDay.setValue( this.cboDay.getItems().get( 0 ) );
        };

        this.cboYear.valueProperty().addListener( changeListener );
        this.cboMonth.valueProperty().addListener( changeListener );

        this.getChildren().addAll( this.cboDay, this.cboMonth, this.cboYear );
    }

    public LocalDate getSelectedDate() {
        return LocalDate.of( Integer.parseInt( this.cboYear.getSelectionModel().getSelectedItem() ),
                Integer.parseInt( this.cboMonth.getSelectionModel().getSelectedItem() ),
                Integer.parseInt( this.cboDay.getSelectionModel().getSelectedItem() ) );
    }

    private String[] getDayItems( int days ) {
        return IntStream.rangeClosed( 1, days ).mapToObj( i ->
                ( i < 10 ? "0" : "" ) + i ).toArray( String[]::new );
    }
}