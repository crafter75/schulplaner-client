package de.csbme.schulplaner.client.manager;

import com.gluonhq.charm.glisten.afterburner.AppView;
import com.gluonhq.charm.glisten.afterburner.AppViewRegistry;
import com.gluonhq.charm.glisten.afterburner.GluonPresenter;
import com.gluonhq.charm.glisten.afterburner.Utils;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.Avatar;
import com.gluonhq.charm.glisten.control.NavigationDrawer;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import de.csbme.schulplaner.client.views.*;
import javafx.scene.image.Image;
import lombok.Getter;

import java.util.Locale;
import java.util.Objects;

import static com.gluonhq.charm.glisten.afterburner.AppView.Flag.SKIP_VIEW_STACK;

public class AppViewManager {

    private static final AppViewRegistry REGISTRY = new AppViewRegistry();

    @Getter
    private static AppView currentView;

    public static final AppView SIGN_IN_VIEW = view( "Anmelden", SignInPresenter.class, MaterialDesignIcon.HOME, AppView.Flag.HOME_VIEW, SKIP_VIEW_STACK );
    public static final AppView SIGN_UP_VIEW = view( "Registrieren", SignUpPresenter.class, MaterialDesignIcon.EDIT, SKIP_VIEW_STACK );
    public static final AppView HOME_VIEW = view( "Startseite", HomePresenter.class, MaterialDesignIcon.EDIT, SKIP_VIEW_STACK );
    public static final AppView TIMETABLE_VIEW = view( "Stundenplan", TimetablePresenter.class, MaterialDesignIcon.EDIT, SKIP_VIEW_STACK );
    public static final AppView ENTRIES_VIEW = view( "Einträge", EntriesPresenter.class, MaterialDesignIcon.EDIT, SKIP_VIEW_STACK );
    public static final AppView NOTES_VIEW = view( "Notizen", NotesPresenter.class, MaterialDesignIcon.EDIT, SKIP_VIEW_STACK );
    public static final AppView GRADES_VIEW = view( "Noten", GradesPresenter.class, MaterialDesignIcon.EDIT, SKIP_VIEW_STACK );
    public static final AppView QUIZ_VIEW = view( "Quizze", QuizPresenter.class, MaterialDesignIcon.EDIT, SKIP_VIEW_STACK );
    public static final AppView QUIZ_QUESTIONS_VIEW = view( "Quiz Fragen", QuizQuestionsPresenter.class, MaterialDesignIcon.EDIT, SKIP_VIEW_STACK );
    public static final AppView PROFILE_VIEW = view( "Profil", ProfilePresenter.class, MaterialDesignIcon.EDIT, SKIP_VIEW_STACK );

    public static void changeView( AppView appView ) {
        if ( currentView != null && currentView.equals( appView ) ) {
            Objects.requireNonNull( getCurrentPresenter() ).refresh();
            return;
        }
        currentView = appView;
        appView.switchView();
        Objects.requireNonNull( getCurrentPresenter() ).refresh();
    }

    public static PresenterFunctions getCurrentPresenter() {
        if ( currentView == null ) return null;
        if ( currentView.getPresenter().isPresent() && currentView.getPresenter().get() instanceof PresenterFunctions ) {
            return (PresenterFunctions) currentView.getPresenter().get();
        }
        return null;
    }

    public static <T extends PresenterFunctions> T getCurrentPresenter( Class<T> presenterClass ) {
        PresenterFunctions functions = getCurrentPresenter();
        if ( presenterClass.isInstance( functions ) ) {
            return presenterClass.cast( functions );
        }
        return null;
    }

    public static void registerViewsAndDrawer( MobileApplication app ) {
        for ( AppView view : REGISTRY.getViews() ) {
            view.registerView( app );
        }

        NavigationDrawer.Header header = new NavigationDrawer.Header( "Gluon Mobile", "Schulplaner", new Avatar( 21, new Image( AppViewManager.class.getResourceAsStream( "/icon.png" ) ) ) );

        Utils.buildDrawer( app.getDrawer(), header, REGISTRY.getViews() );

        currentView = SIGN_IN_VIEW;
    }

    private static AppView view( String title, Class<? extends GluonPresenter<?>> presenterClass, MaterialDesignIcon menuIcon, AppView.Flag... flags ) {
        return REGISTRY.createView( name( presenterClass ), title, presenterClass, menuIcon, flags );
    }

    private static String name( Class<? extends GluonPresenter<?>> presenterClass ) {
        return presenterClass.getSimpleName().toUpperCase( Locale.ROOT ).replace( "PRESENTER", "" );
    }
}
