package de.csbme.schulplaner.client.views;

import com.gluonhq.charm.glisten.afterburner.GluonPresenter;
import com.gluonhq.charm.glisten.animation.FlipInXTransition;
import com.gluonhq.charm.glisten.animation.NoTransition;
import com.gluonhq.charm.glisten.mvc.View;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import de.csbme.schulplaner.client.Main;
import de.csbme.schulplaner.client.builder.BoxBuilder;
import de.csbme.schulplaner.client.builder.DialogBuilder;
import de.csbme.schulplaner.client.builder.HeaderBuilder;
import de.csbme.schulplaner.client.components.NavigationComponent;
import de.csbme.schulplaner.client.components.SidebarNavigationComponent;
import de.csbme.schulplaner.client.manager.AppViewManager;
import de.csbme.schulplaner.client.network.ClientHandler;
import de.csbme.schulplaner.client.types.PlatformType;
import de.csbme.schulplaner.client.utils.DateReminder;
import de.csbme.schulplaner.client.utils.SchoolClassUtil;
import de.csbme.schulplaner.client.utils.SubjectUtil;
import de.csbme.schulplaner.client.utils.WindowUtil;
import de.csbme.schulplaner.lib.network.packets.note.NoteDeletePacket;
import de.csbme.schulplaner.lib.network.packets.note.NoteUpdatePacket;
import de.csbme.schulplaner.lib.object.Note;
import de.csbme.schulplaner.lib.object.Subject;
import de.csbme.schulplaner.lib.user.Category;
import de.csbme.schulplaner.lib.user.Settings;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class EntriesPresenter extends GluonPresenter<Main> implements PresenterFunctions {

    @FXML
    private View root;

    @FXML
    private HBox hbxTop, hbxExit;

    @FXML
    private VBox vbxCenter, vbxSidebar, vbxLeftbar;

    @FXML
    private StackPane stckCenter;

    @FXML
    private AnchorPane anchBottom;

    private DialogBuilder openDialog = null;

    public void initialize() {
        WindowUtil.disableAppBar( root );
        WindowUtil.percentageConstraints( root, hbxTop, stckCenter );

        root.setShowTransitionFactory( view -> {
            if ( Main.getInstance().getAttributes().getUser().getSettings().get( Settings.Key.ANIMATION, Boolean.class ) ) {
                return new FlipInXTransition( stckCenter );
            }
            return new NoTransition();
        } );

        if (Main.getInstance().getAttributes().getPlatform() == PlatformType.MOBILE) {
            anchBottom.getChildren().add(new NavigationComponent());
        } else {
            new SidebarNavigationComponent(vbxSidebar, vbxLeftbar, hbxExit);
        }
    }

    @Override
    public void refresh() {
        // Clear old nodes
        this.hbxTop.getChildren().clear();
        this.vbxCenter.getChildren().clear();

        // Get entries
        List<Subject> subjects = Main.getInstance().getAttributes().getUser().getSubjects();
        List<Note> notes = Main.getInstance().getAttributes().getUser().getNotes().stream().filter( n ->
                n.getCategory() != Category.NOTES.ordinal()
                        && subjects.stream().anyMatch( s -> n.getSubjectId() == s.getSubjectId() ) ).collect( Collectors.toList() );

        // Set header
        new HeaderBuilder().setTitle( "Einträge" )
                .addClickableItem( "Eintrag hinzufügen", e -> this.showNoteDialog( null, null, new int[0] ) )
                .addClickableItem( "Fach hinzufügen", e -> SubjectUtil.showSubjectAddOrUpdateDialog( this, null ) )
                .addClickableItem( "Fach bearbeiten", e -> SubjectUtil.showSubjectEditOrDeleteDialog( this, true ) )
                .addClickableItem( "Fach löschen", e -> SubjectUtil.showSubjectEditOrDeleteDialog( this, false ) )
                .addClickableItem( Main.getInstance().getAttributes().getUser().getClassInfo() == null ? "Klasse beitreten"
                        : "Klassendetails aufrufen", e -> SchoolClassUtil.showCreateJoinOrViewClassDialog( this ) )
                .allowToOpenClickableItems( () -> this.openDialog == null )
                .addChangeViewButton( AppViewManager.HOME_VIEW )
                .addNotificationAmount( notes.size() )
                .show( hbxTop );

        // Set content
        BoxBuilder boxBuilder = new BoxBuilder();
        notes.stream().sorted( ( o1, o2 ) -> Boolean.compare( o1.isDone(), o2.isDone() ) ).forEach( n ->
                boxBuilder.addNoteBox( n, editEvent -> this.showNoteDialog( null, n, new int[0] ), deleteEvent -> {
                    if ( Main.getInstance().getNettyClient().isConnected() ) {
                       Main.getInstance().getNettyClient().sendPacket( new NoteDeletePacket(
                                Main.getInstance().getAttributes().getUser().getId(),
                                n.getNoteId()
                        ) );
                    } else {
                        this.showMessageDialog( "Fehler", "Zurzeit besteht keine Verbindung zum Server!" );
                    }
                } ) );
        boxBuilder.show( vbxCenter );
    }

    @Override
    public StackPane getCenterPane() {
        return this.stckCenter;
    }

    public void showNoteDialog( String title, final Note note, final int[] defaultCategory ) {
        if ( this.openDialog != null ) {
            return;
        }

        if ( Main.getInstance().getAttributes().getUser().getSubjects().isEmpty() ) {
            this.showMessageDialog( "Fehler", "Es wurden keine Fächer gefunden!" );
            return;
        }

        AtomicReference<JFXComboBox<String>> categoryCbxReference = new AtomicReference<>( null );
        if ( defaultCategory.length != 1 ) {
            JFXComboBox<String> cbxType = new JFXComboBox<>();
            cbxType.setPromptText( "Typ" );
            if ( defaultCategory.length > 1 ) {
                cbxType.getItems().addAll( Arrays.stream( Category.values() ).filter( c -> c != Category.NOTES && Arrays.stream( defaultCategory ).anyMatch( i -> c.ordinal() == i ) ).map( Category::getDisplayName ).collect( Collectors.toList() ) );
            } else {
                cbxType.getItems().addAll( Arrays.stream( Category.values() ).filter( c -> c != Category.NOTES ).map( Category::getDisplayName ).collect( Collectors.toList() ) );
            }
            if ( note != null ) {
                cbxType.setValue( Category.values()[note.getCategory()].getDisplayName() );
            } else {
                cbxType.setValue( cbxType.getItems().get( 0 ) );
            }
            categoryCbxReference.set( cbxType );
        }

        JFXComboBox<String> cbxSubject = new JFXComboBox<>();
        cbxSubject.setPromptText( "Fach" );
        cbxSubject.getItems().addAll( Main.getInstance().getAttributes().getUser().getSubjects().stream().map( Subject::getName ).collect( Collectors.toList() ) );
        if ( note != null ) {
            cbxSubject.setValue( Main.getInstance().getAttributes().getUser().getSubjectById( note.getSubjectId() )
                    .map( Subject::getName ).orElse( "Unbekannt" ) );
        } else {
            cbxSubject.setValue( cbxSubject.getItems().get( 0 ) );
        }

        JFXTextField txtText = new JFXTextField();
        txtText.setPromptText( "Text" );
        if ( note != null ) {
            txtText.setText( note.getContent() );
        }

        DialogBuilder builder = this.getDialog();
        this.openDialog = builder;

        if ( categoryCbxReference.get() != null ) {
            builder.addContentNode( categoryCbxReference.get() );
        }
        builder.addContentNode( cbxSubject, txtText );

        DateReminder dateReminder = new DateReminder( note, builder );

        if ( title == null ) {
            title = note == null ? "Eintrag hinzufügen" : "Eintrag bearbeiten";
        }

        builder.setTitle( title )
                .addCloseEvent( e -> {
                    if ( this.openDialog != null && this.openDialog.equals( builder ) ) {
                        this.openDialog = null;
                    }
                } )
                .addButton( "Speichern", true, e -> {
                    Note newNote = note;

                    if ( ( categoryCbxReference.get() != null && ( categoryCbxReference.get().getValue() == null
                            || categoryCbxReference.get().getValue().isEmpty() ) )
                            || cbxSubject.getValue() == null || cbxSubject.getValue().isEmpty()
                            || txtText.getText() == null || txtText.getText().isEmpty() ) {
                        e.consume();
                        this.showMessageDialog( "Fehler", "Bitte fülle alle Felder aus!" );
                        return;
                    }

                    Optional<Category> optionalCategory = categoryCbxReference.get() == null ? Optional.of( Category.values()[defaultCategory[0]] ) :
                            Arrays.stream( Category.values() ).filter( c -> c.getDisplayName().equalsIgnoreCase( categoryCbxReference.get().getSelectionModel().getSelectedItem() ) ).findFirst();
                    Optional<Subject> optionalSubject = Main.getInstance().getAttributes().getUser().getSubjectByName( cbxSubject.getSelectionModel().getSelectedItem() );

                    long expireTimestamp = dateReminder.getTimestamp();

                    try {
                        if ( newNote == null ) {
                            newNote = new Note( optionalSubject.orElseThrow( (Supplier<Throwable>) () -> new NullPointerException( "Can't find subject for name!" ) ).getSubjectId(),
                                    optionalCategory.orElseThrow( (Supplier<Throwable>) () -> new NullPointerException( "Can't find category for name!" ) ),
                                    Note.OwnerType.USER, "", txtText.getText(), expireTimestamp, dateReminder.isDoneSelected() ); // TODO at start only user entries. Class entries coming soon :)
                        } else {
                            newNote.setSubjectId( optionalSubject.orElseThrow( (Supplier<Throwable>) () -> new NullPointerException( "Can't find subject for name!" ) ).getSubjectId() );
                            newNote.setCategory( optionalCategory.orElseThrow( (Supplier<Throwable>) () -> new NullPointerException( "Can't find category for name!" ) ).ordinal() );
                            newNote.setContent( txtText.getText() );
                            newNote.setExpireDate( expireTimestamp );
                            newNote.setDone( dateReminder.isDoneSelected() );
                        }
                    } catch ( Throwable e2 ) {
                        e2.printStackTrace();
                        this.showMessageDialog( "Fehler", "Es ist ein Fehler aufgetreten, versuche es erneut!" );
                        return;
                    }
                    if ( Main.getInstance().getNettyClient().isConnected() ) {
                        Main.getInstance().getNettyClient().sendPacket( new NoteUpdatePacket( Main.getInstance().getAttributes().getUser().getId(), newNote ) );
                    } else {
                        this.showMessageDialog( "Fehler", "Zurzeit besteht keine Verbindung zum Server!" );
                    }
                } ).addButton( "Abbrechen", true, e -> {
        } ).show();
    }
}