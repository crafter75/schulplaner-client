package de.csbme.schulplaner.client.views;

import com.gluonhq.charm.glisten.afterburner.GluonPresenter;
import com.gluonhq.charm.glisten.animation.FlipInXTransition;
import com.gluonhq.charm.glisten.mvc.View;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import de.csbme.schulplaner.client.Main;
import de.csbme.schulplaner.client.components.ArrowComponent;
import de.csbme.schulplaner.client.manager.AppViewManager;
import de.csbme.schulplaner.client.utils.LoginUtil;
import de.csbme.schulplaner.client.utils.WindowUtil;
import de.csbme.schulplaner.lib.network.packets.user.UserRegisterPacket;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class SignUpPresenter extends GluonPresenter<Main> implements PresenterFunctions {

    @FXML
    private View root;

    @FXML
    private HBox hbxTop, hbxBottom;

    @FXML
    private VBox vbxCenter;

    @FXML
    private StackPane stckCenter;

    @FXML
    private JFXTextField txtEmail, txtPassword, txtPasswordRepeat;

    @FXML
    private JFXButton btnSignUp;

    @FXML
    private Label lblSignIn;

    public void initialize() {
        WindowUtil.percentageConstraintsMobile( root, hbxTop, stckCenter, hbxBottom );
        WindowUtil.disableAppBar( root );

        root.setShowTransitionFactory( v -> new FlipInXTransition( stckCenter ) );

        hbxTop.getChildren().add( new ArrowComponent( AppViewManager.SIGN_IN_VIEW ) );

        lblSignIn.setOnMouseClicked( e -> AppViewManager.changeView( AppViewManager.SIGN_IN_VIEW ) );
        btnSignUp.setOnAction( e -> {
            if ( !LoginUtil.isMailAddress( txtEmail.getText() ) ) {
                this.showMessageDialog( "Fehler", "Bitte gebe eine gültige E-Mail Adresse an!" );
                return;
            }
            if ( !txtPassword.getText().equals( txtPasswordRepeat.getText() ) ) {
                this.showMessageDialog( "Fehler", "Die Passwörter stimmen nicht überein!" );
                return;
            }
            if ( Main.getInstance().getNettyClient().isConnected() ) {
                Main.getInstance().getNettyClient().sendPacket( new UserRegisterPacket( txtEmail.getText(), LoginUtil.getHashedPassword( txtPassword.getText() ) ) );
            } else {
                this.showMessageDialog( "Fehler", "Zurzeit besteht keine Verbindung zum Server!" );
            }
        } );
    }

    @Override
    public void refresh() {
        txtEmail.setText( "" );
        txtPassword.setText( "" );
        txtPasswordRepeat.setText( "" );
    }

    @Override
    public StackPane getCenterPane() {
        return this.stckCenter;
    }
}
