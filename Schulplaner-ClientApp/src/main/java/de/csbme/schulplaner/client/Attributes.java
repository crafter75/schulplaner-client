package de.csbme.schulplaner.client;

import com.gluonhq.charm.down.Services;
import com.gluonhq.charm.down.plugins.DeviceService;
import de.csbme.schulplaner.client.types.PlatformType;
import lombok.Data;

@Data
public class Attributes {

    private final String name = "Schulplaner", author = "Thorsten Engel, Simon Perkons & Jannik Buscha";

    private final PlatformType platform;

    private User user;

    public Attributes() {
        this.platform = Services.get( DeviceService.class ).map( deviceService -> {
            switch ( deviceService.getPlatform().toLowerCase() ) {
                case "android":
                case "ios":
                    return PlatformType.MOBILE;
                default:
                    break;
            }
            return PlatformType.UNKNOWN;
        } ).orElse( PlatformType.DESKTOP );
    }

}
