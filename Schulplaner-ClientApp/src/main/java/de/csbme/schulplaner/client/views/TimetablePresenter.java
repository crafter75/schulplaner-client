package de.csbme.schulplaner.client.views;

import animatefx.animation.ZoomIn;
import com.calendarfx.model.Calendar;
import com.calendarfx.model.CalendarSource;
import com.calendarfx.model.Entry;
import com.calendarfx.view.DateControl;
import com.calendarfx.view.DayViewBase;
import com.calendarfx.view.DetailedWeekView;
import com.gluonhq.charm.glisten.afterburner.GluonPresenter;
import com.gluonhq.charm.glisten.animation.FlipInXTransition;
import com.gluonhq.charm.glisten.animation.NoTransition;
import com.gluonhq.charm.glisten.mvc.View;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import de.csbme.schulplaner.client.Main;
import de.csbme.schulplaner.client.User;
import de.csbme.schulplaner.client.builder.DialogBuilder;
import de.csbme.schulplaner.client.builder.HeaderBuilder;
import de.csbme.schulplaner.client.components.NavigationComponent;
import de.csbme.schulplaner.client.components.SidebarNavigationComponent;
import de.csbme.schulplaner.client.manager.AppViewManager;
import de.csbme.schulplaner.client.types.PlatformType;
import de.csbme.schulplaner.client.utils.SchoolClassUtil;
import de.csbme.schulplaner.client.utils.SubjectUtil;
import de.csbme.schulplaner.client.utils.WindowUtil;
import de.csbme.schulplaner.lib.network.packets.timetable.TimetableDeletePacket;
import de.csbme.schulplaner.lib.network.packets.timetable.TimetableRequestPacket;
import de.csbme.schulplaner.lib.network.packets.timetable.TimetableUpdatePacket;
import de.csbme.schulplaner.lib.object.Subject;
import de.csbme.schulplaner.lib.object.Timetable;
import de.csbme.schulplaner.lib.user.Settings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.SelectionMode;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.util.Callback;
import javafx.util.Duration;
import javafx.util.StringConverter;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Arrays;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TimetablePresenter extends GluonPresenter<Main> implements PresenterFunctions {

    @FXML
    private View root;

    @FXML
    private HBox hbxTop, hbxExit;

    @FXML
    private VBox vbxCenter, vbxSidebar, vbxLeftbar;

    @FXML
    private GridPane grdTimeline;

    @FXML
    private StackPane stckCenter;

    @FXML
    private AnchorPane anchBottom;

    private DialogBuilder openDialog = null;

    public void initialize() {
        WindowUtil.disableAppBar(root);
        WindowUtil.percentageConstraints(root, hbxTop, stckCenter);
        WindowUtil.percentageConstraints(root, grdTimeline);

        root.setShowTransitionFactory(view -> {
            if (Main.getInstance().getAttributes().getUser().getSettings().get(Settings.Key.ANIMATION, Boolean.class)) {
                new ZoomIn(grdTimeline).setSpeed(1.8).play();
                return new FlipInXTransition(stckCenter);
            }
            return new NoTransition();
        });

        // Show header
        new HeaderBuilder().setTitle("Stundenplan")
                .addClickableItem("Stunde hinzufügen", e -> this.showSchoolHourDialog(null))
                .addClickableItem("Stunde bearbeiten", e -> this.showSchoolHourEditDialog(true))
                .addClickableItem("Stunde löschen", e -> this.showSchoolHourEditDialog(false))
                .addClickableItem("Fach hinzufügen", e -> SubjectUtil.showSubjectAddOrUpdateDialog(this, null))
                .addClickableItem("Fach bearbeiten", e -> SubjectUtil.showSubjectEditOrDeleteDialog(this, true))
                .addClickableItem("Fach löschen", e -> SubjectUtil.showSubjectEditOrDeleteDialog(this, false))
                .addClickableItem(Main.getInstance().getAttributes().getUser().getClassInfo() == null ? "Klasse beitreten"
                        : "Klassendetails aufrufen", e -> SchoolClassUtil.showCreateJoinOrViewClassDialog(this))
                .allowToOpenClickableItems(() -> this.openDialog == null)
                .addChangeViewButton(AppViewManager.HOME_VIEW)
                .show(hbxTop);

        if (Main.getInstance().getAttributes().getPlatform() == PlatformType.MOBILE) {
            anchBottom.getChildren().add(new NavigationComponent());
        } else {
            new SidebarNavigationComponent(vbxSidebar, vbxLeftbar, hbxExit);
        }
    }

    @Override
    public void refresh() {
        // Clear old nodes
        stckCenter.getChildren().clear();

        // Send request
        if (Main.getInstance().getNettyClient().isConnected()) {
            User user = Main.getInstance().getAttributes().getUser();
            if (!user.isLocal()) {
                Main.getInstance().getNettyClient().sendPacket(new TimetableRequestPacket(user.getId()));
                return;
            }
        }
        this.updateTimetable(null);
    }

    @Override
    public StackPane getCenterPane() {
        return this.stckCenter;
    }

    public void updateTimetable(Timetable timetable) {
        // Clear old nodes
        stckCenter.getChildren().clear();

        Main.getInstance().getAttributes().getUser().setTimetable(timetable);
        if (timetable == null) {
            return;
        }

        // Show timetable
        DetailedWeekView weekView = new DetailedWeekView(5);
        weekView.setShowAllDayView(false);
        weekView.setStartTime(Timetable.TimeHour.FIRST.getStartTime());
        weekView.setEndTime(Timetable.TimeHour.EIGHTH.getEndTime());
        weekView.setEarlyLateHoursStrategy(DayViewBase.EarlyLateHoursStrategy.HIDE);
        weekView.setVisibleHours(7);
        weekView.setShowScrollBar(false);
        //weekView.setHoursLayoutStrategy( DayViewBase.HoursLayoutStrategy.FIXED_HOUR_HEIGHT ); // ADD SCROLLBAR
        weekView.setEntryDetailsCallback(param -> false);
        weekView.setEntryEditPolicy(param -> false);
        weekView.setEnableCurrentTimeMarker(false);
        weekView.setDraggedEntry(null);
        weekView.setContextMenuCallback(param -> null);
        weekView.setDateDetailsCallback(param -> null);
        weekView.setEntryContextMenuCallback(param -> null);
        weekView.setEntryDetailsPopOverContentCallback(param -> null);
        weekView.selectionModeProperty().addListener((observable, oldValue, newValue) -> weekView.clearSelection());
        weekView.setEntryFactory(param -> null);
        weekView.setOnContextMenuRequested(Event::consume);
        weekView.addEventFilter(MouseEvent.ANY, Event::consume);
        weekView.addEventFilter(DragEvent.ANY, Event::consume);
        weekView.addEventFilter(MouseDragEvent.ANY, Event::consume);
        weekView.addEventFilter(KeyEvent.ANY, Event::consume);
        weekView.addEventFilter(SwipeEvent.ANY, Event::consume);
        weekView.addEventFilter(TouchEvent.ANY, Event::consume);
        weekView.addEventFilter(ZoomEvent.ANY, Event::consume);
        weekView.addEventFilter(RotateEvent.ANY, Event::consume);
        weekView.getProperties().put("disable-focus-handling", true);

        Calendar calendar = new Calendar("Timetable");
        //Calendar calendarBreak = new Calendar( "TimetableBreaks" );

        LocalDate date = weekView.getStartDate();
        for (DayOfWeek day : DayOfWeek.values()) {
            if (day == DayOfWeek.SATURDAY) continue;
            if (day == DayOfWeek.SUNDAY) continue;

            Entry entry = null;

            int currentSubject = -1;
            int minutesToRemove = 0;
            for (Timetable.SchoolHour schoolHour : timetable.getHours(day)) {
                Optional<Subject> optional = Main.getInstance().getAttributes().getUser().getSubjectById(schoolHour.getSubjectId());
                if (!optional.isPresent()) {
                    continue;
                }


                int hour = schoolHour.getTimeHour().getSchoolHour();
                if (hour == 3 || hour == 5 || hour == 7) {
                    minutesToRemove += hour == 7 ? 15 : 20;
                    // Insert break
                   /* Entry breakEntry = new Entry<>( "Pause" );
                    breakEntry.changeStartDate( date );
                    breakEntry.changeEndDate( date );
                    breakEntry.changeStartTime( this.getBreakStartTime( hour ) );
                    breakEntry.changeEndTime( this.getBreakEndTime( hour ) );
                    calendarBreak.addEntry( breakEntry );*/
                }

                if (entry != null) {
                    if (optional.get().getSubjectId() == currentSubject && hour != 3 && hour != 5 && hour != 7) {
                        entry.changeEndTime(schoolHour.getTimeHour().getEndTime().minusMinutes(minutesToRemove));
                        calendar.addEntry(entry);
                        entry = null;
                        continue;
                    }
                    calendar.addEntry(entry);
                }

                entry = new Entry<>(optional.get().getShortName() + "\n" + schoolHour.getTeacher() + "\n" + schoolHour.getRoom());
                entry.changeStartDate(date);
                entry.changeEndDate(date);
                entry.changeStartTime(schoolHour.getTimeHour().getStartTime().minusMinutes(minutesToRemove));
                entry.changeEndTime(schoolHour.getTimeHour().getEndTime().minusMinutes(minutesToRemove));
                currentSubject = optional.get().getSubjectId();
            }
            if (entry != null) {
                calendar.addEntry(entry);
            }
            date = date.plusDays(1);
        }

        CalendarSource calendarSource = new CalendarSource("TimetableSource");
        calendarSource.getCalendars().addAll(calendar); //calendarBreak

        weekView.getCalendarSources().add(calendarSource);

        stckCenter.getChildren().add(weekView);

        weekView.clearSelection();
    }

    private void showSchoolHourEditDialog(boolean edit) {
        if (this.openDialog != null) {
            return;
        }

        if (Main.getInstance().getAttributes().getUser().getSubjects().isEmpty()) {
            this.showMessageDialog("Fehler", "Es wurden keine Fächer gefunden!");
            return;
        }
        Timetable timetable = Main.getInstance().getAttributes().getUser().getTimetable();
        if (timetable == null) {
            this.showMessageDialog("Fehler", "Es wurden keine Stunden gefunden!");
            return;
        }

        JFXComboBox<DayOfWeek> cbxDay = new JFXComboBox<>();
        cbxDay.setPromptText("Tag");
        cbxDay.getItems().addAll(Arrays.stream(DayOfWeek.values()).filter(d -> d != DayOfWeek.SATURDAY && d != DayOfWeek.SUNDAY).collect(Collectors.toList()));
        cbxDay.setValue(cbxDay.getItems().get(0));
        cbxDay.setConverter(new StringConverter<DayOfWeek>() {
            @Override
            public String toString(DayOfWeek dayOfWeek) {
                return dayOfWeek.getDisplayName(TextStyle.FULL, Locale.GERMANY);
            }

            @Override
            public DayOfWeek fromString(String string) {
                return Arrays.stream(DayOfWeek.values()).filter(d -> d.getDisplayName(TextStyle.FULL, Locale.GERMANY).equals(string)).findFirst().orElse(null);
            }
        });

        final Timetable.SchoolHour dummy = new Timetable.SchoolHour(-1, DayOfWeek.SUNDAY, Timetable.TimeHour.FIRST, 0, "", "");

        JFXComboBox<Timetable.SchoolHour> cbxHour = new JFXComboBox<>();
        cbxHour.setPromptText("Stunde");
        cbxHour.getItems().addAll(timetable.getHours(cbxDay.getValue()));
        cbxHour.setValue(cbxHour.getItems().get(0));
        cbxHour.setConverter(new StringConverter<Timetable.SchoolHour>() {
            @Override
            public String toString(Timetable.SchoolHour schoolHour) {
                if (schoolHour.getSchoolHourId() == -1) {
                    return "Keine Stunden gefunden";
                }
                LocalTime start = schoolHour.getTimeHour().getStartTime();
                LocalTime end = schoolHour.getTimeHour().getEndTime();
                return schoolHour.getTimeHour().getSchoolHour() + ". (" + start.getHour() + ":" + start.getMinute()
                        + " - " + end.getHour() + ":" + end.getMinute() + ")";
            }

            @Override
            public Timetable.SchoolHour fromString(String string) {
                if (!string.contains(".")) {
                    return dummy;
                }
                return timetable.getHours(cbxDay.getValue()).stream().filter(s -> s.getTimeHour().getSchoolHour()
                        == Integer.parseInt(string.split("\\.")[0])).findFirst().orElse(null);
            }
        });

        cbxDay.valueProperty().addListener((observable, oldValue, newValue) -> {
            cbxHour.getItems().clear();
            cbxHour.getItems().addAll(timetable.getHours(newValue));
            if (cbxHour.getItems().isEmpty()) {
                cbxHour.getItems().add(dummy);
            }
            cbxHour.setValue(cbxHour.getItems().get(0));
        });

        DialogBuilder builder = this.getDialog();
        this.openDialog = builder;

        builder.setTitle(edit ? "Stunde bearbeiten | Auswahl" : "Stunde löschen")
                .addContentNode(cbxDay, cbxHour)
                .addCloseEvent(e -> {
                    if (this.openDialog != null && this.openDialog.equals(builder)) {
                        this.openDialog = null;
                    }
                })
                .addButton(edit ? "Bearbeiten" : "Löschen", true, e -> {
                    if (cbxDay.getValue() == null || cbxHour.getValue() == null
                            || cbxHour.getValue().equals(dummy)) {
                        e.consume();
                        this.showMessageDialog("Fehler", "Es wurde keine Stunde ausgewählt!");
                        return;
                    }
                    if (edit) {
                        if (this.openDialog != null && this.openDialog.equals(builder)) {
                            this.openDialog = null;
                        }
                        this.showSchoolHourDialog(cbxHour.getValue());
                        return;
                    }
                    if (Main.getInstance().getNettyClient().isConnected()) {
                        Main.getInstance().getNettyClient().sendPacket(new TimetableDeletePacket(Main.getInstance().getAttributes().getUser().getId(), cbxHour.getValue().getSchoolHourId()));
                    } else {
                        this.showMessageDialog("Fehler", "Zurzeit besteht keine Verbindung zum Server!");
                    }
                }).addButton("Schließen", true, e -> {
        }).show();
    }

    private void showSchoolHourDialog(final Timetable.SchoolHour schoolHour) {
        if (this.openDialog != null) {
            return;
        }
        if (Main.getInstance().getAttributes().getUser().getSubjects().isEmpty()) {
            this.showMessageDialog("Fehler", "Es wurden keine Fächer gefunden!");
            return;
        }
        JFXComboBox<String> cbxSubject = new JFXComboBox<>();
        cbxSubject.setPromptText("Fach");
        cbxSubject.getItems().addAll(Main.getInstance().getAttributes().getUser().getSubjects().stream().map(Subject::getName).collect(Collectors.toList()));
        if (schoolHour != null) {
            cbxSubject.setValue(Main.getInstance().getAttributes().getUser().getSubjectById(schoolHour.getSubjectId())
                    .map(Subject::getName).orElse("Unbekannt"));
        } else {
            cbxSubject.setValue(cbxSubject.getItems().get(0));
        }

        JFXComboBox<DayOfWeek> cbxDay = new JFXComboBox<>();
        cbxDay.setPromptText("Tag");
        cbxDay.getItems().addAll(Arrays.stream(DayOfWeek.values()).filter(d -> d != DayOfWeek.SATURDAY && d != DayOfWeek.SUNDAY).collect(Collectors.toList()));
        cbxDay.setConverter(new StringConverter<DayOfWeek>() {
            @Override
            public String toString(DayOfWeek dayOfWeek) {
                return dayOfWeek.getDisplayName(TextStyle.FULL, Locale.GERMANY);
            }

            @Override
            public DayOfWeek fromString(String string) {
                return Arrays.stream(DayOfWeek.values()).filter(d -> d.getDisplayName(TextStyle.FULL, Locale.GERMANY).equals(string)).findFirst().orElse(null);
            }
        });
        if (schoolHour != null) {
            cbxDay.setValue(schoolHour.getDay());
        } else {
            cbxDay.setValue(cbxDay.getItems().get(0));
        }

        JFXComboBox<Timetable.TimeHour> cbxHourStart = this.getTimeHourComboBox(true, schoolHour);
        JFXComboBox<Timetable.TimeHour> cbxHourEnd = this.getTimeHourComboBox(false, schoolHour);

        cbxHourStart.valueProperty().addListener((observable, oldValue, newValue) -> {
            Timetable.TimeHour oldTimeHour = cbxHourEnd.getValue();
            cbxHourEnd.getItems().clear();
            cbxHourEnd.getItems().addAll(Arrays.stream(Timetable.TimeHour.values()).filter(t -> t.ordinal() >= newValue.ordinal()).collect(Collectors.toList()));
            if (!cbxHourEnd.getItems().contains(oldTimeHour)) {
                cbxHourEnd.setValue(cbxHourEnd.getItems().get(0));
            } else {
                cbxHourEnd.setValue(oldTimeHour);
            }
        });

        JFXTextField txtRoom = new JFXTextField();
        txtRoom.setPromptText("Raum");
        txtRoom.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.length() > 5) {
                newValue = newValue.substring(0, 5);
            }
            txtRoom.setText(newValue);
        });
        if (schoolHour != null) {
            txtRoom.setText(schoolHour.getRoom());
        }

        JFXTextField txtTeacher = new JFXTextField();
        txtTeacher.setPromptText("Lehrer");
        txtTeacher.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.length() > 5) {
                newValue = newValue.substring(0, 5);
            }
            txtTeacher.setText(newValue);
        });
        if (schoolHour != null) {
            txtTeacher.setText(schoolHour.getTeacher());
        }

        DialogBuilder builder = this.getDialog();
        this.openDialog = builder;

        builder.setTitle(schoolHour == null ? "Stunde hinzufügen" : "Stunde bearbeiten")
                .addContentNode(cbxSubject, cbxDay, cbxHourStart, cbxHourEnd, txtRoom, txtTeacher)
                .addCloseEvent(e -> {
                    if (this.openDialog != null && this.openDialog.equals(builder)) {
                        this.openDialog = null;
                    }
                })
                .addButton("Speichern", true, e -> {
                    if (cbxSubject.getValue() == null || cbxSubject.getValue().isEmpty()
                            || cbxDay.getValue() == null || cbxHourStart.getValue() == null
                            || cbxHourEnd.getValue() == null || txtRoom.getText() == null
                            || txtRoom.getText().isEmpty() || txtTeacher.getText() == null
                            || txtTeacher.getText().isEmpty()) {
                        e.consume();
                        this.showMessageDialog("Fehler", "Bitte fülle alle Felder aus!");
                        return;
                    }

                    Timetable timetable = Main.getInstance().getAttributes().getUser().getTimetable();
                    if (timetable != null && schoolHour == null && timetable.getTimetable().stream().anyMatch(s -> s.getDay() == cbxDay.getValue()
                            && (s.getTimeHour().ordinal() == cbxHourStart.getValue().ordinal() || s.getTimeHour().ordinal() == cbxHourEnd.getValue().ordinal()))) {
                        e.consume();
                        this.showMessageDialog("Fehler", "Diese Stunde ist an diesem Tag bereits belegt!");
                        return;
                    }

                    Optional<Subject> optionalSubject = Main.getInstance().getAttributes().getUser().getSubjectByName(cbxSubject.getSelectionModel().getSelectedItem());
                    int subjectId;
                    try {
                        subjectId = optionalSubject.orElseThrow((Supplier<Throwable>) () -> new NullPointerException("Can't find subject for name!")).getSubjectId();
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                        this.showMessageDialog("Fehler", "Es ist ein Fehler aufgetreten, versuche es erneut!");
                        return;
                    }
                    DayOfWeek dayOfWeek = cbxDay.getSelectionModel().getSelectedItem();
                    Timetable.TimeHour start = cbxHourStart.getSelectionModel().getSelectedItem();
                    Timetable.TimeHour end = cbxHourEnd.getSelectionModel().getSelectedItem();

                    Timetable.SchoolHour[] schoolHours;
                    if (start.ordinal() != end.ordinal()) {
                        schoolHours = new Timetable.SchoolHour[((end.ordinal() - start.ordinal()) + 1)];
                        AtomicInteger index = new AtomicInteger(0);
                        IntStream.rangeClosed(start.ordinal(), end.ordinal()).forEach(i -> {
                            Timetable.SchoolHour hour = null;
                            if (timetable != null) {
                                hour = timetable.getHours(dayOfWeek).stream().filter(s -> s.getTimeHour().ordinal() == i).findFirst().orElse(null);
                            }
                            schoolHours[index.getAndIncrement()] = this.getNewOrReplace(hour, dayOfWeek,
                                    Timetable.TimeHour.values()[i], subjectId, txtRoom.getText(), txtTeacher.getText());
                        });
                    } else {
                        schoolHours = new Timetable.SchoolHour[]{this.getNewOrReplace(schoolHour, dayOfWeek,
                                start, subjectId, txtRoom.getText(), txtTeacher.getText())};
                    }

                    if (Main.getInstance().getNettyClient().isConnected()) {
                        Main.getInstance().getNettyClient().sendPacket(new TimetableUpdatePacket(Main.getInstance().getAttributes().getUser().getId(), schoolHours));
                    } else {
                        this.showMessageDialog("Fehler", "Zurzeit besteht keine Verbindung zum Server!");
                    }
                }).addButton("Abbrechen", true, e -> {
        }).show();
    }

    private JFXComboBox<Timetable.TimeHour> getTimeHourComboBox(boolean start, Timetable.SchoolHour schoolHour) {
        JFXComboBox<Timetable.TimeHour> cbxHour = new JFXComboBox<>();
        cbxHour.setPromptText(start ? "Start" : "Ende");
        cbxHour.getItems().addAll(Timetable.TimeHour.values());
        cbxHour.setConverter(new StringConverter<Timetable.TimeHour>() {
            @Override
            public String toString(Timetable.TimeHour t) {
                if (start) {
                    return t.getSchoolHour() + ". (" + t.getStartTime().getHour() + ":" + t.getStartTime().getMinute() + ")";
                }
                return t.getSchoolHour() + ". (" + t.getEndTime().getHour() + ":" + t.getEndTime().getMinute() + ")";
            }

            @Override
            public Timetable.TimeHour fromString(String string) {
                return Arrays.stream(Timetable.TimeHour.values()).filter(t -> t.getSchoolHour() == Integer.parseInt(string.split("\\.")[0])).findFirst().orElse(null);
            }
        });
        if (schoolHour != null) {
            cbxHour.setValue(schoolHour.getTimeHour());
        } else {
            cbxHour.setValue(cbxHour.getItems().get(start ? 0 : 1));
        }
        return cbxHour;
    }

    private Timetable.SchoolHour getNewOrReplace(Timetable.SchoolHour schoolHour, DayOfWeek dayOfWeek, Timetable.TimeHour hour, int subjectId, String room, String teacher) {
        if (schoolHour == null) {
            schoolHour = new Timetable.SchoolHour(-1, dayOfWeek, hour, subjectId, room, teacher);
        } else {
            schoolHour.setDay(dayOfWeek);
            schoolHour.setTimeHour(hour);
            schoolHour.setSubjectId(subjectId);
            schoolHour.setRoom(room);
            schoolHour.setTeacher(teacher);
        }
        return schoolHour;
    }

    private LocalTime getBreakStartTime(int hour) {
        switch (hour) {
            case 3:
                return LocalTime.of(9, 20);
            case 5:
                return LocalTime.of(11, 10);
            case 7:
                return LocalTime.of(13, 0);
            default:
                return LocalTime.now();
        }
    }

    private LocalTime getBreakEndTime(int hour) {
        return this.getBreakStartTime(hour).plusMinutes(hour == 7 ? 15 : 20);
    }
}