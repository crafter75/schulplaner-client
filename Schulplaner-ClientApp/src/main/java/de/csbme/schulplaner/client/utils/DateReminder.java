package de.csbme.schulplaner.client.utils;

import de.csbme.schulplaner.client.builder.DialogBuilder;
import de.csbme.schulplaner.client.components.CheckBoxComponent;
import de.csbme.schulplaner.client.components.DatePickerComponent;
import de.csbme.schulplaner.client.components.TimePickerComponent;
import de.csbme.schulplaner.lib.object.Note;

import java.time.ZoneId;

public class DateReminder {

    private DatePickerComponent datePickerComponent;

    private TimePickerComponent timePickerComponent;

    private CheckBoxComponent reminderCheckBoxComponent, doneCheckBoxComponent;

    public DateReminder( Note note, DialogBuilder builder ) {
        this.datePickerComponent = new DatePickerComponent( note == null ? -1L : note.getExpireDate() );
        this.timePickerComponent = new TimePickerComponent( note == null ? -1L : note.getExpireDate() );

        this.reminderCheckBoxComponent = new CheckBoxComponent( "Erinnerung aktivieren" );
        this.reminderCheckBoxComponent.setSelected( note != null && note.getExpireDate() != -1L );
        this.reminderCheckBoxComponent.addListener( ( observable, oldValue, newValue ) -> builder.getContentNodes().ifPresent( vBox -> {
            if ( newValue ) {
                if ( !vBox.getChildren().contains( this.datePickerComponent ) ) {
                    vBox.getChildren().add( this.datePickerComponent );
                }

                if ( !vBox.getChildren().contains( this.timePickerComponent ) ) {
                    vBox.getChildren().add( this.timePickerComponent );
                }
            } else {
                vBox.getChildren().removeIf( node -> node instanceof DatePickerComponent || node instanceof TimePickerComponent );
            }
        } ) );
        this.reminderCheckBoxComponent.setId( "reminder" );

        this.doneCheckBoxComponent = null;
        if ( note != null ) {
            this.doneCheckBoxComponent = new CheckBoxComponent( "Erledigt" );
            this.doneCheckBoxComponent.setSelected( note.isDone() );
            this.doneCheckBoxComponent.addListener( ( observable, oldValue, newValue ) -> builder.getContentNodes().ifPresent( vBox -> {
                if ( newValue ) {
                    vBox.getChildren().removeIf( node -> ( node instanceof CheckBoxComponent
                            && this.reminderCheckBoxComponent.getId().equals( node.getId() ) ) );
                    this.reminderCheckBoxComponent.setSelected( false );
                } else {
                    if ( !vBox.getChildren().contains( this.reminderCheckBoxComponent ) ) {
                        vBox.getChildren().add( this.reminderCheckBoxComponent );
                        this.reminderCheckBoxComponent.setSelected( this.reminderCheckBoxComponent.isSelected() );
                    }
                }
            } ) );
        }

        if ( this.doneCheckBoxComponent != null ) {
            builder.addContentNode( this.doneCheckBoxComponent );
        }

        if ( this.doneCheckBoxComponent == null || !this.doneCheckBoxComponent.isSelected() ) {
            builder.addContentNode( this.reminderCheckBoxComponent );
            if ( this.reminderCheckBoxComponent.isSelected() ) {
                builder.addContentNode( this.datePickerComponent, this.timePickerComponent );
            }
        }
    }

    public long getTimestamp() {
        if ( ( this.doneCheckBoxComponent == null || !this.doneCheckBoxComponent.isSelected() ) && this.reminderCheckBoxComponent.isSelected() ) {
            return this.datePickerComponent.getSelectedDate().atTime( this.timePickerComponent.getSelectedTime() )
                    .atZone( ZoneId.systemDefault() ).toInstant().toEpochMilli();
        }
        return -1L;
    }

    public boolean isDoneSelected() {
        return this.doneCheckBoxComponent != null && this.doneCheckBoxComponent.isSelected();
    }
}
