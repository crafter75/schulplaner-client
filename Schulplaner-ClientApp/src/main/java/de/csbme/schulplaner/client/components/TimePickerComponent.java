package de.csbme.schulplaner.client.components;

import com.jfoenix.controls.JFXComboBox;
import de.csbme.schulplaner.client.utils.ComponentUtil;
import javafx.scene.layout.HBox;

import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.stream.IntStream;

public class TimePickerComponent extends HBox {

    private JFXComboBox<String> cboHour, cboMinutes;

    public TimePickerComponent( long input ) {
        this.setSpacing( 10 );

        LocalTime currentTime;
        if ( input != -1L ) {
            currentTime = Instant.ofEpochMilli( input ).atZone( ZoneId.systemDefault() ).toLocalTime();
        } else {
            currentTime = LocalTime.now();
        }

        this.cboHour = ComponentUtil.pickerComboBox();
        this.cboHour.getItems().addAll( IntStream.rangeClosed( 0, 23 ).mapToObj( String::valueOf ).toArray( String[]::new ) );
        this.cboHour.setPromptText( "Stunden" );
        this.cboHour.setValue( currentTime.getHour() + "" );

        this.cboMinutes = ComponentUtil.pickerComboBox();
        this.cboMinutes.getItems().addAll( IntStream.rangeClosed( 0, 59 ).mapToObj( String::valueOf ).toArray( String[]::new ) );
        this.cboMinutes.setPromptText( "Minuten" );
        this.cboMinutes.setValue( currentTime.getMinute() + "" );

        this.getChildren().addAll( this.cboHour, this.cboMinutes );
    }

    public LocalTime getSelectedTime() {
        try {
            return LocalTime.of( Integer.parseInt( this.cboHour.getSelectionModel().getSelectedItem() ),
                    Integer.parseInt( this.cboMinutes.getSelectionModel().getSelectedItem() ), 0 );
        } catch ( Exception e ) {
            return null;
        }
    }

}