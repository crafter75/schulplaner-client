package de.csbme.schulplaner.client.components;

import com.gluonhq.charm.glisten.afterburner.AppView;
import de.csbme.schulplaner.client.manager.AppViewManager;
import de.csbme.schulplaner.client.types.ViewType;
import de.csbme.schulplaner.client.views.TimetablePresenter;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.SVGPath;

public class SidebarNavigationComponent extends VBox {

    public SidebarNavigationComponent(VBox vBoxTop, VBox vBoxLeft, HBox hbxExit) {
        SVGPath icon = new SVGPath();
        icon.setContent("M28,32a4.006,4.006,0,0,1-4-4,2,2,0,1,0-4,0v3a1,1,0,0,1-1,1H1a1,1,0,0,1-1-1V28a4.006,4.006,0,0,1,4-4H8V1A1,1,0,0,1,9,0H31a1,1,0,0,1,1,1V28A4.007,4.007,0,0,1,28,32ZM4,25.994a2,2,0,0,0-2,2v2H18V28a4,4,0,0,1,.537-2Zm10-8.007v2H26v-2Zm0-6.006v2H26v-2Zm0-6.006v2H26v-2Z");
        icon.getStyleClass().add("navigationBarIcon");

        StackPane stckLogo = new StackPane(icon);
        stckLogo.getStyleClass().add("sideBarLogo");
        stckLogo.setOnMouseClicked(e -> AppViewManager.changeView(AppViewManager.HOME_VIEW));

        vBoxTop.getChildren().add(stckLogo);

        VBox vbxTimetable = navigationButton("Stundenplan", ViewType.TIMETABLE);
        VBox vbxEntries = navigationButton("Einträge", ViewType.ENTRIES);
        VBox vbxNotes = navigationButton("Notizen", ViewType.NOTES);
        VBox vbxGrades = navigationButton("Noten", ViewType.GRADES);

        if (AppViewManager.getCurrentView() == AppViewManager.TIMETABLE_VIEW) {
            vbxTimetable.getStyleClass().add("sideBarSelected");
        } else if (AppViewManager.getCurrentView() == AppViewManager.ENTRIES_VIEW) {
            vbxEntries.getStyleClass().add("sideBarSelected");
        } else if (AppViewManager.getCurrentView() == AppViewManager.NOTES_VIEW) {
            vbxNotes.getStyleClass().add("sideBarSelected");
        } else if (AppViewManager.getCurrentView() == AppViewManager.GRADES_VIEW) {
            vbxGrades.getStyleClass().add("sideBarSelected");
        }

        vBoxLeft.getChildren().addAll(vbxTimetable, vbxEntries, vbxNotes, vbxGrades);

        hbxExit.setOnMouseClicked(e -> System.exit(0));
    }

    private VBox navigationButton(String title, ViewType type) {
        VBox vBox = new VBox(8);
        vBox.setMinWidth(108);
        vBox.setMinHeight(72);

        vBox.getStyleClass().add("sideBarItem");

        SVGPath icon = new SVGPath();
        icon.setScaleX(1.4);
        icon.setScaleY(1.4);
        icon.setContent(type.getPath());
        icon.getStyleClass().add("navigationBarIcon");

        VBox.setMargin(icon, new Insets(3, 0, 0, 0));

        vBox.setOnMouseClicked(e -> AppViewManager.changeView(type.getView()));

        Label lblTitle = new Label(title);
        lblTitle.getStyleClass().add("sideBarLabel");

        vBox.getChildren().addAll(icon, lblTitle);

        return vBox;
    }

}