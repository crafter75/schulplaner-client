package de.csbme.schulplaner.client;

import com.gluonhq.charm.glisten.application.MobileApplication;
import de.csbme.schulplaner.client.manager.AppViewManager;
import de.csbme.schulplaner.client.network.NettyClient;
import de.csbme.schulplaner.client.types.PlatformType;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import lombok.Getter;

@Getter
public class Main extends MobileApplication {

    @Getter
    private static Main instance;

    private Attributes attributes;

    private NettyClient nettyClient;

    @Override
    public void init() {
        instance = this;
        this.attributes = new Attributes();
        this.nettyClient = new NettyClient();

        AppViewManager.registerViewsAndDrawer(this);
    }

    @Override
    public void postInit(Scene scene) {
        if (getAttributes().getPlatform() == PlatformType.DESKTOP) {
            scene.getWindow().setWidth(1280 + 16);
            scene.getWindow().setHeight(720 + 37);
        } else {
            scene.getWindow().setWidth(375 + 16);
            scene.getWindow().setHeight(667 + 37);
        }

        this.setTitle("Schulplaner");

        scene.getStylesheets().add(Main.class.getResource("css/style.css").toExternalForm());
        ((Stage) scene.getWindow()).getIcons().add(new Image(Main.class.getResourceAsStream("/icon.png")));
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        if (getNettyClient() != null) {
            getNettyClient().disconnect();
        }
        System.exit(0);
    }

}