package de.csbme.schulplaner.client;

import de.csbme.schulplaner.lib.network.packets.user.UserLoginCallbackPacket;
import de.csbme.schulplaner.lib.object.*;
import de.csbme.schulplaner.lib.object.Class;
import de.csbme.schulplaner.lib.user.Settings;
import de.csbme.schulplaner.lib.user.UserGroup;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Getter
public class User {

    private boolean local;

    private int id;

    @Setter
    private String username;

    @Setter
    private String mailAddress;

    private long registrationDate;

    private Settings settings;

    private UserGroup group;

    @Setter
    private Class classInfo;

    @Setter
    private List<Note> notes;

    @Setter
    private List<Subject> subjects;

    @Setter
    private List<Grade> grades;

    @Setter
    private Timetable timetable;

    @Setter
    private List<Quiz> quizzes;

    @Setter
    private Quiz currentQuiz;

    public User( UserLoginCallbackPacket packet ) {
        this.local = false;
        this.id = packet.getId();
        this.username = packet.getUsername();
        this.mailAddress = packet.getMailAddress();
        this.registrationDate = packet.getRegistrationDate();
        this.settings = packet.getSettings();
        this.group = packet.getUserGroup();
        this.classInfo = packet.getClassInfo();
        this.notes = packet.getNotes();
        this.subjects = packet.getSubjects();
        this.grades = new ArrayList<>();
        this.timetable = null;
        this.quizzes = new ArrayList<>();
        this.currentQuiz = null;
    }

    public User() {
        this.local = true;
        this.id = -1;
        this.username = "Lokaler Benutzer";
        this.mailAddress = "";
        this.registrationDate = System.currentTimeMillis();
        this.settings = new Settings();
        this.group = UserGroup.DEFAULT;
        this.classInfo = null;
        this.notes = new ArrayList<>();
        this.subjects = new ArrayList<>();
        this.grades = new ArrayList<>();
        this.timetable = null;
        this.quizzes = new ArrayList<>();
        this.currentQuiz = null;
    }

    public Optional<Subject> getSubjectById( int id ) {
        return this.subjects.stream().filter( s -> s.getSubjectId() == id ).findFirst();
    }

    public Optional<Subject> getSubjectByName( String name ) {
        return this.subjects.stream().filter( s -> s.getName().equalsIgnoreCase( name ) ).findFirst();
    }
}
