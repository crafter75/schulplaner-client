package de.csbme.schulplaner.client.views;

import com.gluonhq.charm.glisten.afterburner.GluonPresenter;
import com.gluonhq.charm.glisten.animation.FlipInXTransition;
import com.gluonhq.charm.glisten.animation.NoTransition;
import com.gluonhq.charm.glisten.mvc.View;
import com.jfoenix.controls.JFXTextField;
import de.csbme.schulplaner.client.Main;
import de.csbme.schulplaner.client.builder.BoxBuilder;
import de.csbme.schulplaner.client.builder.DialogBuilder;
import de.csbme.schulplaner.client.builder.HeaderBuilder;
import de.csbme.schulplaner.client.components.NavigationComponent;
import de.csbme.schulplaner.client.components.SidebarNavigationComponent;
import de.csbme.schulplaner.client.manager.AppViewManager;
import de.csbme.schulplaner.client.types.PlatformType;
import de.csbme.schulplaner.client.utils.DateReminder;
import de.csbme.schulplaner.client.utils.WindowUtil;
import de.csbme.schulplaner.lib.network.packets.note.NoteDeletePacket;
import de.csbme.schulplaner.lib.network.packets.note.NoteUpdatePacket;
import de.csbme.schulplaner.lib.object.Note;
import de.csbme.schulplaner.lib.user.Category;
import de.csbme.schulplaner.lib.user.Settings;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.util.List;
import java.util.stream.Collectors;

public class NotesPresenter extends GluonPresenter<Main> implements PresenterFunctions {

    @FXML
    private View root;

    @FXML
    private HBox hbxTop, hbxExit;

    @FXML
    private VBox vbxCenter, vbxSidebar, vbxLeftbar;

    @FXML
    private StackPane stckCenter;

    @FXML
    private AnchorPane anchBottom;

    private DialogBuilder openDialog;

    public void initialize() {
        WindowUtil.disableAppBar( root );
        WindowUtil.percentageConstraints( root, hbxTop, stckCenter );

        root.setShowTransitionFactory( view -> {
            if ( Main.getInstance().getAttributes().getUser().getSettings().get( Settings.Key.ANIMATION, Boolean.class ) ) {
                return new FlipInXTransition( stckCenter );
            }
            return new NoTransition();
        } );

        vbxCenter.getChildren().add( new Label( "Notes" ) );

        if (Main.getInstance().getAttributes().getPlatform() == PlatformType.MOBILE) {
            anchBottom.getChildren().add(new NavigationComponent());
        } else {
            new SidebarNavigationComponent(vbxSidebar, vbxLeftbar, hbxExit);
        }
    }

    @Override
    public void refresh() {
        // Clear old nodes
        this.hbxTop.getChildren().clear();
        this.vbxCenter.getChildren().clear();

        // Get notes
        List<Note> notes = Main.getInstance().getAttributes().getUser().getNotes().stream().filter( n ->
                n.getCategory() == Category.NOTES.ordinal() ).collect( Collectors.toList() );

        // Set header
        new HeaderBuilder().setTitle( "Notizen" )
                .addClickableItem( "Notiz hinzufügen", e -> this.showNoteDialog( null ) )
                .allowToOpenClickableItems( () -> this.openDialog == null )
                .addChangeViewButton( AppViewManager.HOME_VIEW )
                .addNotificationAmount( notes.size() )
                .show( this.hbxTop );

        // Set content
        BoxBuilder boxBuilder = new BoxBuilder();
        notes.stream().sorted( ( o1, o2 ) -> Boolean.compare( o1.isDone(), o2.isDone() ) ).forEach( n ->
                boxBuilder.addNoteBox( n, editEvent -> this.showNoteDialog( n ), deleteEvent -> {
                    if ( Main.getInstance().getNettyClient().isConnected() ) {
                        Main.getInstance().getNettyClient().sendPacket( new NoteDeletePacket(
                                Main.getInstance().getAttributes().getUser().getId(),
                                n.getNoteId()
                        ) );
                    } else {
                        this.showMessageDialog( "Fehler", "Zurzeit besteht keine Verbindung zum Server!" );
                    }
                } ) );
        boxBuilder.show( vbxCenter );
    }

    @Override
    public StackPane getCenterPane() {
        return this.stckCenter;
    }

    public void showNoteDialog( final Note note ) {
        if ( this.openDialog != null ) {
            return;
        }

        JFXTextField txtTitle = new JFXTextField();
        txtTitle.setPromptText( "Titel" );
        if ( note != null ) {
            txtTitle.setText( note.getTitle() );
        }

        JFXTextField txtContent = new JFXTextField();
        txtContent.setPromptText( "Beschreibung" );
        if ( note != null ) {
            txtContent.setText( note.getContent() );
        }

        DialogBuilder builder = this.getDialog();
        this.openDialog = builder;

        builder.addContentNode( txtTitle, txtContent );

        DateReminder dateReminder = new DateReminder( note, builder );

        builder.setTitle( note == null ? "Notiz hinzufügen" : "Notiz bearbeiten" )
                .addCloseEvent( e -> {
                    if ( this.openDialog != null && this.openDialog.equals( builder ) ) {
                        this.openDialog = null;
                    }
                } )
                .addButton( "Speichern", true, e -> {
                    Note newNote = note;

                    if ( txtTitle.getText() == null || txtTitle.getText().isEmpty() ) {
                        e.consume();
                        this.showMessageDialog( "Fehler", "Bitte fülle alle Felder aus!" );
                        return;
                    }

                    long expireTimestamp = dateReminder.getTimestamp();

                    try {
                        if ( newNote == null ) {
                            newNote = new Note( -1, Category.NOTES, Note.OwnerType.USER, txtTitle.getText(), txtContent.getText(), expireTimestamp, dateReminder.isDoneSelected() ); // TODO at start only user entries. Class entries coming soon :)
                        } else {
                            newNote.setTitle( txtTitle.getText() );
                            newNote.setContent( txtContent.getText() );
                            newNote.setExpireDate( expireTimestamp );
                            newNote.setDone( dateReminder.isDoneSelected() );
                        }
                    } catch ( Throwable e2 ) {
                        e2.printStackTrace();
                        this.showMessageDialog( "Fehler", "Es ist ein Fehler aufgetreten, versuche es erneut!" );
                        return;
                    }
                    if ( Main.getInstance().getNettyClient().isConnected() ) {
                        Main.getInstance().getNettyClient().sendPacket( new NoteUpdatePacket( Main.getInstance().getAttributes().getUser().getId(), newNote ) );
                    } else {
                        this.showMessageDialog( "Fehler", "Zurzeit besteht keine Verbindung zum Server!" );
                    }
                } ).addButton( "Abbrechen", true, e -> {
        } ).show();
    }
}