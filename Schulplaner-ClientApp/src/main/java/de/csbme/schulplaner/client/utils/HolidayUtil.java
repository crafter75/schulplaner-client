package de.csbme.schulplaner.client.utils;

import com.google.gson.Gson;
import javafx.application.Platform;
import lombok.AllArgsConstructor;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

public class HolidayUtil {

    private static String holidayUrl = "https://ferien-api.de/api/v1/holidays/{state}/{year}";

    private static DateTimeFormatter iso8601Formatter = DateTimeFormatter.ofPattern( "yyyy-MM-dd'T'HH:mm" );

    private static Gson gson = new Gson();

    public static void getNextTwoHolidays( String state, BiConsumer<Holiday, Holiday> biConsumer ) {
        new Thread( () -> {
            LocalDate currentDate = LocalDate.now();
            try {
                URL url = new URL( holidayUrl.replace( "{state}", state ).replace( "{year}", "" + currentDate.getYear() ) );
                URLConnection urlConnection = url.openConnection();
                urlConnection.connect();

                Holiday[] holidays = gson.fromJson( new InputStreamReader( (InputStream) urlConnection.getContent() ), Holiday[].class );

                List<Holiday> sortedHolidays = Arrays.stream( holidays ).sorted( Comparator.comparing( Holiday::getStartDate ) ).filter( h -> {
                    LocalDate date = h.getEndDate();
                    return date.isAfter( currentDate ) || date.isEqual( currentDate );
                } ).limit( 2 ).collect( Collectors.toList() );

                Platform.runLater( () -> {
                    if ( sortedHolidays.size() >= 2 ) {
                        biConsumer.accept( sortedHolidays.get( 0 ), sortedHolidays.get( 1 ) );
                    } else if ( sortedHolidays.size() == 1 ) {
                        biConsumer.accept( sortedHolidays.get( 0 ), null );
                    } else {
                        biConsumer.accept( null, null );
                    }
                } );
            } catch ( IOException e ) {
                Platform.runLater( () -> biConsumer.accept( null, null ) );
            }
        } ).start();
    }

    @AllArgsConstructor
    public class Holiday {

        private String start, end, stateCode, name, slug;

        private int year;

        public LocalDate getStartDate() {
            return LocalDate.parse( this.start, iso8601Formatter );
        }

        public LocalDate getEndDate() {
            return LocalDate.parse( this.end, iso8601Formatter );
        }

        public String getDisplayName() {
            return this.name.substring( 0, 1 ).toUpperCase() + this.name.substring( 1 );
        }
    }
}
