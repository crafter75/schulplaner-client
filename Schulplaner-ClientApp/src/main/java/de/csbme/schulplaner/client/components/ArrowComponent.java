package de.csbme.schulplaner.client.components;

import com.gluonhq.charm.glisten.afterburner.AppView;
import de.csbme.schulplaner.client.manager.AppViewManager;
import javafx.geometry.Pos;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.SVGPath;

public class ArrowComponent extends StackPane {

    public ArrowComponent(AppView view) {
        SVGPath icoArrow = new SVGPath();
        icoArrow.setContent("M2934.727-254.858l-4.889-4.889a1,1,0,0,1-.131-.111,1,1,0,0,1-.293-.718,1,1,0,0,1,.293-.717,1,1,0,0,1,.128-.108l4.892-4.892a1,1,0,0,1,1.415,0,1,1,0,0,1,0,1.414l-3.3,3.3h20.577a1,1,0,0,1,1,1,1,1,0,0,1-1,1h-20.586l3.308,3.307a1,1,0,0,1,0,1.414,1,1,0,0,1-.708.293A1,1,0,0,1,2934.727-254.858Z");

        if (AppViewManager.getCurrentView() == AppViewManager.PROFILE_VIEW) {
            icoArrow.getStyleClass().add("navigationBarIcon");
        } else {
            icoArrow.getStyleClass().add("icon");
        }

        this.setAlignment(Pos.TOP_LEFT);
        this.getStyleClass().add("hand");
        this.setOnMouseClicked(e -> AppViewManager.changeView(view));
        this.getChildren().add(icoArrow);

    }

}