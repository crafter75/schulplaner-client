package de.csbme.schulplaner.client.components;

import de.csbme.schulplaner.client.Main;
import de.csbme.schulplaner.client.manager.AppViewManager;
import de.csbme.schulplaner.client.views.PresenterFunctions;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;

public class LogoComponent extends StackPane {

    public LogoComponent( String letter, int size, Pos position, PresenterFunctions presenterFunctions ) {
        Label lblLetter = new Label( letter );
        lblLetter.setPrefWidth( size );
        lblLetter.setPrefHeight( size );
        lblLetter.getStyleClass().add( "logo" );
        lblLetter.setOnMouseClicked( e -> {
            if ( Main.getInstance().getAttributes().getUser() == null || Main.getInstance().getAttributes().getUser().isLocal() ) {
                presenterFunctions.showMessageDialog( "Fehler", "Du musst für diese Funktion eingeloggt sein!" );
                return;
            }
            AppViewManager.changeView( AppViewManager.PROFILE_VIEW );
        } );

        this.setAlignment( position );
        HBox.setHgrow( this, Priority.ALWAYS );

        this.getChildren().add( lblLetter );
    }

}
