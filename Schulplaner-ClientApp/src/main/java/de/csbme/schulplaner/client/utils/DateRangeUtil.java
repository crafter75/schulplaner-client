package de.csbme.schulplaner.client.utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.TextStyle;
import java.util.Locale;

public class DateRangeUtil {

    public static String getDateRange( long min, long max ) {
        LocalDate minDate = Instant.ofEpochMilli( min ).atZone( ZoneId.systemDefault() ).toLocalDate();
        LocalDate maxDate = Instant.ofEpochMilli( max ).atZone( ZoneId.systemDefault() ).toLocalDate();
        String dateRange = getShortDateFormatFromDate( minDate );
        if ( !minDate.isEqual( maxDate ) ) {
            dateRange += " - " + getShortDateFormatFromDate( maxDate );
        }
        return dateRange;
    }

    private static String getShortDateFormatFromDate( LocalDate date ) {
        return ( date.getDayOfMonth() < 10 ? "0" + date.getDayOfMonth() : date.getDayOfMonth() + "" ) + " "
                + date.getMonth().getDisplayName( TextStyle.SHORT, Locale.GERMANY ) + ".";
    }
}
