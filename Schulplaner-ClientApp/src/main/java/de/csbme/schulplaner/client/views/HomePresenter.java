package de.csbme.schulplaner.client.views;

import com.gluonhq.charm.glisten.afterburner.GluonPresenter;
import com.gluonhq.charm.glisten.animation.FlipInXTransition;
import com.gluonhq.charm.glisten.animation.NoTransition;
import com.gluonhq.charm.glisten.mvc.View;
import de.csbme.schulplaner.client.Main;
import de.csbme.schulplaner.client.User;
import de.csbme.schulplaner.client.builder.BoxBuilder;
import de.csbme.schulplaner.client.builder.HeaderBuilder;
import de.csbme.schulplaner.client.components.NavigationComponent;
import de.csbme.schulplaner.client.components.SidebarNavigationComponent;
import de.csbme.schulplaner.client.manager.AppViewManager;
import de.csbme.schulplaner.client.types.EntriesType;
import de.csbme.schulplaner.client.types.PlatformType;
import de.csbme.schulplaner.client.utils.WindowUtil;
import de.csbme.schulplaner.lib.object.Note;
import de.csbme.schulplaner.lib.object.Subject;
import de.csbme.schulplaner.lib.user.Category;
import de.csbme.schulplaner.lib.user.Settings;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

public class HomePresenter extends GluonPresenter<Main> implements PresenterFunctions {

    @FXML
    private View root;

    @FXML
    private HBox hbxTop, hbxExit;

    @FXML
    private VBox vbxCenter, vbxSidebar, vbxLeftbar;

    @FXML
    private StackPane stckCenter;

    @FXML
    private AnchorPane anchBottom;

    public void initialize() {
        WindowUtil.disableAppBar(root);

        root.setShowTransitionFactory(view -> {
            if (Main.getInstance().getAttributes().getUser().getSettings().get(Settings.Key.ANIMATION, Boolean.class)) {
                return new FlipInXTransition(stckCenter);
            }
            return new NoTransition();
        });

        if (Main.getInstance().getAttributes().getPlatform() == PlatformType.MOBILE) {
            anchBottom.getChildren().add(new NavigationComponent());
            WindowUtil.percentageConstraintsMobile(root, hbxTop, stckCenter);
        } else {
            WindowUtil.percentageConstraints(root, hbxTop, stckCenter);
            new SidebarNavigationComponent(vbxSidebar, vbxLeftbar, hbxExit);
        }

    }

    @Override
    public void refresh() {
        // Clear old nodes
        this.hbxTop.getChildren().clear();
        this.vbxCenter.getChildren().clear();

        // Get user & notes
        User user = Main.getInstance().getAttributes().getUser();
        List<Subject> subjects = user.getSubjects();
        Instant now = Instant.now();
        List<Note> notes = user.getNotes().stream().filter(n -> subjects.stream().anyMatch(s -> s.getSubjectId() == n.getSubjectId()) && !n.isDone()
        && (n.getExpireDate() == -1 || Instant.ofEpochMilli(n.getExpireDate()).isAfter(now))).collect(Collectors.toList());

        // Old: HeaderBuilder.show(hbxTop, "Startseite");
        // Set header
        new HeaderBuilder().setTitle("Startseite")
                .addNotificationAmount((int) notes.stream().filter(n -> n.getCategory() != Category.NOTES.ordinal()).count())
                .addClickableLogo((user.getUsername() == null || user.getUsername().isEmpty()) ? "U" : user.getUsername().substring(0, 1).toUpperCase(), this)
                .show(hbxTop);

        // Set content
        BoxBuilder boxBuilder = new BoxBuilder();
        boxBuilder.addHomeBox(EntriesType.HOMEWORK, notes.stream().filter(n -> n.getCategory() == Category.HOMEWORK.ordinal()).collect(Collectors.toList()), event ->
                this.showNoteDialog("Hausaufgabe hinzufügen", Category.HOMEWORK.ordinal()));

        boxBuilder.addHomeBox(EntriesType.TEST, notes.stream().filter(n -> n.getCategory() == Category.EXAM.ordinal() || n.getCategory() == Category.TEST.ordinal()).collect(Collectors.toList()), event ->
                this.showNoteDialog("Arbeit oder Test hinzufügen", Category.EXAM.ordinal(), Category.TEST.ordinal()));

        boxBuilder.addHomeBox(EntriesType.PRESENTATION, notes.stream().filter(n -> n.getCategory() == Category.PRESENTATION.ordinal()).collect(Collectors.toList()), event ->
                this.showNoteDialog("Präsentation hinzufügen", Category.PRESENTATION.ordinal()));

        boxBuilder.addHomeBox(EntriesType.HOLIDAYS, null, event -> {
        });

        boxBuilder.show(vbxCenter);
    }

    @Override
    public StackPane getCenterPane() {
        return this.stckCenter;
    }

    private void showNoteDialog(String title, int... categories) {
        AppViewManager.changeView(AppViewManager.ENTRIES_VIEW);
        if (AppViewManager.getCurrentPresenter() instanceof EntriesPresenter) {
            ((EntriesPresenter) AppViewManager.getCurrentPresenter()).showNoteDialog(title, null, categories);
        }
    }
}