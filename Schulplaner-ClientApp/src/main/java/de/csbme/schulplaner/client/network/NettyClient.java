package de.csbme.schulplaner.client.network;

import de.csbme.schulplaner.client.Main;
import de.csbme.schulplaner.client.User;
import de.csbme.schulplaner.client.utils.LoginUtil;
import de.csbme.schulplaner.lib.network.Packet;
import de.csbme.schulplaner.lib.network.packets.user.UserAlivePacket;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import javafx.application.Platform;
import lombok.Getter;
import lombok.Setter;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class NettyClient {

    private static final boolean EPOLL = Epoll.isAvailable();

    private static final ExecutorService POOL = Executors.newCachedThreadPool();

    private final String HOST = "127.0.0.1"; // TODO change host!

    private final int PORT = 19130;

    @Setter
    private boolean reconnect = true;

    @Getter
    @Setter
    private Channel channel;

    public NettyClient() {
        // Connect to server
        this.connect();

        // Start alive sender
        new Timer().scheduleAtFixedRate( new TimerTask() {
            @Override
            public void run() {
                // Send alive packet if user logged in
                User user = Main.getInstance().getAttributes().getUser();
                if ( user != null && !user.isLocal() ) {
                    NettyClient.this.sendPacket( new UserAlivePacket( user.getId() ) );
                }
            }
        }, 5000L, 5000L ); // 5 seconds
    }

    /**
     * Connect the netty client to the netty server
     */
    private void connect() {
        // Run in other thread
        POOL.execute( () -> {
            EventLoopGroup eventLoopGroup = EPOLL ? new EpollEventLoopGroup() : new NioEventLoopGroup();

            try {
                // Create bootstrap with epoll if available and add object decoder/encoder
                Bootstrap bootstrap = new Bootstrap()
                        .group( eventLoopGroup )
                        .channel( EPOLL ? EpollSocketChannel.class : NioSocketChannel.class )
                        .handler( new ChannelInitializer<SocketChannel>() {
                            @Override
                            protected void initChannel( SocketChannel socketChannel ) throws Exception {
                                socketChannel.pipeline().addLast( "encoder", new ObjectEncoder() );
                                socketChannel.pipeline().addLast( "decoder", new ObjectDecoder( Integer.MAX_VALUE, ClassResolvers.cacheDisabled( Packet.class.getClassLoader() ) ) );
                                socketChannel.pipeline().addLast( new ClientHandler( NettyClient.this ) );
                            }
                        } );
                // Connect to specific host and port
                ChannelFuture channelFuture = bootstrap.connect( this.HOST, this.PORT );
                // Check connection
                channelFuture.sync();

                System.out.println( "Successfully connected to the netty server!" );

                // Add close listener
                channelFuture.channel().closeFuture().addListener( future -> {
                    Platform.runLater( LoginUtil::onServerTimeout );

                    // Reconnect to server
                    this.reconnect();

                    // Shutdown
                    eventLoopGroup.shutdownGracefully();
                } );
                channelFuture.channel().closeFuture().sync();
            } catch ( Exception e ) {
                // Reset channel
                this.channel = null;

                // Reconnect to server
                try {
                    this.reconnect();
                } catch ( InterruptedException e1 ) {
                    e1.printStackTrace();
                }
            } finally {
                // Shutdown
                eventLoopGroup.shutdownGracefully();
            }
        } );
    }

    /**
     * Reconnect to the server
     */
    private void reconnect() throws InterruptedException {
        System.out.println( "No connection to the Schulplaner Server! Reconnect in 2 seconds..." );

        // Wait 2 seconds
        Thread.sleep( 2000L );

        // Reconnect to the netty server
        if ( this.reconnect ) {
            this.connect();
        }
    }

    /**
     * Send a Packet to the netty server
     *
     * @param packet to send
     */
    public void sendPacket( Packet packet ) {
        // Check if connected
        if ( !this.isConnected() ) return;

        // Check if packet not null
        if ( packet == null ) return;

        // Send packet to server
        this.channel.writeAndFlush( packet );
    }

    /**
     * Disconnect if the client is connected to the server
     */
    public void disconnect() {
        // Disable reconnect attribute
        this.reconnect = false;

        // Check if connected
        if ( !this.isConnected() ) return;

        // Close and reset channel
        this.channel.close();
        this.channel = null;
    }

    /**
     * Check if the client is connected to the server
     *
     * @return if the connection alive and ready
     */
    public boolean isConnected() {
        return this.channel != null && this.channel.isActive();
    }
}
